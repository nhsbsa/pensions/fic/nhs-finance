package uk.nhs.nhsbsa.finance.employerdetails.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.startsWith;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.nhs.nhsbsa.finance.employerdetails.converter.EmployingAuthorityEntityToView;
import uk.nhs.nhsbsa.finance.employerdetails.converter.OrganisationEntityToView;
import uk.nhs.nhsbsa.finance.employerdetails.converter.UserEntityToView;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;
import uk.nhs.nhsbsa.finance.employerdetails.repository.EmployingAuthorityRepository;
import uk.nhs.nhsbsa.finance.employerdetails.repository.FinanceUserRepository;
import uk.nhs.nhsbsa.finance.employerdetails.repository.OrganisationRepository;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;

/**
 * Test service layer facilities
 *
 * Created by duncan on 14/03/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class EmployerDetailsServiceTests {

  private EmployerDetailsService service;

  @MockBean
  private OrganisationRepository organisationRepository;

  @MockBean
  private EmployingAuthorityRepository employingAuthorityRepository;

  @MockBean
  private FinanceUserRepository userRepository;

  @MockBean
  private UserEntityToView userConverter;

  @MockBean
  private EmployingAuthorityEntityToView eaConverter;

  @MockBean
  private OrganisationEntityToView orgConverter;

  @Before
  public void before() {
    ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
    OrganisationEntityToView orgConverter = new OrganisationEntityToView(vf.getValidator(), eaConverter);
    UserEntityToView userConverter = new UserEntityToView(vf.getValidator(), orgConverter);
    service = new EmployerDetailsService(organisationRepository, userRepository, employingAuthorityRepository, orgConverter,
        userConverter);
  }
  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Test
  public void given_existing_organisation_when_uuid_is_provided_then_it_is_retrieved() {
    UUID uuid = UUID.fromString("854a7ac3-b419-406e-87bd-2af8a74f793b");
    String eaCode = "EA12345";
    String name = "Hospital";
    String employerType = "STAFF";

    final EmployingAuthorityEntity ea = EmployingAuthorityEntity.builder()
        .eaCode(eaCode)
        .name(name)
        .employerType(employerType)
        .build();
    final List<EmployingAuthorityEntity> eaList = Collections.singletonList(ea);

    final OrganisationEntity orgEntity = OrganisationEntity.builder()
        .uuid(uuid.toString())
        .name(name)
        .eas(eaList)
        .build();
    when(organisationRepository.findByUuid(uuid.toString()))
        .thenReturn(orgEntity);

    final EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode(eaCode)
        .name(name)
        .employerType(employerType)
        .build();
    final List<EmployingAuthorityView> eaViewList = Collections.singletonList(eaView);
    final OrganisationView orgView = OrganisationView.builder().identifier(uuid).name(name).eas(eaViewList).build();

    when(eaConverter.convert(ea)).thenReturn(eaView);
    when(orgConverter.convert(orgEntity)).thenReturn(orgView);

    Optional<OrganisationView> view = service.findOrganisation((uuid));

    if (view.isPresent()) {
      OrganisationView org = view.get();
      assertThat(org.getIdentifier()).isEqualTo(uuid);
      assertThat(org.getName()).isEqualTo(name);
      assertThat(org.getEas().get(0).getEaCode()).isEqualTo(eaCode);
      assertThat(org.getEas().get(0).getName()).isEqualTo(name);
      assertThat(org.getEas().get(0).getEmployerType()).isEqualTo(employerType);
    } else {
      fail("OrganisationView unexpectedly missing from Optional");
    }
  }

  @Test
  public void given_existing_organisation_when_eaCode_is_provided_then_it_is_retrieved() {
    UUID uuid = UUID.fromString("854a7ac3-b419-406e-87bd-2af8a74f793b");
    String eaCode = "EA12345";
    String name = "Hospital";
    String employerType = "STAFF";

    final EmployingAuthorityEntity ea = EmployingAuthorityEntity.builder()
        .eaCode(eaCode)
        .name(name)
        .employerType(employerType)
        .build();
    final List<EmployingAuthorityEntity> eaList = Collections.singletonList(ea);

    final OrganisationEntity orgEntity = OrganisationEntity.builder()
        .uuid(uuid.toString())
        .name(name)
        .eas(eaList)
        .build();
    final List<OrganisationEntity> orgEntityList = Collections.singletonList(orgEntity);
    when(organisationRepository.findByEaCode(eaCode))
        .thenReturn(orgEntityList);

    final EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode(eaCode)
        .name(name)
        .employerType(employerType)
        .build();
    final List<EmployingAuthorityView> eaViewList = Collections.singletonList(eaView);
    final OrganisationView orgView = OrganisationView.builder().identifier(uuid).name(name).eas(eaViewList).build();

    when(eaConverter.convert(ea)).thenReturn(eaView);
    when(orgConverter.convert(orgEntity)).thenReturn(orgView);

    Optional<OrganisationView> view = service.findOrganisationByEaCode(eaCode);

    if (view.isPresent()) {
      OrganisationView org = view.get();
      assertThat(org.getName()).isEqualTo(name);
      assertThat(org.getIdentifier()).isEqualTo(uuid);
      assertThat(org.getEas().get(0).getEaCode()).isEqualTo(eaCode);
      assertThat(org.getEas().get(0).getName()).isEqualTo(name);
      assertThat(org.getEas().get(0).getEmployerType()).isEqualTo(employerType);
    } else {
      fail("OrganisationView unexpectedly missing from Optional");
    }
  }

  @Test
  public void given_organisation_doesnt_exist_empty_organisation_view_returned() {
    String eaCode = "EA12345";

    when(organisationRepository.findByEaCode(eaCode))
        .thenReturn(Collections.emptyList());

    Optional<OrganisationView> view = service.findOrganisationByEaCode(eaCode);

    assertFalse(view.isPresent());
  }

  @Test
  public void given_unknown_organisation_when_store_invoked_then_instance_created() {
    final EmployingAuthorityView ea = EmployingAuthorityView.builder().eaCode("EA99999").name("Test Account Name").employerType("STAFF").build();
    final List<EmployingAuthorityView> eaList = Collections.singletonList(ea);

    CreateOrganisationRequest organisationToStore = CreateOrganisationRequest.builder()
        .name("Test Account Name")
        .eas(eaList)
        .build();

    final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
        .eaCode("EA99999")
        .name("Test Account Name")
        .employerType("STAFF")
        .build();
    final List<EmployingAuthorityEntity> eaEntityList = Collections.singletonList(eaEntity);

    OrganisationEntity orgEntity = OrganisationEntity.builder().eas(eaEntityList)
        .name("Test Account Name").uuid(UUID.randomUUID().toString()).build();

    when(organisationRepository.save(any(OrganisationEntity.class))).thenReturn(orgEntity);

    final EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode("EA99999")
        .name("Test Account Name")
        .employerType("STAFF")
        .build();
    final List<EmployingAuthorityView> eaViewList = Collections.singletonList(eaView);

    OrganisationView orgView = OrganisationView.builder().eas(eaViewList)
        .name("Test Account Name").identifier(UUID.randomUUID()).build();

    when(eaConverter.convert(eaEntity)).thenReturn(eaView);
    when(orgConverter.convert(orgEntity)).thenReturn(orgView);

    OrganisationView organisation = service.storeOrganisation(organisationToStore);

    assertThat(organisation.getIdentifier()).isNotNull();
    assertThat(organisation.getName()).isEqualTo("Test Account Name");
    assertThat(organisation.getEas().get(0).getEaCode()).isEqualTo("EA99999");
  }

  @Test
  public void given_existing_user_when_find_invoked_then_it_is_retrieved() {
    UUID uuid = UUID.fromString("c62e5dd4-a956-407d-ab8d-4996aedc88dd");
    String name = "Alison";
    String lastName = "Moore";
    String phoneNumber = "01237611978";
    String email = "alison.moore@example.com";
    String eaCode = "EA1234";
    String eaName = "Test Org/ea";
    String employerType = EmployerTypes.STAFF.name();
    String orgUuid = "32d386f6-c7fc-4e25-923a-8c176c3781c6";

    final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
        .eaCode(eaCode)
        .name(eaName)
        .employerType(EmployerTypes.STAFF.name())
        .build();
    final List<EmployingAuthorityEntity> eaList = Collections.singletonList(eaEntity);
    final OrganisationEntity orgEntity = new OrganisationEntity(null,orgUuid,eaName,eaList);
    final List<OrganisationEntity> orgEntityList = Collections.singletonList(orgEntity);

    final FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .uuid(uuid.toString())
        .firstName(name)
        .lastName(lastName)
        .telephoneNumber(phoneNumber)
        .emailAddress(email)
        .organisations(orgEntityList)
        .build();

    when(userRepository.findByUuid(uuid.toString()))
        .thenReturn(userEntity);

    final EmployingAuthorityView eaView = EmployingAuthorityView.builder()
        .eaCode(eaCode)
        .name(name)
        .employerType(employerType)
        .build();
    final List<EmployingAuthorityView> eaViewList = Collections.singletonList(eaView);
    final OrganisationView orgView = OrganisationView.builder().identifier(uuid).name(name).eas(eaViewList).build();
    final List<OrganisationView> organisationViewList = Collections.singletonList(orgView);
    final UserView userView = UserView.builder()
        .identifier(uuid)
        .firstName(name)
        .lastName(lastName)
        .contactNumber(phoneNumber)
        .emailAddress(email)
        .organisations(organisationViewList)
        .build();

    when(eaConverter.convert(eaEntity)).thenReturn(eaView);
    when(orgConverter.convert(orgEntity)).thenReturn(orgView);
    when(userConverter.convert(userEntity)).thenReturn(userView);

    Optional<UserView> view = service.findUser(uuid);

    if (view.isPresent()) {
      UserView user = view.get();
      assertThat(user.getFirstName()).isEqualTo(name);
      assertThat(user.getLastName()).isEqualTo(lastName);
      assertThat(user.getContactNumber()).isEqualTo(phoneNumber);
      assertThat(user.getEmailAddress()).isEqualTo(email);
      assertThat(user.getOrganisations().get(0).getIdentifier()).isEqualTo(UUID.fromString(orgUuid));
      assertThat(user.getOrganisations().get(0).getEas().get(0).getEmployerType()).isEqualTo(employerType);
    } else {
      fail("UserView unexpectedly missing from Optional");
    }
  }

  @Test
  public void given_unknown_user_when_store_invoked_then_instance_created() {

    String orgUuid = "71aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    final EmployingAuthorityView eaEntity = EmployingAuthorityView.builder()
        .eaCode("")
        .name("")
        .employerType(EmployerTypes.STAFF.name())
        .build();
    List<EmployingAuthorityView> eaList = Collections.singletonList(eaEntity);
    OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgUuid)).name("").eas(eaList).build();
    List<OrganisationView> orgEntityList = Collections.singletonList(orgView);

    when(organisationRepository.findByUuid(orgUuid))
        .thenReturn(OrganisationEntity.builder().build());
    UserView view = UserView.builder()
        .firstName("Mary")
        .lastName("Poppins")
        .organisations(orgEntityList)
        .contactNumber("55512344321")
        .emailAddress("mary@poppins.com")
        .build();

    view = service.storeUser(view);

    assertThat(view.getIdentifier()).isNotNull();
    assertThat(String.format("%s %s", view.getFirstName(), view.getLastName()))
        .isEqualTo("Mary Poppins");
    assertThat(view.getContactNumber()).isEqualTo("55512344321");
    assertThat(view.getEmailAddress()).isEqualTo("mary@poppins.com");
    assertThat(view.getOrganisations().get(0).getIdentifier()).isEqualTo(UUID.fromString(orgUuid));
  }

  @Test
  public void given_known_user_when_store_invoked_then_instance_created_with_uuid_provided() {

    String orgUuid = "71aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    final EmployingAuthorityView eaEntity = EmployingAuthorityView.builder()
        .eaCode("")
        .name("")
        .employerType(EmployerTypes.STAFF.name())
        .build();
    List<EmployingAuthorityView> eaList = Collections.singletonList(eaEntity);
    OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgUuid)).name("").eas(eaList).build();
    List<OrganisationView> orgEntityList = Collections.singletonList(orgView);

    when(organisationRepository.findByUuid(orgUuid))
        .thenReturn(OrganisationEntity.builder().build());

    UserView view = UserView.builder()
        .firstName("Robert")
        .lastName("Robertson")
        .organisations(orgEntityList)
        .contactNumber("55512344321")
        .emailAddress("r.robertson@poppins.com")
        .identifier(UUID.fromString("4a0877a8-1454-44f8-ad76-a9c890cf2cfb"))
        .build();

    view = service.storeUser(view);

    assertThat(view.getIdentifier())
        .isEqualTo(UUID.fromString("4a0877a8-1454-44f8-ad76-a9c890cf2cfb"));
  }

  @Test
  public void given_org_id_exists_then_user_returned() {

    String orgUuid = "71aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    String userUuid = "81aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    String firstName = "Sam";
    String lastName = "Jones";
    String phoneNumber = "11111111111";
    String email = "sam.jones@email.com";
    final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
        .eaCode("EA1234")
        .name("ORG NAME")
        .employerType(EmployerTypes.STAFF.name())
        .build();
    final List<EmployingAuthorityEntity> eaList = Collections.singletonList(eaEntity);
    final OrganisationEntity orgEntity = new OrganisationEntity(null,orgUuid,"ORG NAME",eaList);
    final List<OrganisationEntity> orgEntityList = Collections.singletonList(orgEntity);
    final FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .uuid(userUuid)
        .firstName(firstName)
        .lastName(lastName)
        .telephoneNumber(phoneNumber)
        .emailAddress(email)
        .organisations(orgEntityList)
        .build();
    final List<FinanceUserEntity> userEntityList = Collections.singletonList(userEntity);

    when(userRepository.findByOrgUuid(orgUuid))
        .thenReturn(userEntityList);

    UserView userView = UserView.builder()
        .identifier(UUID.fromString(userUuid))
        .firstName(firstName)
        .lastName(lastName)
        .contactNumber(phoneNumber)
        .emailAddress(email)
        .build();

    when(eaConverter.convert(EmployingAuthorityEntity.builder().build())).thenReturn(EmployingAuthorityView.builder().build());
    when(orgConverter.convert(OrganisationEntity.builder().build())).thenReturn(OrganisationView.builder().build());
    when(userConverter.convert(userEntity)).thenReturn(userView);

    Optional<List<UserView>> view = service.findUserByOrgUuid(UUID.fromString(orgUuid));

    if (view.isPresent()) {
      List<UserView> user = view.get();
      assertThat(user.get(0).getFirstName()).isEqualTo(firstName);
      assertThat(user.get(0).getLastName()).isEqualTo(lastName);
      assertThat(user.get(0).getContactNumber()).isEqualTo(phoneNumber);
      assertThat(user.get(0).getEmailAddress()).isEqualTo(email);
    } else {
      fail("UserView unexpectedly missing from Optional");
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void given_unknown_organisation_when_store_user_invoked_then_exception_thrown() {
    final EmployingAuthorityView eaEntity = EmployingAuthorityView.builder()
        .eaCode("")
        .name("")
        .employerType(EmployerTypes.STAFF.name())
        .build();
    List<EmployingAuthorityView> eaList = Collections.singletonList(eaEntity);
    OrganisationView orgView = OrganisationView.builder().identifier(UUID.randomUUID()).name("").eas(eaList).build();
    List<OrganisationView> orgEntityList = Collections.singletonList(orgView);


    UserView view = UserView.builder()
        .firstName("Bob")
        .lastName("Builder")
        .organisations(orgEntityList)
        .contactNumber("555 1234 4321")
        .emailAddress("bob@yeehaw.com")
        .build();

    view = service.storeUser(view);

    assertThat(view.getIdentifier()).isNotNull();
  }

  @Test
  public void storing_finance_user_with_create_user_request_object_test() {

    String email = "valid@email.com";
    String firstName = "";
    String lastName = "";
    String usedId = "UUID-1245498461";
    String orgUuid = "71aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    String orgName = "Test Organsation";
    String eaCode = "EA1234";
    String contactNumber = "";

    final EmployingAuthorityView ea =EmployingAuthorityView.builder().eaCode(eaCode).name("").employerType(EmployerTypes.STAFF.name()).build();
    final List<EmployingAuthorityView> eaList = Collections.singletonList(ea);
    final OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgUuid)).name(orgName).eas(eaList).build();
    final List<OrganisationView> orgViewList = Collections.singletonList(orgView);

    CreateUserRequest createUserRequest = CreateUserRequest.builder()
        .username(email)
        .firstName(firstName)
        .surname(lastName)
        .uuid(usedId)
        .organisations(orgViewList)
        .contactNumber(contactNumber)
        .build();

    final EmployingAuthorityEntity eaEntity =EmployingAuthorityEntity.builder().eaCode(eaCode).name("").employerType(EmployerTypes.STAFF.name()).build();
    final List<EmployingAuthorityEntity> eaEntityList = Collections.singletonList(eaEntity);
    final OrganisationEntity orgEntity = OrganisationEntity.builder().uuid(orgUuid).name(orgName).eas(eaEntityList).build();
    final List<OrganisationEntity> orgEntityList = Collections.singletonList(orgEntity);

    FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .emailAddress(email)
        .firstName(firstName)
        .lastName(lastName)
        .uuid(usedId)
        .organisations(orgEntityList)
        .telephoneNumber(contactNumber)
        .build();

    when(organisationRepository.findByUuid(orgUuid)).thenReturn(orgEntity);
    when(userRepository.save(userEntity)).thenReturn(userEntity);

    CreateUserResponse response = service.storeUser(createUserRequest);

    assertEquals(response.getEmail(), email);
    assertEquals(response.getUuid(), usedId);
  }

  @Test
  public void return_exception_when_finance_user_not_stored_test() {

    String email = "valid@email.com";
    String firstName = "";
    String lastName = "";
    String usedId = "UUID-1245498461";
    String orgUuid = "71aa593a-fe5c-4ff0-b09f-7805d4f591ec";
    String orgName = "Test Organsation";
    String eaCode = "EA1234";
    String contactNumber = "";

    final EmployingAuthorityView ea =EmployingAuthorityView.builder().eaCode(eaCode).name("").employerType(EmployerTypes.STAFF.name()).build();
    final List<EmployingAuthorityView> eaList = Collections.singletonList(ea);
    final OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgUuid)).name(orgName).eas(eaList).build();
    final List<OrganisationView> orgViewList = Collections.singletonList(orgView);

    CreateUserRequest createUserRequest = CreateUserRequest.builder()
        .username(email)
        .firstName(firstName)
        .surname(lastName)
        .uuid(usedId)
        .organisations(orgViewList)
        .contactNumber(contactNumber)
        .build();

    final EmployingAuthorityEntity eaEntity =EmployingAuthorityEntity.builder().eaCode(eaCode).name("").employerType(EmployerTypes.STAFF.name()).build();
    final List<EmployingAuthorityEntity> eaEntityList = Collections.singletonList(eaEntity);
    final OrganisationEntity orgEntity = OrganisationEntity.builder().uuid(orgUuid).name(orgName).eas(eaEntityList).build();
    final List<OrganisationEntity> orgEntityList = Collections.singletonList(orgEntity);

    FinanceUserEntity userEntity = FinanceUserEntity.builder()
        .emailAddress(email)
        .firstName(firstName)
        .lastName(lastName)
        .uuid(usedId)
        .organisations(orgEntityList)
        .telephoneNumber(contactNumber)
        .build();

    when(organisationRepository.findByUuid(orgUuid)).thenReturn(orgEntity);
    when(userRepository.save(userEntity)).thenReturn(null);

      CreateUserResponse response = service.storeUser(createUserRequest);

      thrown.expectMessage(startsWith("Error when trying to save User"));

  }

  @Test
  public void given_known_ea_code_provided_then_employer_type_returned() {

    String eaCode = "EA1234";
    String response = EmployerTypes.STAFF.name();

    when(employingAuthorityRepository.findEmployerTypeByEaCode(eaCode))
        .thenReturn(response);

    Optional<String> result = service.findEmployerTypeByEaCode(eaCode);

    assertThat(result)
        .isEqualTo(Optional.of(response));
  }

  @Test
  public void given_unknown_ea_code_provided_then_null_returned() {

    String eaCode = "EA1234";
    String response = EmployerTypes.STAFF.name();

    when(employingAuthorityRepository.findEmployerTypeByEaCode(eaCode))
        .thenReturn(null);

    Optional<String> result = service.findEmployerTypeByEaCode(eaCode);

    assertFalse(result.isPresent());
  }

  @Test
  public void storing_Organisation_without_ea_object_test() {

    String orgName = "New Organisation";
    String orgUuid = "org-uuid";

    CreateOrganisationRequest organisationRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .build();

    OrganisationEntity savedOrganisationEntity = OrganisationEntity.builder()
        .id(1L)
        .uuid(orgUuid)
        .name(orgName)
        .build();

    when(organisationRepository.save(any(OrganisationEntity.class))).thenReturn(savedOrganisationEntity);

    Optional<CreateOrganisationResponse> response = service.storeOrganisationWithoutEa(organisationRequest);

    assertTrue(response.isPresent());
    assertEquals(response.get().getName(), orgName );
    assertEquals(response.get().getUuid(), orgUuid);

  }

  @Test
  public void storing_Ea_object_without_previous_eas_test() {

    String eaCode = "EA1234";
    String eaName = "EA Name";
    String employerType = "STAFF";
    UUID orgUuid = UUID.randomUUID();
    String orgName = "Organisation Name";

    when(employingAuthorityRepository.findByEaCode(any(String.class))).thenReturn(null);

    EmployingAuthorityEntity savedEa = EmployingAuthorityEntity.builder()
        .id(1L)
        .name(eaName)
        .eaCode(eaCode)
        .employerType(employerType)
        .build();

    when(employingAuthorityRepository.save(any(EmployingAuthorityEntity.class))).thenReturn(savedEa);

    List<EmployingAuthorityEntity> eaList = new ArrayList<>();
    OrganisationEntity savedOrganisationEntity = OrganisationEntity.builder()
        .id(1L)
        .uuid(orgUuid.toString())
        .name(orgName)
        .eas(eaList)
        .build();

    when(organisationRepository.findByUuid(any(String.class))).thenReturn(savedOrganisationEntity);

    OrganisationEntity organisationEntityWithNewEa = OrganisationEntity.builder()
        .id(1L)
        .uuid(orgUuid.toString())
        .name(orgName)
        .eas(Collections.singletonList(savedEa))
        .build();

    when(organisationRepository.save(any(OrganisationEntity.class))).thenReturn(organisationEntityWithNewEa);

    EmployingAuthorityView convertedEa = EmployingAuthorityView.builder().eaCode(eaCode).name(eaName).employerType(employerType).build();
    OrganisationView convertedOrg = OrganisationView.builder().identifier(orgUuid).name(orgName).eas(Collections.singletonList(convertedEa)).build();

    when(eaConverter.convert(savedEa)).thenReturn(convertedEa);
    when(orgConverter.convert(organisationEntityWithNewEa)).thenReturn(convertedOrg);


    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .organisationUuid(orgUuid.toString())
        .eaCode(eaCode)
        .eaName(eaName)
        .employerType(employerType)
        .build();
    Optional<OrganisationView> response = service.storeEa(eaRequest);

    assertTrue(response.isPresent());
    assertEquals(response.get().getName(), orgName );
    assertEquals(response.get().getIdentifier(), orgUuid);
    assertFalse(response.get().getEas().isEmpty());
    assertEquals(response.get().getEas().get(0), convertedEa);

  }

  @Test
  public void storing_Ea_object_with_previous_eas_test() {

    String eaCode = "EA2222";
    String eaName = "EA 2 Name";
    String employerType = "STAFF";
    UUID orgUuid = UUID.randomUUID();
    String orgName = "Organisation Name";

    when(employingAuthorityRepository.findByEaCode(any(String.class))).thenReturn(null);

    EmployingAuthorityEntity savedEa = EmployingAuthorityEntity.builder()
        .id(2L)
        .name(eaName)
        .eaCode(eaCode)
        .employerType(employerType)
        .build();

    when(employingAuthorityRepository.save(any(EmployingAuthorityEntity.class))).thenReturn(savedEa);

    EmployingAuthorityEntity existingEa = EmployingAuthorityEntity.builder()
        .id(1L)
        .name("EA 1 Name")
        .eaCode("EA1111")
        .employerType(employerType)
        .build();

    List<EmployingAuthorityEntity> savedEaList = new ArrayList<>();
    savedEaList.add(existingEa);
    OrganisationEntity savedOrganisationEntity = OrganisationEntity.builder()
        .id(1L)
        .uuid(orgUuid.toString())
        .name(orgName)
        .eas(savedEaList)
        .build();

    when(organisationRepository.findByUuid(any(String.class))).thenReturn(savedOrganisationEntity);

    List<EmployingAuthorityEntity> eaList = new ArrayList<>();
    eaList.add(existingEa);
    eaList.add(savedEa);
    OrganisationEntity organisationEntityWithNewEa = OrganisationEntity.builder()
        .id(1L)
        .uuid(orgUuid.toString())
        .name(orgName)
        .eas(eaList)
        .build();

    when(organisationRepository.save(any(OrganisationEntity.class))).thenReturn(organisationEntityWithNewEa);

    List<EmployingAuthorityView> convertedEaList = new ArrayList<>();
    EmployingAuthorityView convertedEa1 = EmployingAuthorityView.builder().eaCode("EA1111").name("EA 1 Name").employerType(employerType).build();
    EmployingAuthorityView convertedEa2 = EmployingAuthorityView.builder().eaCode(eaCode).name(eaName).employerType(employerType).build();
    convertedEaList.add(convertedEa1);
    convertedEaList.add(convertedEa2);
    OrganisationView convertedOrg = OrganisationView.builder().identifier(orgUuid).name(orgName).eas(convertedEaList).build();

    when(eaConverter.convert(existingEa)).thenReturn(convertedEa1);
    when(eaConverter.convert(savedEa)).thenReturn(convertedEa2);
    when(orgConverter.convert(organisationEntityWithNewEa)).thenReturn(convertedOrg);


    CreateEaRequest eaRequest = CreateEaRequest.builder()
        .organisationUuid(orgUuid.toString())
        .eaCode(eaCode)
        .eaName(eaName)
        .employerType(employerType)
        .build();

    Optional<OrganisationView> response = service.storeEa(eaRequest);

    assertTrue(response.isPresent());
    assertEquals(response.get().getName(), orgName );
    assertEquals(response.get().getIdentifier(), orgUuid);
    assertFalse(response.get().getEas().isEmpty());
    assertEquals(response.get().getEas().get(0), convertedEa1);
    assertEquals(response.get().getEas().get(1), convertedEa2);

  }

  @Test
  public void given_existing_organisation_when_orgName_is_provided_then_it_is_retrieved() {
    UUID uuid = UUID.fromString("854a7ac3-b419-406e-87bd-2af8a74f793b");
    String orgName = "Hospital";

    final OrganisationEntity orgEntity = OrganisationEntity.builder()
        .uuid(uuid.toString())
        .name(orgName)
        .eas(Collections.emptyList())
        .build();
    when(organisationRepository.findByNameIgnoreCaseContaining(orgName))
        .thenReturn(orgEntity);

    Optional<String> response = service.findOrganisationByName(orgName);

    assertTrue(response.isPresent());
    assertThat(response.get()).isEqualTo(orgName);

  }

  @Test
  public void given_organisation_doesnt_exist_when_orgName_is_provided_then_empty() {
    String orgName = "Hospital";

    when(organisationRepository.findByNameIgnoreCaseContaining(orgName))
        .thenReturn(null);

    Optional<String> response = service.findOrganisationByName(orgName);

    assertFalse(response.isPresent());

  }

  @Test
  public void given_existing_ea_when_eaCode_is_provided_then_it_is_retrieved() {
    String eaCode = "EA1234";

    final EmployingAuthorityEntity eaEntity = EmployingAuthorityEntity.builder()
        .eaCode(eaCode)
        .build();
    when(employingAuthorityRepository.findByEaCode(eaCode))
        .thenReturn(eaEntity);

    Optional<String> response = service.findEaByCode(eaCode);

    assertTrue(response.isPresent());
    assertThat(response.get()).isEqualTo(eaCode);

  }

  @Test
  public void given_ea_doesnt_exist_when_eaCode_is_provided_then_empty() {
    String eaCode = "EA1234";

    when(employingAuthorityRepository.findByEaCode(eaCode))
        .thenReturn(null);

    Optional<String> response = service.findEaByCode(eaCode);

    assertFalse(response.isPresent());

  }

}