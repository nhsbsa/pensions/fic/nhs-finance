package uk.nhs.nhsbsa.finance.employerdetails.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.EmployerTypes;
import com.nhsbsa.security.CreateEaRequest;
import com.nhsbsa.security.CreateOrganisationRequest;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientResponseException;
import uk.nhs.nhsbsa.finance.employerdetails.service.EmployerDetailsService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView.UserBuilder;

/**
 * Test the EmployerDetailsController
 *
 * Created by duncan on 08/03/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class EmployerDetailsControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    EmployerDetailsService employerDetailsService;

    /// GET:/organisation

    // GET:/organsation is not permitted;
    // POST:/organisation is ok
    @Test
    public void when_get_organisation_invoked_then_405_response_is_received() {
        ResponseErrorHandler originalErrorHandler = this.restTemplate.getRestTemplate().getErrorHandler();
        try {
            this.restTemplate.getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler());
            ResponseEntity<OrganisationView> entity = this.restTemplate.getForEntity("/organisation", OrganisationView.class);
        } catch (HttpClientErrorException e) {
            assertThat(e.getStatusCode()).isEqualTo(HttpStatus.METHOD_NOT_ALLOWED);
            return;
        } finally {
            this.restTemplate.getRestTemplate().setErrorHandler(originalErrorHandler);
        }
        fail("Expected exception from GET; didn't receive one");
    }

    @Test
    public void when_get_organisation_invoked_with_valid_id_organisation_is_returned() {
        String eaCode = "EA12345";
        String name = "Blackpool Victoria Hospital";
        UUID id = UUID.randomUUID();

        final EmployingAuthorityView eaEntity = EmployingAuthorityView.builder()
            .eaCode(eaCode)
            .name("")
            .employerType(EmployerTypes.STAFF.name())
            .build();
        List<EmployingAuthorityView> eaList = Collections.singletonList(eaEntity);

        when(employerDetailsService.findOrganisation(id))
            .thenReturn(
                Optional.of(
                    OrganisationView.builder()
                        .identifier(id)
                        .eas(eaList)
                        .name(name)
                        .build()
                )
            );

        ResponseEntity<OrganisationView> entity = this.restTemplate.getForEntity("/organisation/{orgId}", OrganisationView.class, id.toString());
        OrganisationView org = entity.getBody();
        assertThat(org.getEas().get(0).getEaCode()).isEqualTo(eaCode);
        assertThat(org.getName()).isEqualTo(name);
    }

    @Test
    public void when_get_organisation_invoked_with_invalid_id_no_content_status_is_returned() {

        UUID id = UUID.randomUUID();
        when(employerDetailsService.findOrganisation(id)).thenReturn(Optional.empty());
        ResponseEntity<OrganisationView> response = this.restTemplate.
            getForEntity("/organisation/{orgId}", OrganisationView.class, id.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void when_get_user_invoked_with_valid_user_id_then_user_is_returned() {

        UUID id = UUID.randomUUID();
        String contactNumber = "01237611978";
        String emailAddress = "alison.moore@example.com";
        String name = "Alison";
        String lastName = "Moore";

        when(employerDetailsService.findUser(id))
            .thenReturn(
                Optional.of(
                    UserView.builder()
                        .identifier(id)
                        .contactNumber(contactNumber)
                        .emailAddress(emailAddress)
                        .firstName(name)
                        .lastName(lastName)
                        .build()
                )
            );

        ResponseEntity<UserView> entity = this.restTemplate.getForEntity("/user/{userId}", UserView.class, id.toString());
        UserView user = entity.getBody();
        assertThat(user.getFirstName()).isEqualTo(name);
        assertThat(user.getLastName()).isEqualTo(lastName);
        assertThat(user.getContactNumber()).isEqualTo(contactNumber);
        assertThat(user.getEmailAddress()).isEqualTo(emailAddress);
    }

    @Test
    public void when_get_user_invoked_with_invalid_user_id_no_content_status_is_returned() {
        UUID id = UUID.randomUUID();
        when(employerDetailsService.findUser(id)).thenReturn(Optional.empty());
        ResponseEntity<?> response = this.restTemplate.getForEntity("/user/{userId}", UserView.class, id.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void when_get_user_invoked_with_valid_org_id_then_user_is_returned() {

        UUID id = UUID.randomUUID();
        String contactNumber = "01237611978";
        String emailAddress = "alison.moore@example.com";
        String name = "Alison";
        String lastName = "Moore";

        Optional<List<UserView>> userView = Optional.of(Collections.singletonList(UserView.builder()
            .identifier(id)
            .contactNumber(contactNumber)
            .emailAddress(emailAddress)
            .firstName(name)
            .lastName(lastName)
            .build()));

        when(employerDetailsService.findUserByOrgUuid(id))
            .thenReturn(userView);


        ResponseEntity<UserView[]> entity = this.restTemplate.getForEntity("/org-users/{orgUuid}", UserView[].class, id.toString());
        UserView[] user = entity.getBody();
        assertThat(user[0].getFirstName()).isEqualTo(name);
        assertThat(user[0].getLastName()).isEqualTo(lastName);
        assertThat(user[0].getContactNumber()).isEqualTo(contactNumber);
        assertThat(user[0].getEmailAddress()).isEqualTo(emailAddress);
    }

    @Test
    public void when_get_user_invoked_with_invalid_org_id_no_content_status_is_returned() {
        UUID id = UUID.randomUUID();
        when(employerDetailsService.findUserByOrgUuid(id)).thenReturn(Optional.empty());
        ResponseEntity<?> response = this.restTemplate.getForEntity("/org-users/{orgUuid}", UserView.class, id.toString());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    /// POST:/organisation/{id}/user

    @Test
    public void given_post_user_in_organisation_invoked_when_body_is_not_valid_then_400() throws IOException {
        final UUID orgId = UUID.fromString("0002-00-00-00-123456");
        final String name = "The Donald";
        final String email = "donald.trump@example.com";
        final String number = "0123456789";

        OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgId.toString())).name("").eas(Collections.emptyList()).build();
        List<OrganisationView> orgEntityList = Collections.singletonList(orgView);
        UserView user = UserView.builder()
                .organisations(orgEntityList)
                //.fullName(name)  - skip name for purposes of test
                .contactNumber(number)
                .emailAddress(email)
                .build();
        ResponseEntity<UserView> response = this.restTemplate.postForEntity("/organisation/0002-00-00-00-123456/user", user, UserView.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

    @Test
    public void given_post_user_in_organisation_invoked_when_email_is_not_valid_then_400() {
        final UUID orgId = UUID.fromString("71aa593a-fe5c-4ff0-b09f-7805d4f591ec");
        final String name = "The Donald";
        final String email = "donald.trump_example.com";
        final String number = "0123456789";
        OrganisationView orgView = OrganisationView.builder().identifier(UUID.fromString(orgId.toString())).name("").eas(Collections.emptyList()).build();
        List<OrganisationView> orgEntityList = Collections.singletonList(orgView);

        UserView user = UserView.builder()
                .organisations(orgEntityList)
                .firstName("The")
                .lastName("Donald")
                .contactNumber(number)
                .emailAddress(email)
                .build();
        ResponseErrorHandler originalErrorHandler = this.restTemplate.getRestTemplate().getErrorHandler();
        try {
            this.restTemplate.getRestTemplate().setErrorHandler(new DefaultResponseErrorHandler());
            ResponseEntity<UserView> response = this.restTemplate.postForEntity("/organisation/71aa593a-fe5c-4ff0-b09f-7805d4f591ec/user", user, UserView.class);
        } catch (RestClientResponseException e) {
            assertThat(e.getRawStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST.value());
            return;
        } finally {
            this.restTemplate.getRestTemplate().setErrorHandler(originalErrorHandler);
        }
        fail("Expected exception from POST; didn't receive one");
    }

    @Test
    public void given_post_organisation_user_invoked_when_body_is_valid_user_created() {
        final UUID orgId = UUID.randomUUID();
        final UUID userId = UUID.randomUUID();
        final String firstName = "Frodo";
        final String lastName = "Baggins";
        final String email = "frodo@example.com";
        final String number = "0234567891";
        final EmployingAuthorityView eaEntity = EmployingAuthorityView.builder()
            .eaCode("")
            .name("")
            .employerType(EmployerTypes.STAFF.name())
            .build();
        List<EmployingAuthorityView> eaList = Collections.singletonList(eaEntity);
        OrganisationView orgView = OrganisationView.builder().identifier(orgId).name("").eas(eaList).build();
        List<OrganisationView> orgEntityList = Collections.singletonList(orgView);

        UserBuilder userBuilder = UserView.builder()
            .organisations(orgEntityList)
            .firstName(firstName)
            .lastName(lastName)
            .contactNumber(number)
            .emailAddress(email);

        UserView user = userBuilder.build();

        when(employerDetailsService.storeUser(eq(user)))
            .thenReturn(userBuilder.identifier(userId).build());

        ResponseEntity<UserView> response = this.restTemplate.postForEntity("/organisation/{orgId}/user", user, UserView.class, orgId.toString());
        assertThat(response.getHeaders().getLocation()).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        // Assert that the returned resource has updated details
        UserView fromPost = response.getBody();
        assertThat(fromPost).isNotNull();
        assertThat(fromPost.getOrganisations().get(0).getIdentifier()).isEqualTo(orgId);
        assertThat(fromPost.getFirstName()).isEqualTo(firstName);
        assertThat(fromPost.getLastName()).isEqualTo(lastName);
        assertThat(fromPost.getContactNumber()).isEqualTo(number);
        assertThat(fromPost.getEmailAddress()).isEqualTo(email);
        assertThat(fromPost.getIdentifier()).isEqualTo(userId);
        assertThat(fromPost.getOrganisations().get(0).getEas().get(0).getEmployerType()).isEqualTo(EmployerTypes.STAFF.name());
    }

    @Test
    public void when_get_organisation_by_eaCode_invoked_with_valid_id_organisation_is_returned() {
        String eaCode = "EA12345";
        String accountName = "Blackpool Victoria Hospital";
        String employerType = "STAFF";
        UUID id = UUID.randomUUID();
        EmployingAuthorityView employingAuthorityView = EmployingAuthorityView.builder().eaCode(eaCode).name(accountName).employerType(employerType).build();
        List<EmployingAuthorityView> eaList = Collections.singletonList(employingAuthorityView);

        when(employerDetailsService.findOrganisationByEaCode(eaCode))
            .thenReturn(
                Optional.of(
                    OrganisationView.builder()
                        .identifier(id)
                        .eas(eaList)
                        .name(accountName)
                        .build()
                )
            );

        ResponseEntity<OrganisationView> entity = this.restTemplate.getForEntity("/organisation/ea-code/{eaCode}", OrganisationView.class, eaCode);
        OrganisationView org = entity.getBody();

        assertThat(org.getEas().get(0).getEaCode()).isEqualTo(eaCode);
        assertThat(org.getEas().get(0).getName()).isEqualTo(accountName);
        assertThat(org.getEas().get(0).getEmployerType()).isEqualTo(employerType);
        assertThat(org.getName()).isEqualTo(accountName);
    }

    @Test
    public void when_get_organisation_by_eaCode_invoked_with_invalid_id_no_content_status_is_returned() {
        String eaCode = "EA_CODE";
        when(employerDetailsService.findOrganisationByEaCode(eaCode)).thenReturn(Optional.empty());
        ResponseEntity<OrganisationView> response = this.restTemplate.
            getForEntity("/organisation/ea-code/{eaCode}", OrganisationView.class, eaCode);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }


    @Test
    public void when_get_employer_type_by_eaCode_invoked_with_valid_ea_emptype_is_returned() {
        String eaCode = "EA1234";
        String empType = EmployerTypes.STAFF.name();

        when(employerDetailsService.findEmployerTypeByEaCode(eaCode))
            .thenReturn(
                Optional.of(empType));

        ResponseEntity<String> entity = this.restTemplate.getForEntity("/employer-type/{eaCode}", String.class, eaCode);
        String response = entity.getBody();

        assertThat(response).isEqualTo(empType);
    }

    @Test
    public void when_get_employer_type_by_eaCode_invoked_with_invalid_ea_no_content_is_returned() {
        String eaCode = "EA1234";

        when(employerDetailsService.findEmployerTypeByEaCode(eaCode))
            .thenReturn(
                Optional.empty());

        ResponseEntity<String> entity = this.restTemplate.getForEntity("/employer-type/{eaCode}", String.class, eaCode);

        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    public void given_post_organisation_by_eacode_user_then_valid_user_is_created() {
        final UUID userId = UUID.randomUUID();
        final UUID organisationId = UUID.randomUUID();
        final String email = "frodo@example.com";
        OrganisationView organisationView = OrganisationView.builder().identifier(organisationId).name("").eas(Collections.emptyList()).build();
        List<OrganisationView> orgList = Collections.singletonList(organisationView);

        CreateUserRequest createUserRequest = CreateUserRequest.builder()
            .uuid(userId.toString())
            .organisations(orgList)
            .username(email)
            .build();

        CreateUserResponse createUserResponse = CreateUserResponse.builder()
            .email(email)
            .uuid("UUID-123456789")
            .build();

        when(employerDetailsService.storeUser(createUserRequest)).thenReturn(createUserResponse);

        CreateUserResponse response = this.restTemplate
            .postForObject("/organisation/{organisationId}/standard-user", createUserRequest,
                CreateUserResponse.class, organisationId);

        assertEquals(createUserResponse.getEmail(), response.getEmail());
        assertEquals(response.getUuid(), createUserResponse.getUuid());
    }

    @Test
    public void given_post_create_organisation_then_valid_organisation_is_created() {
        final UUID organisationId = UUID.randomUUID();
        final String orgName = "Test Organisation";
        final String eaCode = "EA1234";
        final String employerType = "STAFF";
        EmployingAuthorityView eaView = EmployingAuthorityView.builder().eaCode(eaCode).name(orgName).employerType(employerType).build();
        List<EmployingAuthorityView> ealist = Collections.singletonList(eaView);

        CreateOrganisationRequest createOrgRequest = CreateOrganisationRequest.builder()
            .name(orgName)
            .eas(ealist)
            .build();

        OrganisationView organisationViewResponse = OrganisationView.builder()
            .identifier(organisationId)
            .name(orgName)
            .eas(ealist)
            .build();


        when(employerDetailsService.storeOrganisation(createOrgRequest)).thenReturn(organisationViewResponse);

        OrganisationView response = this.restTemplate
            .postForObject("/organisation", createOrgRequest,
                OrganisationView.class);

        assertEquals(organisationViewResponse.getIdentifier(), response.getIdentifier());
        assertEquals(organisationViewResponse.getName(), response.getName());
        assertEquals(organisationViewResponse.getEas().get(0).getEaCode(), response.getEas().get(0).getEaCode());
    }

  @Test
  public void given_post_create_organisation_without_ea_then_valid_organisation_is_created() {
    final UUID organisationId = UUID.randomUUID();
    final String orgName = "Test Organisation";

    CreateOrganisationRequest createOrgRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .eas(Collections.emptyList())
        .build();

    CreateOrganisationResponse organisationResponse = CreateOrganisationResponse.builder()
        .name(orgName)
        .uuid(organisationId.toString())
        .build();

    when(employerDetailsService.storeOrganisationWithoutEa(createOrgRequest))
        .thenReturn(Optional.of(organisationResponse));

    CreateOrganisationResponse response = this.restTemplate
        .postForObject("/organisation-no-ea", createOrgRequest,
            CreateOrganisationResponse.class);

    assertEquals(response.getUuid(), response.getUuid());
    assertEquals(response.getName(), response.getName());
  }

  @Test
  public void given_post_create_organisation_without_ea_invalid_then_null_returned() {
    final String orgName = "Test Organisation";

    CreateOrganisationRequest createOrgRequest = CreateOrganisationRequest.builder()
        .name(orgName)
        .eas(Collections.emptyList())
        .build();

    when(employerDetailsService.storeOrganisationWithoutEa(createOrgRequest)).thenReturn(Optional.empty());

    CreateOrganisationResponse response = this.restTemplate
        .postForObject("/organisation-no-ea", createOrgRequest,
            CreateOrganisationResponse.class);

    assertNull(response);

  }

  @Test
  public void given_post_create_ea_then_valid_ea_is_created() {
    final UUID organisationId = UUID.randomUUID();
    final String orgName = "Test Organisation";
    final String eaName = "EA Name";
    final String eaCode = "EA1234";
    final String employerType = "STAFF";
    EmployingAuthorityView eaView = EmployingAuthorityView.builder().eaCode(eaCode).name(eaName)
        .employerType(employerType).build();
    List<EmployingAuthorityView> ealist = Collections.singletonList(eaView);

    CreateEaRequest createEaRequest = CreateEaRequest.builder()
        .eaCode(eaCode)
        .eaName(eaName)
        .employerType(employerType)
        .organisationUuid(organisationId.toString())
        .build();

    OrganisationView organisationResponse = OrganisationView.builder()
        .name(orgName)
        .identifier(organisationId)
        .eas(ealist)
        .build();

    when(employerDetailsService.storeEa(createEaRequest))
        .thenReturn(Optional.of(organisationResponse));

    OrganisationView response = this.restTemplate
        .postForObject("/employing-authority", createEaRequest,
            OrganisationView.class);

    assertEquals(response.getName(), orgName);
    assertEquals(response.getIdentifier(), organisationId);
    assertEquals(response.getEas().get(0), response.getEas().get(0));
  }

  @Test
  public void given_post_create_ea_invalid_then_null_returned() {
    final UUID organisationId = UUID.randomUUID();
    final String eaName = "EA Name";
    final String eaCode = "EA1234";
    final String employerType = "STAFF";

    CreateEaRequest createEaRequest = CreateEaRequest.builder()
        .eaCode(eaCode)
        .eaName(eaName)
        .employerType(employerType)
        .organisationUuid(organisationId.toString())
        .build();

    when(employerDetailsService.storeEa(createEaRequest)).thenReturn(Optional.empty());

    OrganisationView response = this.restTemplate
        .postForObject("/employing-authority", createEaRequest,
            OrganisationView.class);

    assertNull(response);

  }

  @Test
  public void when_get_organisation_invoked_with_valid_name_organisation_is_returned() {
      String name = "Blackpool Victoria Hospital";
      CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
          .name("Blackpool Victoria Hospital")
          .build();

    when(employerDetailsService.findOrganisationByName(orgRequest.getName()))
        .thenReturn(Optional.of(name));

    String entity = this.restTemplate.postForObject("/organisation-name", orgRequest, String.class);
    assertThat(entity).isEqualTo(name);
  }

  @Test
  public void when_get_organisation_invoked_with_invalid_name_organisation_not_returned() {
    String name = "Blackpool Victoria Hospital";
    CreateOrganisationRequest orgRequest = CreateOrganisationRequest.builder()
        .name("Blackpool Victoria Hospital")
        .build();

    when(employerDetailsService.findOrganisationByName(name))
        .thenReturn(Optional.empty());

    String entity = this.restTemplate.postForObject("/organisation-name", orgRequest, String.class);

    assertNull(entity);
  }

  @Test
  public void when_get_ea_by_eaCode_invoked_with_valid_code_ea_is_returned() {
    String eaCode = "EA12345";

    when(employerDetailsService.findEaByCode(eaCode))
        .thenReturn(Optional.of(eaCode));

    ResponseEntity<String> entity = this.restTemplate.getForEntity("/employing-authority/{eaCode}", String.class, eaCode);
    String response = entity.getBody();

    assertThat(response).isEqualTo(eaCode);
  }

  @Test
  public void when_get_ea_by_eaCode_invoked_with_invalid_code_no_content_status_is_returned() {
    String eaCode = "EA_CODE";
    when(employerDetailsService.findEaByCode(eaCode)).thenReturn(Optional.empty());

    ResponseEntity<String> response = this.restTemplate.
        getForEntity("/employing-authority/{eaCode}", String.class, eaCode);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
  }

}