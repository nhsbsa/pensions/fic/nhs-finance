package uk.nhs.nhsbsa.finance.employerdetails.repository;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;
import uk.nhs.nhsbsa.finance.employerdetails.entity.OrganisationEntity;

import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * FinanceUser repository integration tests
 *
 * Created by marklishman on 22/03/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class FinanceUserTests {

    private static Validator validator;

    @Before
    public void setUp(){
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    @Test
    public void given_correct_entity_data_pass_validation() {

        OrganisationEntity organisationEntity = OrganisationEntity.builder()
            .uuid(UUID.randomUUID().toString())
            .name("Test Organisation")
            .eas(Collections.emptyList()).build();
        List<OrganisationEntity> orgList = Collections.singletonList(organisationEntity);
        FinanceUserEntity entity = FinanceUserEntity.builder()
            .id(1L)
            .uuid(UUID.randomUUID().toString())
            .emailAddress("sam.jones@email.com")
            .firstName("Sam")
            .lastName("Jones")
            .telephoneNumber("123456789")
            .organisations(orgList)
            .build();

        Set<ConstraintViolation<FinanceUserEntity>> validate = validator.validate(entity);
        assertThat(validate, is(empty()));
    }



    @Test
    public void given_missing_entity_uuid_fail_validation() {

        final FinanceUserEntity financeUser = FinanceUserEntity
                .builder()
                .emailAddress("name@mail.com")
                .build();

        Set<ConstraintViolation<FinanceUserEntity>> validate = validator.validate(financeUser);
        assertThat(validate.size(), is(1));
        assertThat(validate.iterator().next().getPropertyPath().toString(), is("uuid"));
    }
}