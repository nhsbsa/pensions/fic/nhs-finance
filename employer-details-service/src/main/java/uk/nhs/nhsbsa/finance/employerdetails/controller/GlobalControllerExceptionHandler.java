package uk.nhs.nhsbsa.finance.employerdetails.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import uk.nhs.nhsbsa.finance.employerdetails.EmployerDetailsException;

/**
 * Controller advice for all controllers; provides consistent error handling
 * for all controller methods.
 *
 * Created by duncan on 20/03/2017.
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    /**
     * Thrown from the controller(s) when an incoming request has binding
     * and / or validation errors. When the validation is invoked in the
     * service for mapping database entities onto view objects, any
     * validation errors cause {@code ValidationException} instances to be
     * thrown, which generate 500 responses for the client.
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(EmployerDetailsException.class)
    public void handleValidationFailure() {
        // Nothing to do
    }
}
