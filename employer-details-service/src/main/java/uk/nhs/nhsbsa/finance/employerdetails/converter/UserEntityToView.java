package uk.nhs.nhsbsa.finance.employerdetails.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;
import com.nhsbsa.view.OrganisationView;
import uk.nhs.nhsbsa.finance.employerdetails.view.UserView;

import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Convert a {@code FinanceUserEntity} to a {@code UserView}. The view
 * will have its members validated prior to returning. If the validation
 * fails, a ValidationException will be thrown.
 *
 * Created by duncan on 17/03/2017.
 */
@Component
public class UserEntityToView implements Converter<FinanceUserEntity, UserView> {

    private Validator validator;
    private OrganisationEntityToView orgConverter;

    public UserEntityToView(Validator validator,
        OrganisationEntityToView organisationEntityToView) {
        this.validator = validator;
        this.orgConverter = organisationEntityToView;
    }

    /**
     *
     * @param source Entity to convert
     * @return View from Entity
     * @throws ValidationException if the entity contains values that could
     * not be validated when used as a member in the view.
     */
    @Override
    public UserView convert(FinanceUserEntity source) {
        // Spring guarantees not to call this method with null, but it's not being
        // called by Spring
        if (source == null) {
            return null;
        }

        List<OrganisationView> organisations = source.getOrganisations().stream()
            .map(orgConverter::convert).collect(Collectors.toList());

        UserView view = UserView.builder()
                    .emailAddress(source.getEmailAddress())
                    .contactNumber(source.getTelephoneNumber())
                    .firstName(source.getFirstName())
                    .lastName(source.getLastName())
                    .identifier(UUID.fromString(source.getUuid()))
                    .organisations(organisations)
                    .build();
        return validateView(view);
    }

    private UserView validateView(UserView view) {
        Set<ConstraintViolation<UserView>> violations = validator.validate(view);
        if (violations.isEmpty()) {
            return view;
        }
        String message = violations.stream()
                .map(v -> v.getPropertyPath() + ": " + v.getMessage())
                .collect(Collectors.joining(","));
        throw new ValidationException("Error validating User - " + message);
    }

}