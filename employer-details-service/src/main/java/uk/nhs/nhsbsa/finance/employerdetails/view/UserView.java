package uk.nhs.nhsbsa.finance.employerdetails.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nhsbsa.view.OrganisationView;
import java.util.List;
import lombok.Builder;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Model a user.
 *
 * Created by duncan on 09/03/2017.
 */
@lombok.Value
@Builder(builderClassName = "UserBuilder")
public final class UserView {

    private final UUID identifier;

    @NotBlank
    @Size(max = 200)
    private final String firstName;

    @NotBlank
    @Size(max = 200)
    private final String lastName;

    @NotBlank
    @Size(max = 200)
    private final String contactNumber;

    @Email
    @NotBlank
    @Size(max = 200)
    private final String emailAddress;

    @NotNull
    private final List<OrganisationView> organisations;

    @JsonCreator
    public static UserView jsonFactory(@JsonProperty("identifier") UUID id,
                                       @JsonProperty("firstName") String firstName,
                                       @JsonProperty("lastName") String lastName,
                                       @JsonProperty("contactNumber") String contactNumber,
                                       @JsonProperty("emailAddress") String emailAddress,
                                       @JsonProperty("organisation") List<OrganisationView> organisations) {
        return UserView.builder()
                .identifier(id)
                .firstName(firstName)
                .lastName(lastName)
                .contactNumber(contactNumber)
                .emailAddress(emailAddress)
                .organisations(organisations)
                .build();
    }

    /**
     * Return a new immutable {@code UserView} instance with the identifier
     * provided.
     *
     * @param identifier
     * @return
     */
    public UserView withIdentifier(UUID identifier) {
        return UserView.builder()
                  .identifier(identifier)
                  .firstName(this.getFirstName())
                  .lastName(this.getLastName())
                  .contactNumber(this.getContactNumber())
                  .emailAddress(this.getEmailAddress())
                  .organisations(this.getOrganisations())
                  .build();
    }
}
