package uk.nhs.nhsbsa.finance.employerdetails.converter;

import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import uk.nhs.nhsbsa.finance.employerdetails.entity.EmployingAuthorityEntity;
import com.nhsbsa.view.EmployingAuthorityView;

/**
 * Convert a {@code OrganisationEntity} to an {@code EmployingAuthorityView}. The view
 * will have its members validated prior to returning. If the validation
 * fails, a ValidationException will be thrown.
 *
 * Created by duncan on 17/03/2017.
 */
@Component
public class EmployingAuthorityEntityToView implements Converter<EmployingAuthorityEntity, EmployingAuthorityView> {

    private Validator validator;

    public EmployingAuthorityEntityToView(Validator validator) {
        this.validator = validator;
    }

    /**
     *
     * @param source Entity to convert
     * @return View from Entity
     * @throws ValidationException if the entity contains values that could
     * not be validated when used as a member in the view.
     */
    @Override
    public EmployingAuthorityView convert(EmployingAuthorityEntity source) {
        // Spring guarantees not to call this method with null, but it's not being
        // called by Spring
        if (source == null) {
            return null;
        }
        EmployingAuthorityView view = EmployingAuthorityView.builder()
                .eaCode(source.getEaCode())
                .name(source.getName())
                .employerType(source.getEmployerType())

                .build();
        return validateView(view);
    }

    private EmployingAuthorityView validateView(EmployingAuthorityView view) {
        Set<ConstraintViolation<EmployingAuthorityView>> violations = validator.validate(view);
        if (violations.isEmpty()) {
            return view;
        }
        String message = violations.stream()
                .map(v -> v.getPropertyPath() + ": " + v.getMessage())
                .collect(Collectors.joining(","));
        throw new ValidationException("Error validating EA - " + message);
    }

}
