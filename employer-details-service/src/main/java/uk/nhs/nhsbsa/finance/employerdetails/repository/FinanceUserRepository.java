package uk.nhs.nhsbsa.finance.employerdetails.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uk.nhs.nhsbsa.finance.employerdetails.entity.FinanceUserEntity;

public interface FinanceUserRepository extends JpaRepository<FinanceUserEntity, Long> {

    FinanceUserEntity findByUuid(String uuid);

    @Query("SELECT user FROM FinanceUserEntity user join user.organisations org WHERE org.uuid = :orgUuid")
    List<FinanceUserEntity> findByOrgUuid(@Param("orgUuid") String orgUuid);
}
