/**
 * Data / Value Objects that are intended to be mapped to and
 * from JSON.
 *
 * Created by duncan on 09/03/2017.
 */
package uk.nhs.nhsbsa.finance.employerdetails.view;