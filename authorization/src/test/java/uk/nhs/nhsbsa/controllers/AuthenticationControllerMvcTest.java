package uk.nhs.nhsbsa.controllers;

import com.nhsbsa.model.UserRoles;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import uk.nhs.nhsbsa.service.UserAuthenticationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static uk.nhs.nhsbsa.rest.RestAuthentication.AUTH_APP_ID;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Mark Lishman on 12/01/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AuthenticationControllerMvcTest {

    public static final String APPLICATION_ID = "application-id";
    public static final String USER_NAME = "user-name";
    public static final String PASSWORD = "password";
    public static final String USER_UUID = "user-uuid";

    public static final String VALID_CONTENT = "{" +
            "\"username\" : \"" + USER_NAME + "\"," +
            "\"password\" : \"" + PASSWORD + "\"" +
            "}";
    public static final String INVALID_CONTENT = "{" +
            "\"username\" : \"" + USER_NAME + "\"" +
            "}";
    public static final String SUCCESS_RESPONSE = "{" +
            "\"uuid\":\"user-uuid\"," +
            "\"token\":\"OAUTH2-TOKEN\"," +
            "\"firstLogin\":false," +
            "\"role\":\"ROLE_STANDARD\"," +
            "\"authenticationStatus\":\"AUTH_SUCCESS\"" +
            "}";

    public static final String FAILURE_RESPONSE = "{" +
            "\"uuid\":null," +
            "\"token\":null," +
            "\"firstLogin\":null," +
            "\"role\":null," +
            "\"authenticationStatus\":\"AUTH_FAILURE\"}";

    private MockMvc mvc;

    @Mock
    private UserAuthenticationService userAuthenticationService;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders
                .standaloneSetup(new AuthenticationController(userAuthenticationService))
                .build();
    }

    @Test
    public void userIsAuthenticated() throws Exception {

        final User savedUser = User
                .builder()
                .uuid(USER_UUID)
                .role(UserRoles.ROLE_STANDARD.name())
                .build();

        final AuthenticationResponse response = AuthenticationResponse
            .builder()
            .uuid(savedUser.getUuid())
            .role(savedUser.getRole())
            .token("OAUTH2-TOKEN")
            .firstLogin(false)
            .authenticationStatus(AuthenticationStatus.AUTH_SUCCESS)
            .build();



        given(userAuthenticationService.authenticateUser(APPLICATION_ID, USER_NAME, PASSWORD)).willReturn(response);

        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(VALID_CONTENT))
                .andExpect(status().isOk())
                .andExpect(content().string(is(equalTo(SUCCESS_RESPONSE))));
    }

    @Test
    public void userIsNotRegisteredIfRequestDataIsInvalid() throws Exception {

        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(INVALID_CONTENT))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userIsNotRegisteredIfAppIdIsMissing() throws Exception {

        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(VALID_CONTENT))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(isEmptyString()));
    }

    @Test
    public void userIsNotRegisteredIfDataCannotBeSaved() throws Exception {

        given(userAuthenticationService.authenticateUser(APPLICATION_ID, USER_NAME, PASSWORD))
            .willReturn(
                AuthenticationResponse.builder()
                    .authenticationStatus(AuthenticationStatus.AUTH_FAILURE)
                    .build()
            );

        mvc.perform(MockMvcRequestBuilders.post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .header(AUTH_APP_ID, APPLICATION_ID)
                .content(VALID_CONTENT))
                .andExpect(status().isOk())
                .andExpect(content().string(FAILURE_RESPONSE));
    }
}