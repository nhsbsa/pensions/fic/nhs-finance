package uk.nhs.nhsbsa.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.model.UserPasswordHistory;
import uk.nhs.nhsbsa.repos.UserPasswordHistoryRepository;
import uk.nhs.nhsbsa.repos.UserRepository;
import uk.nhs.nhsbsa.security.SetPasswordRequest;

/**
 * Created by nataliehulse on 10/10/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UserUpdateDetailsServiceTest {

    public static final String HASH = "hash";
    public static final String UUID = "user-uuid";
    public static final String NEW_PASSWORD = "Testing123";
    public static final boolean FIRST_LOGIN = false;
    public static final boolean ACCOUNT_LOCKED = false;

    public static final String HASH_NEW = "hash_new";
    public static final Long UPH_ID = 1L;
    public static final Long USER_ID = 1L;
    public static final String NAME = "Testing@Test";

    private UserUpdateDetailsService userUpdateDetailsService;

    @Mock
    private UserRepository userRepository;
    @Mock
    private PasswordService passwordService;
    @Mock
    private UserPasswordHistoryRepository userPasswordHistoryRepository;

    @Before
    public void before() {
        userUpdateDetailsService = new UserUpdateDetailsService(passwordService, userRepository, userPasswordHistoryRepository);
    }

    @Test
    public void newPasswordIsSaved() {
        given(passwordService.generatePasswordHash(NEW_PASSWORD)).willReturn(HASH);

        final User user = User.builder().uuid(UUID).hash(HASH).firstLogin(FIRST_LOGIN).locked(true).build();

        given(userRepository.findByUuid(UUID)).willReturn(user);

        final User savedUser = User.builder().uuid(UUID).firstLogin(FIRST_LOGIN).hash(HASH).locked(ACCOUNT_LOCKED).build();
        given(userRepository.save(user)).willReturn(savedUser);

        final UserPasswordHistory savedUserPasswordHistory = UserPasswordHistory.builder().userId(USER_ID).hash(HASH).name(NAME).build();
        given(userUpdateDetailsService.saveUserPasswordHistory(user)).willReturn(savedUserPasswordHistory);

        final SetPasswordRequest request = SetPasswordRequest.builder().uuid(UUID).newPassword(NEW_PASSWORD).firstLogin(false).build();
        User response = userUpdateDetailsService.updatePassword(request);

        assertThat(response.getUuid(), is(UUID));
        assertThat(response.isFirstLogin(), is(FIRST_LOGIN));
        assertThat(response.getHash(), is(HASH));
        assertThat(response.isLocked(), is(ACCOUNT_LOCKED));
    }

    @Test
    public void newPasswordIsNotSaved() {
        given(passwordService.generatePasswordHash(NEW_PASSWORD)).willReturn(HASH);

        final User user = User
            .builder()
            .uuid(UUID)
            .hash(HASH)
            .firstLogin(FIRST_LOGIN)
            .locked(ACCOUNT_LOCKED)
            .build();
        given(userRepository.findByUuid(UUID)).willReturn(user);

        given(userRepository.save(user)).willReturn(null);

        final SetPasswordRequest request = SetPasswordRequest.builder().uuid(UUID).newPassword(NEW_PASSWORD).firstLogin(false).build();
        User response = userUpdateDetailsService.updatePassword(request);

        assertThat(response, is(nullValue()));

    }

    @Test
    public void passwordHistoryIsSaved() {

        final User user = User.builder().id(USER_ID).name(NAME).hash(HASH).build();

        final UserPasswordHistory userPasswordHistory = UserPasswordHistory.builder().userId(USER_ID).name(NAME).hash(HASH).build();

        final UserPasswordHistory savedUserPasswordHistory = UserPasswordHistory.builder().id(UPH_ID).userId(USER_ID).name(NAME).hash(HASH).build();
        given(userPasswordHistoryRepository.save(userPasswordHistory)).willReturn(savedUserPasswordHistory);

        UserPasswordHistory response = userUpdateDetailsService.saveUserPasswordHistory(user);

        assertThat(response.getUserId(), is(USER_ID));
        assertThat(response.getName(), is(NAME));
        assertThat(response.getHash(), is(HASH));
    }

    @Test
    public void passwordHistoryIsNotSaved() {

        final User user = User.builder().id(USER_ID).name(NAME).hash(HASH).build();

        final UserPasswordHistory userPasswordHistory = UserPasswordHistory.builder().userId(USER_ID).name(NAME).hash(HASH).build();

        given(userPasswordHistoryRepository.save(userPasswordHistory)).willReturn(null);

        UserPasswordHistory response = userUpdateDetailsService.saveUserPasswordHistory(user);

        assertThat(response, is(nullValue()));
    }

    @Test
    public void oldPasswordIsSavedIntoPasswordHistory() {

        SetPasswordRequest setPasswordRequest = SetPasswordRequest.builder().uuid(UUID).firstLogin(FIRST_LOGIN).newPassword(HASH_NEW).build();
        final User user = User.builder().id(USER_ID).name(NAME).hash(HASH).build();

        given(passwordService.generatePasswordHash(NEW_PASSWORD)).willReturn(HASH_NEW);
        given(userRepository.findByUuid(UUID)).willReturn(user);

        final UserPasswordHistory userPasswordHistory = UserPasswordHistory.builder().userId(user.getId()).name(user.getName()).hash(user.getHash()).build();
        final UserPasswordHistory savedUserPasswordHistory = UserPasswordHistory.builder().id(UPH_ID).userId(USER_ID).name(NAME).hash(HASH).build();
        given(userPasswordHistoryRepository.save(userPasswordHistory)).willReturn(savedUserPasswordHistory);

        UserPasswordHistory savedPasswordHistory = userUpdateDetailsService.saveUserPasswordHistory(user);

        final User savedUser = User.builder().id(USER_ID).name(NAME).hash(HASH_NEW).build();
        given(userRepository.save(user)).willReturn(savedUser);

        User newUserDetails = userUpdateDetailsService.updatePassword(setPasswordRequest);

        assertThat(newUserDetails.getHash(), is(HASH_NEW));
        assertThat(savedPasswordHistory.getHash(), is(HASH));

    }

    @Test
    public void testUpdatePasswordReturnsNull() {

        SetPasswordRequest setPasswordRequest = SetPasswordRequest.builder()
            .uuid(UUID)
            .firstLogin(FIRST_LOGIN)
            .newPassword(HASH_NEW)
            .build();

        given(passwordService.generatePasswordHash(NEW_PASSWORD)).willReturn(HASH_NEW);
        given(userRepository.findByUuid(UUID)).willReturn(null);

        User newUserDetails = userUpdateDetailsService.updatePassword(setPasswordRequest);

        assertNull(newUserDetails);

    }

    @Test
    public void newPasswordSameAsPreviousValueIsReturned() {
        final User user = User
            .builder()
            .uuid(UUID)
            .hash(HASH)
            .build();
        given(userRepository.findByUuid(UUID)).willReturn(user);

        final SetPasswordRequest setPasswordRequest = SetPasswordRequest
            .builder()
            .uuid(UUID)
            .newPassword(NEW_PASSWORD)
            .build();

        given(passwordService.isValidPassword(anyString(), anyString()))
            .willReturn(true);

        final boolean isNewPasswordSameAsPrevious = userUpdateDetailsService
            .isNewPasswordSameAsPrevious(setPasswordRequest);

        assertThat(isNewPasswordSameAsPrevious, is(true));
    }

    @Test
    public void newPasswordSameAsPreviousValueReturnsFalse() {
        final User user = User
            .builder()
            .uuid(UUID)
            .hash(HASH)
            .build();
        given(userRepository.findByUuid(UUID)).willReturn(user);

        final SetPasswordRequest setPasswordRequest = SetPasswordRequest
            .builder()
            .uuid(UUID)
            .newPassword(NEW_PASSWORD)
            .build();

        given(passwordService.isValidPassword(anyString(), anyString()))
            .willReturn(false);

        final boolean isNewPasswordSameAsPrevious = userUpdateDetailsService
            .isNewPasswordSameAsPrevious(setPasswordRequest);

        assertThat(isNewPasswordSameAsPrevious, is(false));
    }

    @Test
    public void newPasswordSameAsPreviousValueUserNullReturnsFalse() {

        given(userRepository.findByUuid(UUID)).willReturn(null);

        final SetPasswordRequest setPasswordRequest = SetPasswordRequest
            .builder()
            .uuid(UUID)
            .newPassword(NEW_PASSWORD)
            .build();

        final boolean isNewPasswordSameAsPrevious = userUpdateDetailsService
            .isNewPasswordSameAsPrevious(setPasswordRequest);

        assertThat(isNewPasswordSameAsPrevious, is(false));
    }

}