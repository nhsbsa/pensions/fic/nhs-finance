package uk.nhs.nhsbsa.service;

import com.nhsbsa.model.AuthorizedApplication;
import com.nhsbsa.model.User;
import com.nhsbsa.security.RegistrationRequest;
import uk.nhs.nhsbsa.repos.ApplicationRepository;
import uk.nhs.nhsbsa.repos.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Created by Mark Lishman on 05/01/2017.
 */
@PrepareForTest({ UserRegistrationService.class })
@RunWith(PowerMockRunner.class)
public class UserRegistrationServiceTest {

    public static final String APP_NAME = "app-name";
    public static final String USER_NAME = "user-name";
    public static final String USER_UUID = "user-uuid";
    public static final String PASSWORD = "password";
    public static final String PASSWORD_HASH = "password-hash";
    public static final String NINO = "AA123456789";
    private UserRegistrationService userRegistrationService;

    @Mock private PasswordService passwordService;
    @Mock private UserRepository userRepository;
    @Mock private ApplicationRepository applicationRepository;
    @Mock private UUID userUuid;

    @Before
    public void before() {
        userRegistrationService = new UserRegistrationService(
                passwordService,
                userRepository,
                applicationRepository
        );
    }

    @Test
    public void userIsSaved() {
        final AuthorizedApplication app = AuthorizedApplication
                .builder()
                .name(APP_NAME)
                .build();
        given(applicationRepository.findByName(APP_NAME)).willReturn(app);

        given(passwordService.generatePasswordHash(PASSWORD)).willReturn(PASSWORD_HASH);

        given(userUuid.toString()).willReturn(USER_UUID);
        mockStatic(UUID.class);
        given(UUID.randomUUID()).willReturn(userUuid);

        final User savedUser = User.builder().build();
        final ArgumentCaptor<User> argumentCaptor = ArgumentCaptor.forClass(User.class);
        given(userRepository.save(argumentCaptor.capture())).willReturn(savedUser);

        final RegistrationRequest request = RegistrationRequest
                .builder()
                .username(USER_NAME)
                .password(PASSWORD)
                .build();

        final User actualUser = userRegistrationService.register(APP_NAME, request);

        assertThat(actualUser, is(equalTo(savedUser)));
        final User userArgument = argumentCaptor.getValue();
        assertThat(userArgument.getName(), is(equalTo(USER_NAME)));
        assertThat(userArgument.getHash(), is(equalTo(PASSWORD_HASH)));
        assertThat(userArgument.getUuid(), is(equalTo(USER_UUID)));
        List<AuthorizedApplication> expectedApps = userArgument.getApplications();
        assertThat(expectedApps, hasSize(1));
        assertThat(expectedApps.get(0).getName(), is(equalTo(APP_NAME)));
    }

    @Test
    public void userIsNotSavedIfApplicationNotFound() {
        final RegistrationRequest request = mock(RegistrationRequest.class);

        final User actualUser = userRegistrationService.register(APP_NAME, request);

        verify(passwordService, never()).generatePasswordHash(anyString());
        verify(userRepository, never()).save((User) anyObject());
        assertThat(actualUser, is(nullValue()));
    }
}