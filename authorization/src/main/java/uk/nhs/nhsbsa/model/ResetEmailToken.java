package uk.nhs.nhsbsa.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by nathulse on 11/12/2017.
 * Reset Email Token
 */

@Builder
@Data
@Entity
@Table(name = "reset_email_token")
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = {"id"})
public class ResetEmailToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ret_id", insertable = false, updatable = false)
    private Long id;

    private Long userId;
    private String token;
    private LocalDateTime createdDate;

}
