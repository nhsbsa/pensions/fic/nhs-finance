package uk.nhs.nhsbsa;

import uk.nhs.nhsbsa.config.SwaggerConfig;
import uk.nhs.nhsbsa.config.AppConfig;
import com.nhsbsa.model.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.EndpointMBeanExportAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@Import({AppConfig.class, SwaggerConfig.class})
@EnableAutoConfiguration(exclude = {EndpointMBeanExportAutoConfiguration.class})
@EntityScan(basePackageClasses = {Application.class, Jsr310JpaConverters.class, User.class})
public class Application {  

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
