package uk.nhs.nhsbsa.config;

import com.google.common.base.Predicate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.*;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by sbradley on 10/03/2017.
 */
@ConditionalOnProperty(prefix = "swagger.enable", name = "dynamic", matchIfMissing = true)
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(paths())
                .build();
    }


    private Predicate<String> paths() {
        return or(
                regex("/authenticate"),
                regex("/trs-authenticate"),
                regex("/register"),
                regex("/identity-register"),
                regex("/trs-accesscode"),
                regex("/trs-register"));
    }
}