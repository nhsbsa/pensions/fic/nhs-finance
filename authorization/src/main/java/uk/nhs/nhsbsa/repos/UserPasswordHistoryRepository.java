package uk.nhs.nhsbsa.repos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.nhsbsa.model.User;
import uk.nhs.nhsbsa.model.UserPasswordHistory;

/**
 * Created by nathulse on 30/10/2017.
 */

@Repository
public interface UserPasswordHistoryRepository extends CrudRepository<UserPasswordHistory, Long> {

    User findByName(String userName);
}
