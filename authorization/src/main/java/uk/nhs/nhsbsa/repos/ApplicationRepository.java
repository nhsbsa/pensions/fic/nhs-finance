package uk.nhs.nhsbsa.repos;

import com.nhsbsa.model.AuthorizedApplication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jeffreya on 16/08/2016.
 */


@Repository
public interface ApplicationRepository extends CrudRepository<AuthorizedApplication, Long> {

    AuthorizedApplication findByName(String name);

}
