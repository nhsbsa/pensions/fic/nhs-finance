package uk.nhs.nhsbsa.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Mark Lishman on 09/01/2017.
 */

@Slf4j
@ControllerAdvice
public class AdviceHandlerExceptionController {

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(Exception.class)
  public void handleException(final Exception exception) {
    log.error("Unrecoverable error - " + exception.getMessage(), exception);
  }
}
