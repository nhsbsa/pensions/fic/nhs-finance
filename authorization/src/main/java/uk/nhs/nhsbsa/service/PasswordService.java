package uk.nhs.nhsbsa.service;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

/**
 * Created by jeffreya on 19/08/2016.
 */
@Service
public class PasswordService {

    public String generatePasswordHash(final String password) {
        final String salt = BCrypt.gensalt(12);
        return BCrypt.hashpw(password, salt);
    }

    public boolean isValidPassword(final String password, final String passwordHash) {
        return BCrypt.checkpw(password, passwordHash);
    }

    public boolean notAValidPassword(final String password, final String passwordHash) {
        return ! isValidPassword(password, passwordHash);
    }
}
