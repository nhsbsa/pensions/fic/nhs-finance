package uk.nhs.nhsbsa.service;


import static org.assertj.core.util.DateUtil.now;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nhsbsa.model.User;

import lombok.extern.slf4j.Slf4j;
import uk.nhs.nhsbsa.repos.UserRepository;

/**
 * Created by jeffreya on 23/09/2016.
 * UserService
 */

@Slf4j
@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByUuid(final String uuid) {
        return userRepository.findByUuid(uuid);
    }
    
    public List<User> getUsersByRole(String role) {
        return userRepository.findByRole(role);
    }

    public User deleteUserByUuid(final String uuid) {

        User user = userRepository.findByUuid(uuid);
        user.setDeletedOn(now());
        userRepository.save(user);

        return user;
    }
}
