package uk.nhs.nhsbsa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by jeffreya on 23/09/2016.
 * ApplicationValidationService
 */

@Service
public class ApplicationService {

    private final PasswordService passwordService;
    private final AuthorizedApplicationService authorizedApplicationService;

    @Autowired
    public ApplicationService(final PasswordService passwordService,
                              final AuthorizedApplicationService authorizedApplicationService) {
        this.passwordService = passwordService;
        this.authorizedApplicationService = authorizedApplicationService;
    }


    public boolean isValidApplication(final String applicationID, final String applicationToken) {
        final String preHash = applicationID + applicationToken;
        final String applicationHash = authorizedApplicationService.getApplicationHash(applicationID);
        return applicationHash != null && passwordService.isValidPassword(preHash, applicationHash);
    }


}
