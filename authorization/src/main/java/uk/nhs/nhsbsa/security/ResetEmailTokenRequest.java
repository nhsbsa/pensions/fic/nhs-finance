package uk.nhs.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nataliehulse on 12/12/2017.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResetEmailTokenRequest {

    private String emailToken;


}

