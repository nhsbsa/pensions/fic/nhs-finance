package uk.nhs.nhsbsa.security;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Created by nataliehulse on 05/10/2017.
 */

@Data
@Builder
@NoArgsConstructor
public class SetPasswordRequest {

    public SetPasswordRequest(
        @JsonProperty String uuid,
        @JsonProperty Boolean firstLogin,
        @JsonProperty String newPassword
    ){
        this.uuid = uuid;
        this.firstLogin = firstLogin;
        this.newPassword = newPassword;
    }

    private String uuid;

    @NotNull
    private boolean firstLogin;

    @NotNull
    private String newPassword;

}

