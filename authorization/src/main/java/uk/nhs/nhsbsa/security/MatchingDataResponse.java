package uk.nhs.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MatchingDataResponse implements Serializable {

    private String surname;
    private String firstname;
    private String postcode;
    private String nino;

    @SuppressWarnings("squid:S3437") // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS3437
    private LocalDate dob;
}
