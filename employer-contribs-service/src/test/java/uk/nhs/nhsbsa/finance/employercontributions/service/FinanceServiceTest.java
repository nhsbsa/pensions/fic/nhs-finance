package uk.nhs.nhsbsa.finance.employercontributions.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.view.RequestForTransferView;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import uk.nhs.nhsbsa.finance.employercontributions.model.RequestForTransfer;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.TransferView;
import com.nhsbsa.view.TransferViewConverter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import org.assertj.core.api.SoftAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.web.client.RestTemplate;
import uk.nhs.nhsbsa.finance.employercontributions.repository.RequestForTransferRepository;
import uk.nhs.nhsbsa.finance.employercontributions.utility.FileDateGenerator;

/**
 * Created by Mark Lishman on 10/11/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class FinanceServiceTest {

  public static final String RFT_UUID = "123";
  private static final String EA_CODE = "EA1234";
  @Mock
  private RequestForTransferRepository requestForTransferRepository;
  @Mock
  private RestTemplate restTemplate;

  private FinanceService financeService;

  private CsvGeneratingService csvGeneratingService;

  private FileDateGenerator fileDateGenerator;

  private static final List<String> BANK_HOLIDAYS = Arrays
      .asList("01/01","07/05","28/05","27/07","25/12","26/12");

  private static final int DEADLINE_HOUR = 13;

  private static final RequestForTransfer RFT = RequestForTransfer.builder()
      .rftUuid("TEST_UUID")
      .employeeContributions(new BigDecimal("50"))
      .employerContributions(new BigDecimal("500"))
      .totalPensionablePay(new BigDecimal("550"))
      .totalDebitAmount(new BigDecimal("550"))
      .additionalPension(new BigDecimal("10"))
      .employeeAddedYears(new BigDecimal("10"))
      .errbo(new BigDecimal("0"))
      .isGp(false)
      .transferDate(TransferFormDate.builder().year("2018").month("December").days("17").build())
      .contributionDate(ContributionDate.builder().contributionMonth("December").contributionYear(2018).build())
      .transferDate(TransferFormDate.builder().year("2018").month("December").days("17").build())
      .adjustmentsRequired(false)
      .eaCode("EA1234")
      .build();

  private static final Set<RequestForTransfer> RFT_SET = Collections.singleton(RFT);


  @Before
  public void before() {
    fileDateGenerator = new FileDateGenerator(BANK_HOLIDAYS, DEADLINE_HOUR);
    csvGeneratingService = new CsvGeneratingService();

    financeService = new FinanceService(requestForTransferRepository, restTemplate, csvGeneratingService, fileDateGenerator);
    financeService.setFinanceFacadeUri("finance-facade");
  }

  @Test
  public void getRequestForTransfer() {
    RequestForTransfer expectedRft = new RequestForTransfer();
    given(requestForTransferRepository.findByRftUuid(RFT_UUID)).willReturn(expectedRft);

    RequestForTransfer actualRft = financeService.getRequestForTransferByRftUuid(RFT_UUID);

    assertThat(actualRft, is(sameInstance(expectedRft)));
  }

  @Test
  public void saveRequestForTransfer() {
    RequestForTransfer newRft = new RequestForTransfer();
    RequestForTransfer expectedRft = new RequestForTransfer();
    given(requestForTransferRepository.save(newRft)).willReturn(expectedRft);

    RequestForTransfer actualRft = financeService.saveRequestForTransfer(newRft);

    assertThat(actualRft, is(sameInstance(expectedRft)));
  }

  @Test
  public void givenEligibleRFTRecordsConvertToFacadeViewsWorks() {
    RequestForTransferView prototype = RequestForTransferView.builder()
        .isGp(true)
        .transferDate(TransferFormDate.builder()
            .days("11")
            .month("AUGUST")
            .year("2014")
            .build())
        .contributionDate(ContributionDate.builder()
            .contributionMonth("JUNE")
            .contributionYear(2012)
            .build())
        .employeeContributions(new BigDecimal("1000.00"))
        .employerContributions(new BigDecimal("750.00"))
        .build();

    TransferView view = TransferViewConverter.of(prototype);

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(view.getTransactionDate()).as("transaction date")
        .isEqualTo(LocalDate.of(2014, 8, 11));
    softly.assertThat(view.getContributionYearMonth()).as("cont year/month").isEqualTo("1206");
    softly.assertThat(view.getContributions()).as("conts").isEqualTo(new BigDecimal("1000.00"));
    softly.assertThat(view.getEmployerContributions()).as("employer conts")
        .isEqualTo(new BigDecimal("750.00"));
    softly.assertAll();

    assertEquals("G", view.getEmployerType());
  }

  @Test
  public void givenEligibleRFTRecordsConvertToFacadeViewsWorksWithEmployerStaffType() {
    RequestForTransferView prototype = RequestForTransferView.builder()
        .isGp(false)
        .transferDate(TransferFormDate.builder()
            .days("11")
            .month("AUGUST")
            .year("2014")
            .build())
        .contributionDate(ContributionDate.builder()
            .contributionMonth("JUNE")
            .contributionYear(2012)
            .build())
        .employeeContributions(new BigDecimal("1000.00"))
        .employerContributions(new BigDecimal("750.00"))
        .build();

    TransferView view = TransferViewConverter.of(prototype);

    SoftAssertions softly = new SoftAssertions();
    softly.assertThat(view.getTransactionDate()).as("transaction date")
        .isEqualTo(LocalDate.of(2014, 8, 11));
    softly.assertThat(view.getContributionYearMonth()).as("cont year/month").isEqualTo("1206");
    softly.assertThat(view.getContributions()).as("conts").isEqualTo(new BigDecimal("1000.00"));
    softly.assertThat(view.getEmployerContributions()).as("employer conts")
        .isEqualTo(new BigDecimal("750.00"));
    softly.assertAll();

    assertEquals("S", view.getEmployerType());
  }

  private Set<RequestForTransfer> getTestRFTSet() {
    RequestForTransfer prototype = RequestForTransfer.builder()
        .isGp(true)
        .transferDate(TransferFormDate.builder()
            .days("11")
            .month("AUGUST")
            .year("2014")
            .build())
        .contributionDate(ContributionDate.builder()
            .contributionMonth("JUNE")
            .contributionYear(2012)
            .build())
        .employeeContributions(new BigDecimal("1000.00"))
        .employerContributions(new BigDecimal("750.00"))
        .build();
    RequestForTransfer rft1 = prototype.toBuilder()
        .transferDate(TransferFormDate.builder()
            .days("12")
            .month("AUGUST")
            .year("2014")
            .build())
        .employerContributions(new BigDecimal("1000.00"))
        .build();
    RequestForTransfer rft2 = prototype.toBuilder()
        .transferDate(TransferFormDate.builder()
            .days("13")
            .month("AUGUST")
            .year("2014")
            .build())
        .employerContributions(new BigDecimal("1250.00"))
        .build();
    Set<RequestForTransfer> set = new HashSet<>();
    set.add(prototype);
    set.add(rft1);
    set.add(rft2);
    return set;
  }

  @Test
  public void getSubmitDateByEaCodeReturnsDate() {

    LocalDate date = LocalDate.now();
    Instant expectedDate = date.atStartOfDay(ZoneOffset.UTC).toInstant();

    given(requestForTransferRepository.findSubmitDateByEaCode(EA_CODE)).willReturn(expectedDate);

    Optional<LocalDate> actualDate = financeService.getSubmitDateByEaCode(EA_CODE);

    assertThat(actualDate.get().atStartOfDay().toInstant(ZoneOffset.UTC), is(equalTo(expectedDate)));
  }

  @Test
  public void getSubmitDateByEaCodeReturnsEmpty() {

    given(requestForTransferRepository.findSubmitDateByEaCode(EA_CODE)).willReturn(null);

    Optional<LocalDate> actualDate = financeService.getSubmitDateByEaCode(EA_CODE);

    assertFalse(actualDate.isPresent());
  }

  @Test
  public void when_get_fileList_for_saturday_then_monday_returned() {

    String[] fileList = new String[1];
    fileList[0] = "2018-12-08"; // SATURDAY

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(1, result.length);
    assertEquals("2018-12-10", result[0]); // MONDAY
  }

  @Test
  public void when_get_fileList_for_sunday_then_monday_returned() {

    String[] fileList = new String[1];
    fileList[0] = "2018-12-09"; // SUNDAY

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(1, result.length);
    assertEquals("2018-12-10", result[0]); // MONDAY
  }

  @Test
  public void when_get_fileList_for_bank_hol_then_tuesday_returned() {

    String[] fileList = new String[1];
    fileList[0] = "2018-05-28"; // MONDAY BANK HOL

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(1, result.length);
    assertEquals("2018-05-29", result[0]); // TUESDAY
  }

  @Test
  public void when_get_fileList_for_weekend_and_bank_hol_then_tuesday_returned() {

    String[] fileList = new String[1];
    fileList[0] = "2018-05-26"; // SATURDAY before Bank Hol

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(1, result.length);
    assertEquals("2018-05-29", result[0]); // TUESDAY
  }

  @Test
  public void when_get_fileList_for_different_dates_then_correct_list_returned_in_desc_order() {

    String[] fileList = new String[5];
    fileList[0] = "2018-05-25"; // FRIDAY before Bank Hol
    fileList[1] = "2018-05-26"; // SATURDAY before Bank Hol
    fileList[2] = "2018-05-27"; // SUNDAY before Bank Hol
    fileList[3] = "2018-05-28"; // BANK HOL
    fileList[4] = "2018-05-29"; // TUESDAY

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(result.length, 2);
    assertEquals("2018-05-29", result[0]); // FRIDAY
    assertEquals("2018-05-25", result[1]); // TUESDAY
  }

  @Test
  public void when_get_fileList_for_working_days_then_list_returned_in_desc_order() {

    String[] fileList = new String[2];
    fileList[0] = "2018-12-10";
    fileList[1] = "2018-12-11";

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(result.length, 2);
    assertEquals("2018-12-11", result[0]);
    assertEquals("2018-12-10", result[1]);
  }

  @Test
  public void when_get_fileList_for_today_after_deadline_hour_then_file_returned() {

    FileDateGenerator mockFileDateGenerator = new FileDateGenerator(BANK_HOLIDAYS, 1);

    FinanceService financeService = new FinanceService(requestForTransferRepository, restTemplate, csvGeneratingService, mockFileDateGenerator);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
    LocalDate now = LocalDate.now();

    String[] fileList = new String[1];
    fileList[0] = now.format(formatter);

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(1, result.length);
    assertEquals(now.format(formatter), result[0]);

  }

  @Test
  public void when_get_fileList_for_today_before_deadline_hour_then_no_file_returned() {

    FileDateGenerator mockFileDateGenerator = new FileDateGenerator(BANK_HOLIDAYS, 23);

    FinanceService financeService = new FinanceService(requestForTransferRepository, restTemplate, csvGeneratingService, mockFileDateGenerator);

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");
    LocalDate now = LocalDate.now();

    String[] fileList = new String[1];
    fileList[0] = now.format(formatter);

    given(requestForTransferRepository.getCsvFileList()).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(0, result.length);

  }

  @Test
  public void when_get_file_then_byte_with_data_is_returned() throws IOException {

    String fileName = "2018-12-17";

    given(requestForTransferRepository
        .findByFormattedCsvProcessedDate(Mockito.any(String.class),Mockito.any(String.class))).willReturn(RFT_SET);

    Optional<byte[]> result = financeService.getFile(fileName);

    String[] sresult = new String(result.get(), "UTF-8").split("\n");

    assertEquals("Account Code,Transaction Date,Base Amount,Base Amount(+/-),Payment Type,Reference,Description,Sales Analysis"
        + ",Journal Source,Contribution Month,Staff/GP,Contribution Type", sresult[0]);
    assertThat(sresult[1], containsString("EA1234"));
    assertThat(sresult[1], containsString("17/12/2018"));
  }

  @Test
  public void when_get_file_then_exception_thrown() throws IOException {

    String fileName = "2018-12-17";

    given(requestForTransferRepository
        .findByFormattedCsvProcessedDate(Mockito.any(String.class),Mockito.any(String.class))).willThrow(IOException.class);

    Optional<byte[]> actualFile = financeService.getFile(fileName);

    assertFalse(actualFile.isPresent());
  }

}