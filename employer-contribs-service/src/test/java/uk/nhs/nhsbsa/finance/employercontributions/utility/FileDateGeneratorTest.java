package uk.nhs.nhsbsa.finance.employercontributions.utility;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringRunner.class)
public class FileDateGeneratorTest {

  private final List<String> BANK_HOLIDAYS = Arrays.asList("01/01","07/05","28/05","27/07","03/12","25/12","26/12");
  private int DEADLINE_HOUR = 13;

  private FileDateGenerator dateGenerator;
  
  @Before
  public void setUp() {
      dateGenerator = spy(new FileDateGenerator(BANK_HOLIDAYS, DEADLINE_HOUR));

      ReflectionTestUtils.setField(dateGenerator, "excludedDates", BANK_HOLIDAYS);
      ReflectionTestUtils.setField(dateGenerator, "deadlineHour", DEADLINE_HOUR);
      
  }

  @Test
  public void given_time_on_or_after_3_return_true() {

    when(dateGenerator.now()).thenReturn(LocalTime.of(13,00));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(true, result);
  }

  @Test
  public void given_time_after_3_return_true() {

    when(dateGenerator.now()).thenReturn(LocalTime.of(14,00));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(true, result);
  }

  @Test
  public void given_time_before_3_return_true() {

    when(dateGenerator.now()).thenReturn(LocalTime.of(12,59));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(false, result);
  }

  @Test
  public void testGetNextWorkingDayIncludingWeekend() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.DECEMBER, 15), 1);

    assertEquals(LocalDate.of(2018, Month.DECEMBER, 17), result);
  }

  @Test
  public void testGetNextWorkingDayIncludingWeekendAndBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.DECEMBER, 16), 1);

    assertEquals(LocalDate.of(2018, Month.DECEMBER, 17), result);
  }

  @Test
  public void testGetNextWorkingDayIncludingBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.DECEMBER, 25), 1);

    assertEquals(LocalDate.of(2018, Month.DECEMBER, 27), result);
  }

  @Test
  public void testGetNextWorkingDayWithoutWeekendOrBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.DECEMBER, 17), 1);

    assertEquals(LocalDate.of(2018, Month.DECEMBER, 18), result);
  }

  @Test
  public void testGetNextWorkingDayWithBankHolidayAndWeekend() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.NOVEMBER, 30), 1);

    assertEquals(LocalDate.of(2018, Month.DECEMBER, 4), result);
  }

  @Test
  public void testGetPreviousWorkingDayFromTuesdayIncludingWeekend() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JUNE, 12), -2); // Tuesday

    assertEquals(LocalDate.of(2018, Month.JUNE, 8), result); // Previous Friday
  }

  @Test
  public void testGetPreviousWorkingDayFromMondayIncludingWeekend() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JUNE, 11), -2); // Monday

    assertEquals(LocalDate.of(2018, Month.JUNE, 7), result); // Previous Thursday
  }

  @Test
  public void testGetPreviousWorkingDayIncludingWeekendAndBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 30), -2); // Wednesday

    assertEquals(LocalDate.of(2018, Month.MAY, 25), result); // Previous Friday
  }
  
  @Test
  public void testGetWorkingDayGivenWorkingDayAndZeroOffset() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 30), 0); // Wednesday

    assertEquals(LocalDate.of(2018, Month.MAY, 30), result); // Wednesday
  }
  
  @Test
  public void testNextWorkingDayGivenNonWorkingDayAndZeroOffset() {    

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JULY, 21), 0); // Saturday

    assertEquals(LocalDate.of(2018, Month.JULY, 23), result); // Monday
  }

  
  @Test
  public void testIsWorkingDayReturnsFalseForSaturday() {
    
    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 12));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsFalseForSunday() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 13));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsFalseForBankHoliday() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 7));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsTrueForWorkingDay() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 14));

    assertEquals(true, result);
  }

}


