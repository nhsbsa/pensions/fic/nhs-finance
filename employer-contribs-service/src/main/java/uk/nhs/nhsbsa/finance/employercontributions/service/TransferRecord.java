package uk.nhs.nhsbsa.finance.employercontributions.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nhsbsa.view.TransferView;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Model a single row in the csv file, provide transformers that will
 * put data in the expected format for the csv file.
 *
 * Created by duncan on 31/03/2017.
 */
public class TransferRecord {

    @JsonProperty(value = "Account Code") String eaCode;
    @JsonProperty(value = "Transaction Date") String transactionDate;
    @JsonProperty(value = "Base Amount") String baseAmount;
    @JsonProperty(value = "Base Amount(+/-)") String baseAmountPlusMinus;
    @JsonProperty(value = "Payment Type") String paymentType;
    @JsonProperty(value = "Reference") String reference;
    @JsonProperty(value = "Description") String description;
    @JsonProperty(value = "Sales Analysis") String salesAnalysis;
    @JsonProperty(value = "Journal Source") String journalSource = "RFTOL";
    @JsonProperty(value = "Contribution Month") String contributionYearMonth;
    @JsonProperty(value = "Staff/GP") String staffOrGP;
    @JsonProperty(value = "Contribution Type") ContributionType contributionType;

    public Date absoluteYearMonthToDate(String yearMonth) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyMM");
        return format.parse(yearMonth);
    }

    String dateToFinancialYearMonth(Date date) {
        // April is month 1 in this scheme, and the year might be wrong; April 2017 is actually
        // in financial year 2018.
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        // Month in range 1-12
        int month = localDate.getMonthValue();
        // Absolute year
        int year = localDate.getYear() + 1900;

        if (month <= 3) {
            month += 9;
        } else {
            month -= 3;
            year += 1;
        }
        LocalDate contributionDate = LocalDate.now().withMonth(month).withYear(year);
        Date dateOfContrib = Date.from(contributionDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        DateFormat format = new SimpleDateFormat("yyMM");
        return format.format(dateOfContrib);
    }

    String currencyAmountToBaseAmount(BigDecimal value) {
        return value.abs().toString();
    }

    String currencyAmountToPaymentType(BigDecimal value) {
        if (value.compareTo(BigDecimal.ZERO) < 0) {
            return "CRN";
        }
        return "INV";
    }

    String salesAnalysisFromTypeAndAdjustment(ContributionType type, boolean isAdjustment,
        String employerType) {
        String analysis = "XR";
        int code;

           switch (type) {
               case A:
                   // employee added years
                   code = 7201;
                   break;
               case B:
                   // ERRBO
                   code = 7203;
                   break;
               case D:
                   // additional pension
                   code = 7202;
                   break;
               case E:
                   // employee contribution
                   code = 7200;
                   break;
               case R:
                   // employer contribution
                   code = 7100;
                   break;
               default:
                   throw new EnumConstantNotPresentException(type.getClass(),
                       String.format("ContributionType %s not handled", type));
           }

       if(employerType.equals("G")){
               code+=10;
       }

        analysis += String.valueOf(code);

        if (isAdjustment) {
            analysis += "Adj";
        }
        return analysis;
    }

    String formatTransactionDate(LocalDate date) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return date.format(formatter);
    }

    static TransferRecord makeTransferRecordFrom(TransferView view, BigDecimal amount, ContributionType type, boolean isAdjustment) {
        TransferRecord record = new TransferRecord();
        record.eaCode = view.getAccountCode();
        record.transactionDate = record.formatTransactionDate(view.getTransactionDate());
        try {
            String yearMonth = isAdjustment ? view.getAdjustmentYearMonth() : view.getContributionYearMonth();
            record.contributionYearMonth = record.dateToFinancialYearMonth(record.absoluteYearMonthToDate(yearMonth));
        } catch (ParseException e) {
            throw new IllegalArgumentException("Parse error", e);
        }
        record.reference = record.eaCode + record.contributionYearMonth;
        record.baseAmount = record.currencyAmountToBaseAmount(amount);
        record.baseAmountPlusMinus = amount.toString();
        record.paymentType = record.currencyAmountToPaymentType(amount);
        record.contributionType = type;
        record.staffOrGP = view.getEmployerType();
        record.salesAnalysis = record.salesAnalysisFromTypeAndAdjustment(record.contributionType, isAdjustment,view.getEmployerType());

        record.description = "JS: RFTOL REF: "
            + record.reference
            + " DES: Online Monthly Cont'tions Staff/GP: " + record.staffOrGP;
        return record;
    }

}

