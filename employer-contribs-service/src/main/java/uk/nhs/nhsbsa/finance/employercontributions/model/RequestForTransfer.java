package uk.nhs.nhsbsa.finance.employercontributions.model;

import com.nhsbsa.audit.AuditingBaseEntity;
import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.ContributionDate;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.TransferFormDateConverter;
import com.nhsbsa.model.validation.ContributionsValidationGroup;
import com.nhsbsa.model.validation.SchedulePaymentValidationGroup;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uk.nhs.nhsbsa.finance.employercontributions.model.validation.EmployeeContributionThreshold;
import uk.nhs.nhsbsa.finance.employercontributions.model.validation.EmployerContributionThreshold;
import uk.nhs.nhsbsa.finance.employercontributions.model.validation.TransferFormDateNotBlank;
import uk.nhs.nhsbsa.finance.employercontributions.model.validation.ValidFormDate;

@Data
@Builder(toBuilder = true)
@Entity
@Table(name = "request_for_transfer")
@NoArgsConstructor
@AllArgsConstructor // NOSONAR
@EqualsAndHashCode(callSuper = true)
@TransferFormDateNotBlank(groups = SchedulePaymentValidationGroup.class)
@ValidFormDate(groups = SchedulePaymentValidationGroup.class)
@EmployeeContributionThreshold(groups = ContributionsValidationGroup.class)
@EmployerContributionThreshold(groups = ContributionsValidationGroup.class)
public class RequestForTransfer extends AuditingBaseEntity<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "rft_id", insertable = false, updatable = false)
  private Long id;

  @Valid
  @Convert(converter = TransferFormDateConverter.class)
  private TransferFormDate transferDate;

  private Boolean isGp = false;

  @Valid
  @Embedded
  private ContributionDate contributionDate = ContributionDate.builder().build();

  @DecimalMin(value = "1.00", message = "{totalPensionablePay.invalid}")
  @DecimalMax(value = "99999999.99", message = "{totalPensionablePay.invalid}")
  @Digits(integer=10, fraction=2, message = "{totalPensionablePay.invalid}")
  @NotNull(message = "{totalPensionablePay.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal totalPensionablePay;

  @DecimalMin(value = "1.00", message = "{employeeContributions.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employeeContributions.invalid}")
  @Digits(integer=10, fraction=2, message = "{employeeContributions.invalid}")
  @NotNull(message = "{employeeContributions.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal employeeContributions;

  @DecimalMin(value = "1.00", message = "{employerContributions.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employerContributions.invalid}")
  @Digits(integer=10, fraction=2, message = "{employerContributions.invalid}")
  @NotNull(message = "{employerContributions.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal employerContributions;

  @DecimalMin(value = "0.00", message = "{employeeAddedYears.invalid}")
  @DecimalMax(value = "99999999.99", message = "{employeeAddedYears.invalid}")
  @Digits(integer=10, fraction=2, message = "{employeeAddedYears.invalid}")
  private BigDecimal employeeAddedYears;

  @DecimalMin(value = "0.00", message = "{additionalPension.invalid}")
  @DecimalMax(value = "99999999.99", message = "{additionalPension.invalid}")
  @Digits(integer=10, fraction=2, message = "{additionalPension.invalid}")
  private BigDecimal additionalPension;

  @DecimalMin(value = "0.00", message = "{errbo.invalid}")
  @DecimalMax(value = "99999999.99", message = "{errbo.invalid}")
  @Digits(integer=10, fraction=2, message = "{errbo.invalid}")
  private BigDecimal errbo;

  @NotNull(message = "{adjustmentsRequired.notNull}", groups = ContributionsValidationGroup.class)
  private Boolean adjustmentsRequired = false;

  @NotNull(message = "{totalDebitAmount.notNull}", groups = ContributionsValidationGroup.class)
  private BigDecimal totalDebitAmount;

  /**
   * The date the record was created (the time the user started populating request data)
   */
  @Temporal(TemporalType.TIMESTAMP)
  private Date receiveDate = new Date();

  @Temporal(TemporalType.TIMESTAMP)
  private Date submitDate;
  /**
   * The date that the RequestForTransfer entity is received by the finance-facade for inclusion in
   * a csv file for processing. Once this value is non-null, the RFT will no longer be considered
   * for processing by the finance-facade.
   */
  @Temporal(TemporalType.TIMESTAMP)
  private Date csvProcessedDate;

  private String eaCode;

  private String rftUuid;

  @Transient
  private String payAsSoonAsPossible;

  // Where any adjustments are stored, table (see class definition) is "adjustment" and linked on rft_id
  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
  @JoinColumn(name = "rft_id")
  @Valid()
  private List<Adjustment> adjustmentList;


  public Adjustment getAdjustment() {
    if (adjustmentList == null || adjustmentList.isEmpty()) {
      return null;
    }
    return adjustmentList.get(0);
  }

  public void setAdjustment(final Adjustment adjustment) {
    removeAdjustment();
    if (adjustment != null) {
      if (this.adjustmentList == null) {
        adjustmentList = new ArrayList<>();
      }
      adjustmentList.add(adjustment);
    }
  }

  public void removeAdjustment() {
    if (this.adjustmentList != null) {
      this.adjustmentList.clear();
    }
  }
}