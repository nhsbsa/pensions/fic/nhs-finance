package uk.nhs.nhsbsa.finance.employercontributions.view;

import lombok.Data;

/**
 * Thin bean representing response from finance-facade service.
 *
 * Created by duncan on 27/03/2017.
 */
@Data
public class FacadeResponseView {

    private String confirmationStatus;
}
