package com.nhsbsa.controllers.finance;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.AdminEmployerUser;
import com.nhsbsa.model.AdminUser;
import com.nhsbsa.model.EmployerUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateOrganisationResponse;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CreateAdminUserControllerTest {

  @MockBean
  private UserRegistrationService userRegistrationService;

  @MockBean
  private AuthorizationService authorizationService;

  @MockBean
  private OrganisationService organisationService;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  private static final EmployingAuthorityView EA = EmployingAuthorityView.builder()
      .eaCode("EA1234")
      .name("EA")
      .employerType("STAFF")
      .build();
  private static final List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);

  private static final OrganisationView ORGANISATION = OrganisationView.builder()
      .identifier(UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca"))
      .name("ORG")
      .eas(EA_LIST)
      .build();

  private static final FinanceUser FINANCE_USER = FinanceUser.builder()
      .uuid("UUID")
      .role("ADMIN")
      .firstName("Sam")
      .lastName("Jones")
      .username("sam.jones@email.com")
      .organisations(Collections.singletonList(ORGANISATION))
      .build();

  private final static String USER_UUID = "UUID";
  private final static String EA_CODE = "EA1234";

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_showCreateAdminUserPage_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-user"))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_showCreateAdminUserPage_requested_with_admin_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-user"))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_showCreateAdminUserPage_requested_with_master_role_then_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-user"))
        .andExpect(handler().methodName("showCreateAdminUserPage"))
        .andExpect(model().attributeExists("adminUser"));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_1_invalid_field_then_page_is_returned_with_errors()
      throws Exception {

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(
            MockMvcRequestBuilders.post("/create-admin-user")
                .param("email", "invalidEmail")
                .param("firstPassword", "ValidPassword123")
                .param("firstName", "first")
                .param("lastName", "last")
                .param("contactTelephone", "12345678912")
                .param("organisationName", "Test Organisation"))
        .andExpect(model().attributeExists("adminUser"))
        .andExpect(model().errorCount(1));
  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_user_that_already_exists_then_page_is_returned_with_userExists_attribute()
      throws Exception {

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(new User()));
    when(financeService.getFinanceUserByUuid(any())).thenReturn(Optional.of(new FinanceUser()));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect(model().attributeExists("userExists"));

  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_organisation_that_already_exists_then_page_is_returned_with_orgExists_attribute()
      throws Exception {

    String orgExists = "Organisation exists";
    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(financeService.getFinanceUserByUuid(any())).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.of(orgExists));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect(model().attributeExists("orgExists"));

  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_authentication_user_and_organisation_that_already_exists_then_page_is_returned_with_orgExists_attribute()
      throws Exception {

    String orgExists = "Organisation exists";
    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(new User()));
    when(financeService.getFinanceUserByUuid(any())).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.of(orgExists));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect(model().attributeExists("orgExists"));

  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_authentication_user_that_does_not_exist_then_forwarded_page_is_returned()
      throws Exception {

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid("VALID-UUID");

    CreateOrganisationResponse orgResponse = CreateOrganisationResponse.builder().name("Test Organisation").uuid("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca").build();
    CreateUserResponse userResponse = CreateUserResponse.builder().uuid("VALID-UUID").email("sam.jones@email.com").build();

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.empty());

    when(userRegistrationService.registerAuthenticationUser(any())).thenReturn((Optional.of(mockedAuthenticationResponse)));
    when(organisationService.registerOrganisationWithoutEa(any())).thenReturn(Optional.of(orgResponse));
    when(userRegistrationService.createFinanceUser(any())).thenReturn(Optional.of(userResponse));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect(status().is2xxSuccessful())
        .andExpect((view().name("redirect:/create-ea/"+mockedAuthenticationResponse.getUuid())));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_with_authentication_user_that_exists_but_finance_user_not_then_forwarded_page_is_returned()
      throws Exception {

    String validUUID = "VALID-UUID";

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = new User();
    mockedUser.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(mockedUser));
    when(financeService.getFinanceUserByUuid(validUUID)).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.empty());

    CreateOrganisationResponse orgResponse = CreateOrganisationResponse.builder().name("Test Organisation").uuid("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca").build();
    CreateUserResponse userResponse = CreateUserResponse.builder().uuid("VALID-UUID").email("sam.jones@email.com").build();
    when(organisationService.registerOrganisationWithoutEa(any())).thenReturn(Optional.of(orgResponse));
    when(userRegistrationService.createFinanceUser(any())).thenReturn(Optional.of(userResponse));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect(status().is2xxSuccessful())
        .andExpect((view().name("redirect:/create-ea/"+mockedAuthenticationResponse.getUuid())));
  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_and_userRegistrationService_createAuthenticationUser_returns_null_then_500_response_is_sent()
      throws Exception {

    String validUUID = "VALID-UUID";

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = new User();
    mockedUser.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(userRegistrationService.registerAuthenticationUser(any())).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.empty());

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect((status().is5xxServerError()));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_createAdminEmployer_posted_and_organisationService_returns_null_then_500_response_is_sent()
      throws Exception {

    String validUUID = "VALID-UUID";

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = new User();
    mockedUser.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(organisationService.getOrganisationByName(any())).thenReturn(Optional.empty());

    when(userRegistrationService.registerAuthenticationUser(any())).thenReturn(Optional.of(mockedAuthenticationResponse));
    when(organisationService.registerOrganisationWithoutEa(any())).thenReturn(Optional.empty());

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-employer-admin-user.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-admin-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("firstName", "first")
            .param("lastName", "last")
            .param("contactTelephone", "12345678912")
            .param("organisationName", "Test Organisation"))
        .andExpect((status().is5xxServerError()));
  }


/*
* CREATE EA Page tests
*/
  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_create_ea_endpoint_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-ea/"+USER_UUID))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_create_ea_endpoint_requested_with_admin_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-ea/"+USER_UUID))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_requested_with_master_role_then_requested_page_is_returned()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.of(FINANCE_USER));
    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-ea/"+USER_UUID))
        .andExpect(handler().methodName("showCreateEaPage"))
        .andExpect(model().attributeExists("employingAuthority"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("userMail", FINANCE_USER.getUsername()));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_requested_with_null_user_then_500_error_returned()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.empty());
    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-ea/"+USER_UUID))
        .andExpect((status().is5xxServerError()));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_post_successful_then_create_ea_url_is_returned()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.of(FINANCE_USER));
    when(organisationService.getEaByEaCode(any())).thenReturn(Optional.empty());
    when(organisationService.registerEa(any())).thenReturn(Optional.of(ORGANISATION));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-ea.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-ea/"+ USER_UUID)
            .param("eaName", "EA Name")
            .param("employerType", "STAFF")
            .param("eaCode", "EA1234"))
        .andExpect(status().is2xxSuccessful())
        .andExpect((view().name("redirect:/create-ea/"+ FINANCE_USER.getUuid())));

    verify(organisationService, times(1)).registerEa(any());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_post_returns_binding_errors()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.of(FINANCE_USER));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-ea.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-ea/"+ USER_UUID)
            .param("eaName", "")
            .param("employerType", "")
            .param("eaCode", ""))
        .andExpect(model().attributeExists("employingAuthority"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("userMail", FINANCE_USER.getUsername()))
        .andExpect(model().errorCount(3));

    verify(organisationService, times(0)).registerEa(any());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_post_has_null_user_then_return_500_error()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.empty());
    when(organisationService.getEaByEaCode(any())).thenReturn(Optional.empty());

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-ea.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-ea/"+ USER_UUID)
            .param("eaName", "EA Name")
            .param("employerType", "STAFF")
            .param("eaCode", "EA1234"))
        .andExpect((status().is5xxServerError()));

    verify(organisationService, times(0)).registerEa(any());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_create_ea_endpoint_post_has_existing_ea_returns_view()
      throws Exception {

    when(financeService.getFinanceUserByUuid(USER_UUID)).thenReturn(Optional.of(FINANCE_USER));
    when(organisationService.getEaByEaCode(any())).thenReturn(Optional.of(EA_CODE));

    CreateAdminUserController controller = Mockito
        .spy(new CreateAdminUserController(organisationService, authorizationService, userRegistrationService, financeService));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/admin/create-ea.html"))
        .build();

    mvc
        .perform(MockMvcRequestBuilders.post("/create-ea/"+ USER_UUID)
            .param("eaName", "EA Name")
            .param("employerType", "STAFF")
            .param("eaCode", "EA1234"))
        .andExpect(model().attributeExists("employingAuthority"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("userMail", FINANCE_USER.getUsername()))
        .andExpect(model().attributeExists("eaExists"));

    verify(organisationService, times(0)).registerEa(any());
  }





}
