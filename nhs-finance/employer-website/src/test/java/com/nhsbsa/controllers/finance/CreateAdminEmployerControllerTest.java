package com.nhsbsa.controllers.finance;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.nhsbsa.model.AdminEmployerUser;
import com.nhsbsa.model.EmployerUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.MockitoAnnotations;
import org.mockito.verification.VerificationMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CreateAdminEmployerControllerTest {

  @MockBean
  private UserRegistrationService userRegistrationService;

  @MockBean
  private AuthorizationService authorizationService;

  @MockBean
  private OrganisationService organisationService;

  @MockBean
  private FinanceService financeService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;
  private final EmployingAuthorityView EA = EmployingAuthorityView.builder().eaCode("EA1234").name("EA").employerType("STAFF").build();
  private final List<EmployingAuthorityView> EA_LIST = Collections.singletonList(EA);
  private final OrganisationView ORGANISATION = OrganisationView.builder().identifier(UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca")).name("ORG").eas(EA_LIST).build();

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-employer"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN"})
  public void given_endpoint_requested_with_admin_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-employer"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_requested_with_master_role_then_requested_create_standard_employer_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-employer")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser()))
        .andExpect(handler().methodName("showCreateAdminEmployerPage"))
        .andExpect(model().attributeExists("adminEmployerUser"))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_with_1_invalid_field_then_page_is_returned_with_1_error()
      throws Exception {

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/create-admin-employer")
                .param("email", "invalidEmail")
                .param("firstPassword", "ValidPassword123")
                .sessionAttr("adminEmployerUser", new AdminEmployerUser())
                .with(csrf()))
        .andExpect(model().attributeExists("adminEmployerUser"))
        .andExpect(model().errorCount(1))
        .andReturn();
  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_with_user_that_already_exists_then_page_is_returned_with_userExists_attribute()
      throws Exception {

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(new User()));
    when(financeService.getFinanceUserByUuid(any())).thenReturn(Optional.of(new FinanceUser()));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-admin-employer")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect(model().attributeExists("userExists"))
        .andReturn();

  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_with_authentication_user_that_does_not_exist_then_forwarded_page_is_returned()
      throws Exception {

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid("VALID-UUID");

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(userRegistrationService.createAuthenticationUser(any())).thenReturn((Optional.of(mockedAuthenticationResponse)));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-admin-employer")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect((view().name("forward:/admin-employer-details")))
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_with_authentication_user_that_exists_but_finance_user_not_then_forwarded_page_is_returned()
      throws Exception {

    String validUUID = "VALID-UUID";

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = new User();
    mockedUser.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.of(mockedUser));
    when(financeService.getFinanceUserByUuid(validUUID)).thenReturn(Optional.empty());

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-admin-employer")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect((view().name("forward:/admin-employer-details")))
        .andReturn();
  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_and_userRegistrationService_createAuthenticationUser_returns_null_then_500_response_is_sent()
      throws Exception {

    String validUUID = "VALID-UUID";

    AuthenticationResponse mockedAuthenticationResponse = new AuthenticationResponse();
    mockedAuthenticationResponse.setUuid(validUUID);

    User mockedUser = new User();
    mockedUser.setUuid(validUUID);

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(userRegistrationService.createAuthenticationUser(any())).thenReturn(Optional.empty());

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-admin-employer")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect((status().is5xxServerError()))
        .andReturn();
  }

  @Test
  @WithMockUser( roles = {"MASTER"})
  public void given_endpoint_post_with_existing_organisation_then_finance_user_is_created() throws Exception{

    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");

    when(userRegistrationService.createEmployerUser(any())).thenReturn((Optional.of(mockedResponse)));

    when(organisationService.getOrganisationByEaCode((any()))).thenReturn(Optional.of(ORGANISATION));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-finance-employer-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("fullName", "Full Name")
            .param("contactTelephone","11111111111")
            .param("accountName", "Random Name")
            .param("employerType", "STAFF")
            .param("eaCode", "EA1234")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect((view().name("forward:/employer-created")))
        .andReturn();

    ArgumentCaptor<EmployerUser> userArgumentCaptor = ArgumentCaptor.forClass(EmployerUser.class);

    verify(userRegistrationService,times(1))
        .createEmployerUser(userArgumentCaptor.capture());

    EmployerUser user = userArgumentCaptor.getValue();

    assertThat(user.getOrganisations(), hasItem(ORGANISATION));
  }


  @Test
  @WithMockUser( roles = {"MASTER"})
  public void given_endpoint_post_with_non_existing_organisation_then_finance_user_is_created() throws Exception{

    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");

    when(userRegistrationService.createEmployerUser(any())).thenReturn((Optional.of(mockedResponse)));

    when(organisationService.getOrganisationByEaCode((any()))).thenReturn(Optional.empty());

    when(organisationService
        .registerOrganisation(anyString(), anyString(), anyString())).thenReturn(Optional.of(ORGANISATION));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-finance-employer-user")
            .param("email", "any@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("fullName", "Full Name")
            .param("contactTelephone","11111111111")
            .param("accountName", "Random Name")
            .param("employerType", "STAFF")
            .param("eaCode", "EA1234")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect((view().name("forward:/employer-created")))
        .andReturn();

    ArgumentCaptor<EmployerUser> userArgumentCaptor = ArgumentCaptor.forClass(EmployerUser.class);

    verify(userRegistrationService,times(1))
        .createEmployerUser(userArgumentCaptor.capture());

    EmployerUser user = userArgumentCaptor.getValue();

    assertThat(user.getOrganisations(), hasItem(ORGANISATION));

  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_and_userRegistrationService_createEmployerUser_returns_null_when_creating_employer_user_then_500_response_is_sent()
      throws Exception {

    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());

    when(userRegistrationService.createEmployerUser(any())).thenReturn((Optional.ofNullable(null)));

    when(organisationService.getOrganisationByEaCode((any()))).thenReturn(Optional.of(ORGANISATION));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-finance-employer-user")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("eaCode", "EA1234")
            .param("contactTelephone", "01234567891")
            .param("accountName", "Account Name")
            .param("employerType", "STAFF")
            .param("fullName", "Full Name")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect(status().is5xxServerError())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_post_and_userRegistrationService_returns_null_when_creating_authentication_user_then_500_response_is_sent()
      throws Exception {

    CreateUserResponse mockedResponse = new CreateUserResponse();
    mockedResponse.setEmail("any@email.com");

    when(authorizationService.getUserByEmail(any())).thenReturn(Optional.empty());
    when(userRegistrationService.createAuthenticationUser(any())).thenReturn((Optional.ofNullable(null)));

    mockMvc
        .perform(MockMvcRequestBuilders.post("/create-admin-employer")
            .param("email", "valid@email.com")
            .param("firstPassword", "ValidPassword123")
            .param("eaCode", "EA1234")
            .param("contactTelephone", "01234567891")
            .param("accountName", "Account Name")
            .param("employerType", "STAFF")
            .param("fullName", "Full Name")
            .sessionAttr("adminEmployerUser", new AdminEmployerUser())
            .with(csrf()))
        .andExpect(status().is5xxServerError())
        .andReturn();
  }


}
