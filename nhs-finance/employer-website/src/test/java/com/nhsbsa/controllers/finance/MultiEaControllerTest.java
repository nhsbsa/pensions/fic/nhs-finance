package com.nhsbsa.controllers.finance;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.MultiEa;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.view.InternalResourceView;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class MultiEaControllerTest {

  @Mock
  private RequestForTransferService requestForTransferService;

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  private static EmployingAuthorityView EA_1 = EmployingAuthorityView.builder().eaCode("EA1111").name("EA 1").build();
  private static EmployingAuthorityView EA_2 = EmployingAuthorityView.builder().eaCode("EA2222").name("EA 2").build();
  private static List<EmployingAuthorityView> EA_LIST = Arrays.asList(EA_1, EA_2);
  private static OrganisationView ORGANISATION = OrganisationView.builder().name("Org 1").eas(EA_LIST).build();
  private static List<OrganisationView> ORG_LIST1 = Collections.singletonList(ORGANISATION);

  private static MultiEa MULTI_EA_1 = MultiEa.builder().eaCode("EA1111").name("EA1111 - EA 1").lastSubmitted("15 August 2018").build();
  private static MultiEa MULTI_EA_2 = MultiEa.builder().eaCode("EA2222").name("EA2222 - EA 2").lastSubmitted("15 August 2018").build();

  private static EmployingAuthorityView EA_3 = EmployingAuthorityView.builder().eaCode("EA3333").name("EA 3").build();
  private static List<EmployingAuthorityView> EA_LIST2 = Collections.singletonList(EA_3);
  private static OrganisationView ORGANISATION2 = OrganisationView.builder().name("Org 2").eas(EA_LIST2).build();
  private static List<OrganisationView> ORG_LIST2 = Collections.singletonList(ORGANISATION2);

  private static MultiEa MULTI_EA_3 = MultiEa.builder().eaCode("EA3333").name("EA3333 - EA 3").lastSubmitted("-").build();

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_eacode_page_returned()
      throws Exception {
    FinanceUser user = FinanceUser.builder().role(UserRoles.ROLE_STANDARD.name())
        .organisations(ORG_LIST1)
        .build();

    MultiEaController controller = Mockito
        .spy(new MultiEaController(requestForTransferService));
    doReturn(Optional.of(user)).when(controller).getCurrentUser();

    Optional<LocalDate> submitDate = Optional.of(LocalDate.of(2018,8,15));
    doReturn(submitDate).when(requestForTransferService).getSubmitDateByEaCode(Mockito.any(String.class));


    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/choose-ea-account.html"))
        .build();

    MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/multi-ea"))
        .andExpect(status().isOk())
        .andExpect(view().name("choose-ea-account"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("eas", hasSize(2)))
        .andReturn();

    List aboutYouList = (List) result.getModelAndView().getModel().get("eas");
    assertEquals(MULTI_EA_1, aboutYouList.get(0));
    assertEquals(MULTI_EA_2, aboutYouList.get(1));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_eacode_page_returned_with_empty_submit_date()
      throws Exception {
    FinanceUser user = FinanceUser.builder().role(UserRoles.ROLE_STANDARD.name())
        .organisations(ORG_LIST2)
        .build();

    MultiEaController controller = Mockito
        .spy(new MultiEaController(requestForTransferService));
    doReturn(Optional.of(user)).when(controller).getCurrentUser();

    doReturn(Optional.empty()).when(requestForTransferService).getSubmitDateByEaCode(Mockito.any(String.class));

    MockMvc mvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/choose-ea-account.html"))
        .build();

    MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/multi-ea"))
        .andExpect(status().isOk())
        .andExpect(view().name("choose-ea-account"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("eas", hasSize(1)))
        .andReturn();

    List aboutYouList = (List) result.getModelAndView().getModel().get("eas");
    assertEquals(MULTI_EA_3, aboutYouList.get(0));
  }

  @Test
  @WithMockUser(roles = {"STANDARD"})
  public void given_endpoint_requested_with_no_eas_page_returned_with_empty_list()
      throws Exception {
    FinanceUser user = FinanceUser.builder().role(UserRoles.ROLE_STANDARD.name())
        .organisations(Collections.singletonList(OrganisationView.builder().eas(Collections.emptyList()).build()))
        .build();

    MultiEaController controller = Mockito
        .spy(new MultiEaController(requestForTransferService));
    doReturn(Optional.of(user)).when(controller).getCurrentUser();

    mockMvc = standaloneSetup(controller)
        .setSingleView(new InternalResourceView("/WEB-INF/templates/choose-ea-account.html"))
        .build();

    mockMvc.perform(MockMvcRequestBuilders.get("/multi-ea"))
        .andExpect(status().isOk())
        .andExpect(view().name("choose-ea-account"))
        .andExpect(model().attributeExists("eas"))
        .andExpect(model().attribute("eas", hasSize(0)));
  }


}
