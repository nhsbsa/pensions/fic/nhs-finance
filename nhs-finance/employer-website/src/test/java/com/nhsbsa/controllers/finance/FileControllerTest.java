package com.nhsbsa.controllers.finance;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.handler;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.nhsbsa.service.FileService;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FileControllerTest {


  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private FileService fileService;

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_get_request_with_master_role_then_download_file_page_is_returned()
      throws Exception {

    when(fileService.getFiles()).thenReturn(Optional.of(new String[0]));

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files"))
        .andExpect(handler().methodName("showDownloadFilesPage"))
        .andExpect(view().name("downloadFiles"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("fileList"));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_get_request_and_fileService_returns_empty_optional_then_download_file_page_is_returned()
      throws Exception {

    when(fileService.getFiles()).thenReturn(Optional.empty());

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files"))
        .andExpect(handler().methodName("showDownloadFilesPage"))
        .andExpect(view().name("downloadFiles"))
        .andExpect(status().isOk())
        .andExpect(model().attributeExists("fileList"));
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_get_request_when_file_date_is_not_valid_then_404_response()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files/2018-31-31"))
        .andExpect(handler().methodName("downloadFile"))
        .andExpect(status().isNotFound());
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_get_request_when_file_is_not_available_to_download_then_404_response()
      throws Exception {

    when(fileService.getFile(anyString())).thenReturn(Optional.empty());

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files/2018-01-02"))
        .andExpect(handler().methodName("downloadFile"))
        .andExpect(status().isNotFound());
  }


  @Test
  @WithMockUser(roles = {"MASTER"})
  public void given_endpoint_get_request_then_file_is_available_to_download()
      throws Exception {

    String fileDate = "2018-01-02";
    when(fileService.getFile(anyString())).thenReturn(Optional.of(new byte[0]));

    MvcResult result = mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files/" + fileDate)).andDo(print())
        .andReturn();

    MockHttpServletResponse response = result.getResponse();

    assertEquals("attachment; filename=ficposts_02_01_2018.csv",
        response.getHeader("Content-Disposition"));
    assertEquals(MediaType.TEXT_PLAIN.toString(), response.getContentType());
  }
}
