package com.nhsbsa.controllers.finance;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.nhsbsa.model.AdminEmployerUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class ControllersAccessRoleTest {

  private static final String VALID_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

  @Autowired
  private WebApplicationContext wac;

  private MockMvc mockMvc;

  @Before
  public void setup() {

    MockitoAnnotations.initMocks(this);

    mockMvc = MockMvcBuilders
        .webAppContextSetup(wac)
        .apply(springSecurity())
        .build();

  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void scheduleyourpayment_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/scheduleyourpayment"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void contributionsandpayment_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/contributionsandpayment/{rftUuid}", VALID_UUID))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void summary_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", VALID_UUID))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void thankyou_endpoint_requested_with_master_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", VALID_UUID))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  public void create_admin_employer_endpoint_requested_with_admin_and_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/create-admin-employer"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  public void adminEmployerDetails_endpoint_requested_with_admin_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files"))
        .andExpect(status().isForbidden());
  }

  @Test
  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  public void downloadFiles_endpoint_requested_with_admin_standard_role_then_forbidden_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/download-files"))
        .andExpect(status().isForbidden())
        .andReturn();
  }

  @Test
  @WithMockUser(roles = {"MASTER"})
  public void adminEmployerDetails_endpoint_requested_with_master_role_then_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.post("/admin-employer-details").with(csrf())
        .param("email", "valid@email.com")
        .param("firstPassword", "ValidPassword123")
        .sessionAttr("adminEmployerUser", new AdminEmployerUser()))
        .andExpect(status().isOk())
        .andReturn();
  }


  @Test
  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  public void scheduleyourpayment_endpoint_requested_with_admin_and_standard_role_then_page_is_returned()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/scheduleyourpayment"))
        .andExpect(status().isOk())
        .andReturn();
  }

  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  @Test(expected = NestedServletException.class)
  public void contributionsandpayment_endpoint_requested_with_admin_and_standard_role_then_nestedexception_is_thrown()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/contributionsandpayment/{rftUuid}", VALID_UUID))
        .andReturn();
  }

  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  @Test(expected = NestedServletException.class)
  public void summary_endpoint_requested_with_admin_and_standard_role_then_nestedexception_is_thrown()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/summary/{rftUuid}", VALID_UUID))
        .andReturn();
  }

  @WithMockUser(roles = {"ADMIN", "STANDARD"})
  @Test(expected = NestedServletException.class)
  public void thankyou_endpoint_requested_with_admin_and_standard_role_then_nestedexception_is_thrown()
      throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/thankyou/{rftUuid}", VALID_UUID))
        .andReturn();
  }

}