package com.nhsbsa.service.authentication;

import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.StandardEmployerUser;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.RegistrationRequest;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserRegistrationSeviceTest {


  private AuthorizationService mockedAuthorizationService;
  private FinanceService mockedFinanceService;

  private UserRegistrationService userRegistrationService;

  @Before
  public void setUp() throws Exception {

    mockedAuthorizationService = mock(AuthorizationService.class);
    mockedFinanceService = mock(FinanceService.class);

    userRegistrationService = new UserRegistrationService(mockedAuthorizationService,mockedFinanceService);

    List<EmployingAuthorityView> eaList = new ArrayList<>();
    eaList.add(EmployingAuthorityView.builder().eaCode("").name("").employerType("CODE-123-456").build());
    List<OrganisationView> organisations = new ArrayList<>();
    organisations.add(OrganisationView.builder().identifier(null).name("").eas(eaList).build());

    FinanceUser user = FinanceUser.builder().organisations(organisations).build();
    Authentication authentication = mock(Authentication.class);
    SecurityContext securityContext = mock(SecurityContext.class);
    when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
    when(SecurityContextHolder.getContext().getAuthentication().getDetails()).thenReturn(user);

  }

  @Test
  public void create_standard_employer_test(){

    StandardEmployerUser newStandardEmployerUser = StandardEmployerUser.builder().email("valid@email.com").firstPassword("ValidPassword123").build();
    AuthenticationResponse mockedAuthResponse = AuthenticationResponse.builder().uuid("UUID-123456789").build();
    CreateUserResponse mockedResponse = CreateUserResponse.builder().email("valid@email.com").uuid("UUID-123456789").build();


    when(mockedAuthorizationService.registerUser(any(RegistrationRequest.class))).thenReturn(mockedAuthResponse);

    when(mockedFinanceService.createUser(any(CreateUserRequest.class))).thenReturn(mockedResponse);

    Optional<CreateUserResponse> response = userRegistrationService.createEmployerUser(newStandardEmployerUser);

    assertSame(response.get(), mockedResponse);

  }

  @Test
  public void when_creating_standard_employer_authorizationService_returns_null_then_empty_optional_is_returned(){

    StandardEmployerUser newStandardEmployerUser = StandardEmployerUser.builder().email("valid@email.com").firstPassword("ValidPassword123").build();
    when(mockedAuthorizationService.registerUser(any(RegistrationRequest.class))).thenReturn(null);

    Optional<CreateUserResponse> response = userRegistrationService.createEmployerUser(newStandardEmployerUser);

    assertTrue(! response.isPresent());

  }

  @Test
  public void register_authentication_user_test(){

    AuthenticationResponse mockedAuthResponse = AuthenticationResponse.builder().uuid("UUID-123456789").role("ADMIN").build();

    when(mockedAuthorizationService.registerUser(any(RegistrationRequest.class))).thenReturn(mockedAuthResponse);

    RegistrationRequest registrationRequest = RegistrationRequest.builder().username("valid@email.com").password("ValidPassword123").role("admin").build();

    Optional<AuthenticationResponse> response = userRegistrationService.registerAuthenticationUser(registrationRequest);

    assertTrue(response.isPresent());
    assertSame(response.get().getUuid(), mockedAuthResponse.getUuid());
    assertSame(response.get().getRole(), mockedAuthResponse.getRole());

  }

  @Test
  public void create_finance_user_test(){

    CreateUserResponse mockedUserResponse = CreateUserResponse.builder().uuid("UUID-123456789").email("valid@email.com").build();

    when(mockedFinanceService.createUser(any(CreateUserRequest.class))).thenReturn(mockedUserResponse);

    CreateUserRequest userRequest = CreateUserRequest.builder().username("valid@email.com").uuid("UUID-123456789").organisations(Collections
        .emptyList()).build();

    Optional<CreateUserResponse> response = userRegistrationService.createFinanceUser(userRequest);

    assertTrue(response.isPresent());
    assertSame(response.get().getUuid(), mockedUserResponse.getUuid());
    assertSame(response.get().getEmail(), mockedUserResponse.getEmail());

  }

}
