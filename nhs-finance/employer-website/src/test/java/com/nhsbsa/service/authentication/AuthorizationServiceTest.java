package com.nhsbsa.service.authentication;


import static org.codehaus.groovy.runtime.DefaultGroovyMethods.any;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.exceptions.UserNotFoundAuthenticationException;
import com.nhsbsa.model.AccountUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.ForgottenPassword;
import com.nhsbsa.model.User;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.security.RegistrationRequest;
import com.nhsbsa.security.ResetEmailTokenResponse;
import com.nhsbsa.security.SetPassword;
import com.nhsbsa.security.SetPasswordResponse;
import com.nhsbsa.security.ValidateEmailResponse;
import com.nhsbsa.service.BackendAuthenticationApiUriService;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


/**
 * Created by nataliehulse on 12/10/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {BackendAuthenticationApiUriService.class})
@WebAppConfiguration
@TestPropertySource(properties = {
    "authorization.protocol=testValue",
    "authorization.url = testValue",
    "authorization.port = 80",
    "authorization.context = testValue"
})
public class AuthorizationServiceTest {

    private static final String AUTH_URL = "/auth-test";
    private static final String FINANCE_URL = "/finance-test";
    private static final String SAVE_PASSWORD_URL = "/password-test";
    private static final String VALIDATE_PASSWORD_URL = "/validate-test";
    private static final String VALIDATE_EMAIL_URL = "/validate-email";
    private static final String GET_UUID_BY_TOKEN = "/verify-token";
    private static final String SEND_EMAIL_URL = "/send-email";
    private static final String DELETE_TOKEN = "/delete-token";
    private static final String USER_NAME = "user";
    private static final String PASSWORD = "password";
    private static final String UUID = "usr-uuid";
    private static final String CONTACT_EMAIL = "test.test@test.com";
    private static final String FIRST_NAME = "Test";
    private static final String LAST_NAME = "Tester";
    private static final String REGISTER_USER_URL = "/register-user";
    private static final LocalDate LOCAL_DATE = LocalDate.of(2018,10,1);
    private static Date DELETED_ON = Date.from(LOCAL_DATE.atStartOfDay(ZoneId.systemDefault()).toInstant());

  private static final boolean FIRST_LOGIN = true;
  private static final boolean PASSWORD_SAME_AS_PREVIOUS = false;

  private AuthorizationService authorizationService;

  @Mock
  private RestTemplate restTemplate;

  @Autowired
  private BackendAuthenticationApiUriService backendAuthenticationApiUriService;

    @Before
    public void setUp() throws Exception {

        authorizationService = new AuthorizationService(backendAuthenticationApiUriService, restTemplate);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendUri", AUTH_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "financeAuthUrl", FINANCE_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendUpdateDetailsUri", SAVE_PASSWORD_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendValidateDetailsUri", VALIDATE_PASSWORD_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendResetPassUri", VALIDATE_EMAIL_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "financeEmailUrl", SEND_EMAIL_URL, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendVerifyTokenUri", GET_UUID_BY_TOKEN, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendDeleteTokenUri", DELETE_TOKEN, true);
        FieldUtils.writeDeclaredField(authorizationService, "authorizationBackendRegisterUserUri",
                REGISTER_USER_URL, true);

    }

    @Test
    public void testGetFinanceUser() throws Exception {
        final LoginRequest loginRequest = LoginRequest.builder().username(USER_NAME).password(PASSWORD).uuid(UUID).firstLogin(FIRST_LOGIN).build();

        AuthenticationResponse authenticationResponse = AuthenticationResponse.builder()
            .uuid(UUID)
            .firstLogin(FIRST_LOGIN)
            .authenticationStatus(AuthenticationStatus.AUTH_SUCCESS)
            .build();

        given(restTemplate.postForObject(AUTH_URL, loginRequest, AuthenticationResponse.class)).willReturn(authenticationResponse);

        FinanceUser user = FinanceUser.builder().uuid(UUID).contactEmail(CONTACT_EMAIL).firstName(FIRST_NAME).lastName(LAST_NAME).firstLogin(FIRST_LOGIN).build();
        given(restTemplate.postForObject(FINANCE_URL, loginRequest, FinanceUser.class)).willReturn(user);

        FinanceUserStatusWrapper financeUserStatusWrapper = authorizationService.authenticateAndRetrieveFinanceUser(loginRequest);
        FinanceUser financeUser = financeUserStatusWrapper.getFinanceUser();

        assertThat(financeUser.getUuid(), is(UUID));
        assertThat(financeUser.getContactEmail(), is(CONTACT_EMAIL));
        assertThat(financeUser.getFirstName(), is(FIRST_NAME));
        assertThat(financeUser.getLastName(), is(LAST_NAME));
        assertThat(financeUser.isFirstLogin(), is(FIRST_LOGIN));
    }

    @Test(expected = UserNotFoundAuthenticationException.class)
    public void testFinanceUserNotFound() throws Exception {
        final LoginRequest loginRequest = LoginRequest.builder().username(USER_NAME).password(PASSWORD).uuid(UUID).firstLogin(FIRST_LOGIN).build();

        AuthenticationResponse authenticationResponse = AuthenticationResponse.builder()
            .uuid(UUID)
            .firstLogin(FIRST_LOGIN)
            .authenticationStatus(AuthenticationStatus.AUTH_SUCCESS)
            .build();
        given(restTemplate.postForObject(AUTH_URL, loginRequest, AuthenticationResponse.class)).willReturn(authenticationResponse);

        given(restTemplate.postForObject(FINANCE_URL, loginRequest, FinanceUser.class)).willThrow(HttpClientErrorException.class);

        authorizationService.authenticateAndRetrieveFinanceUser(loginRequest);
    }

  @Test
  public void testGetFinanceUserReturnsOptionalEmpty() throws Exception {
    final LoginRequest loginRequest = LoginRequest.builder().username(USER_NAME).password(PASSWORD).uuid(UUID).firstLogin(FIRST_LOGIN).build();

    AuthenticationResponse authenticationResponse = AuthenticationResponse.builder()
        .uuid(UUID)
        .firstLogin(FIRST_LOGIN)
        .authenticationStatus(AuthenticationStatus.AUTH_FAILURE)
        .build();

    given(restTemplate.postForObject(AUTH_URL, loginRequest, AuthenticationResponse.class)).willReturn(authenticationResponse);

    FinanceUserStatusWrapper financeUserStatusWrapper = authorizationService.authenticateAndRetrieveFinanceUser(loginRequest);

    assertThat(financeUserStatusWrapper.getStatus(), is(AuthenticationStatus.AUTH_FAILURE));
  }

  @Test
  public void testAuthenticateThrowsException() throws Exception {
    final LoginRequest loginRequest = LoginRequest.builder().username(USER_NAME).password(PASSWORD).uuid(UUID).firstLogin(FIRST_LOGIN).build();

    given(restTemplate.postForObject(AUTH_URL, loginRequest, AuthenticationResponse.class))
        .willAnswer(invocation -> { throw new Exception("Failed to authenticate login request"); });

    try {
      Optional<AuthenticationResponse> response = authorizationService.authenticate(loginRequest);
      assertFalse(response.isPresent());

    }catch (Exception e) {
      assertEquals("Failed to authenticate login request", e.getMessage());
    }

  }

    @Test
    public void testSavePassword() throws Exception {

        final SetPassword setPassword = SetPassword.builder().uuid(UUID).newPassword(PASSWORD).firstLogin(false).build();

        final SetPasswordResponse setPasswordResponse = SetPasswordResponse.builder().uuid(UUID).firstLogin(false).build();
        given(restTemplate.postForObject(SAVE_PASSWORD_URL, setPassword, SetPasswordResponse.class)).willReturn(setPasswordResponse);

        SetPasswordResponse response = authorizationService.savePassword(setPassword);
        assertThat(response.getUuid(), is(UUID));
        assertThat(response.isFirstLogin(), is(false));
    }

    @Test
    public void testPasswordNotSaved() throws Exception {

        final SetPassword setPassword = SetPassword.builder().uuid(UUID).newPassword(PASSWORD).firstLogin(false).build();

        given(restTemplate.postForObject(SAVE_PASSWORD_URL, setPassword, SetPasswordResponse.class)).willReturn(null);

        SetPasswordResponse response = authorizationService.savePassword(setPassword);
        assertThat(response, is(nullValue()));
    }

    @Test
    public void testSavePasswordThrowsException() throws Exception {
      final SetPassword setPassword = SetPassword.builder().uuid(UUID).newPassword(PASSWORD).firstLogin(false).build();

      given(restTemplate.postForObject(SAVE_PASSWORD_URL, setPassword, SetPasswordResponse.class))
          .willAnswer(invocation -> { throw new Exception("Failed to save password"); });

      try {
        authorizationService.savePassword(setPassword);

      }catch (Exception e) {
        assertEquals("Failed to save password", e.getMessage());
      }

    }

    @Test
    public void testValidatePassword() throws Exception {

        final SetPassword setPassword = SetPassword.builder().uuid(UUID).passwordSameAsPrevious(PASSWORD_SAME_AS_PREVIOUS).build();

    boolean isPasswordSameAsPrevious = authorizationService.validatePassword(setPassword);
    assertThat(isPasswordSameAsPrevious, is(PASSWORD_SAME_AS_PREVIOUS));
  }

    @Test
    public void testValidatePasswordReturnsFalse() throws Exception {

        final SetPassword setPassword = SetPassword.builder().uuid(UUID).passwordSameAsPrevious(PASSWORD_SAME_AS_PREVIOUS).build();

    SetPasswordResponse setPasswordResponse = SetPasswordResponse.builder().uuid(UUID)
        .passwordSameAsPrevious(PASSWORD_SAME_AS_PREVIOUS).build();
    given(restTemplate.postForObject(VALIDATE_PASSWORD_URL, setPassword, SetPasswordResponse.class))
        .willReturn(null);

    boolean isPasswordSameAsPrevious = authorizationService.validatePassword(setPassword);
    assertThat(isPasswordSameAsPrevious, is(false));
  }

  @Test
  public void testValidatePasswordThrowsException() throws Exception {
    final SetPassword setPassword = SetPassword.builder().uuid(UUID).newPassword(PASSWORD).firstLogin(false).build();

    given(restTemplate.postForObject(SAVE_PASSWORD_URL, setPassword, SetPasswordResponse.class))
        .willAnswer(invocation -> { throw new Exception("Failed to validate password"); });

    try {
      authorizationService.validatePassword(setPassword);

    }catch (Exception e) {
      assertEquals("Failed to validate password", e.getMessage());
    }

  }

  @Test
  public void testRegisterUserFailsReturnsNull() throws Exception {

    RegistrationRequest request = RegistrationRequest.builder()
        .password("ValidPassword123")
        .role("ROLE_ADMIN")
        .username("valid@email.com")
        .build();

    given(restTemplate
        .postForObject(REGISTER_USER_URL, any(CreateUserRequest.class), CreateUserResponse.class))
        .willThrow(Exception.class);

    AuthenticationResponse response = authorizationService.registerUser(request);
    assertNull(response);
  }


  @Test
  public void testRegisterUserSuccessReturnsCreateUserResponse() throws Exception {

    RegistrationRequest request = RegistrationRequest.builder()
        .password("ValidPassword123")
        .role("ROLE_ADMIN")
        .username("valid@email.com")
        .build();

    AuthenticationResponse mockedResponse = AuthenticationResponse.builder()
        .uuid("2123121-32123115-45451").build();

    given(restTemplate
        .postForObject(REGISTER_USER_URL, request, AuthenticationResponse.class))
        .willReturn(mockedResponse);

    AuthenticationResponse response = authorizationService.registerUser(request);

    assertSame(response, mockedResponse);
  }

  @Test
  public void testRegisterUserThrowsException() throws Exception {
    RegistrationRequest request = RegistrationRequest.builder()
        .password("ValidPassword123")
        .role("ROLE_ADMIN")
        .username("valid@email.com")
        .build();

    given(restTemplate.postForObject(REGISTER_USER_URL, request, AuthenticationResponse.class))
        .willAnswer(invocation -> { throw new Exception("Failed to register user %s"); });

    try {
      authorizationService.registerUser(request);

    }catch (Exception e) {
      assertEquals("Failed to register user %s", e.getMessage());
    }

  }

  @Test
  public void testCheckUsernameThrowsException() throws Exception {
    ForgottenPassword request = ForgottenPassword.builder()
        .email("valid@email.com")
        .build();

    given(restTemplate.postForObject(VALIDATE_EMAIL_URL, request, ValidateEmailResponse.class))
        .willAnswer(invocation -> { throw new Exception("Failed to get Username = %s"); });

    try {
      authorizationService.checkUsername(request);

    }catch (Exception e) {
      assertEquals("Failed to get Username = %s", e.getMessage());
    }

  }

    @Test
    public void testValidateEmail() throws Exception {

        final ForgottenPassword forgottenPassword = ForgottenPassword.builder().email(CONTACT_EMAIL).build();

        final ValidateEmailResponse validEmailResponse = ValidateEmailResponse.builder().uuid(UUID).userName(CONTACT_EMAIL).build();
        given(restTemplate.postForObject(VALIDATE_EMAIL_URL, forgottenPassword, ValidateEmailResponse.class)).willReturn(validEmailResponse);

        final ValidateEmailResponse response = authorizationService.checkUsername(forgottenPassword);
        assertThat(response.getUuid(), is(UUID));
        assertThat(response.getUserName(), is(CONTACT_EMAIL));
    }

    @Test
    public void testValidateEmailReturnsNull() throws Exception {

        final ForgottenPassword forgottenPassword = ForgottenPassword.builder().email(CONTACT_EMAIL).build();

        given(restTemplate.postForObject(VALIDATE_EMAIL_URL, forgottenPassword, ValidateEmailResponse.class)).willReturn(null);

        final ValidateEmailResponse response = authorizationService.checkUsername(forgottenPassword);
        assertThat(response, is(nullValue()));
    }

    @Test
    public void testGetOptionalUuidByEmailTokenReturnsResponse() throws Exception {
      String token = "test.test@email.com";
      ResetEmailTokenResponse tokenResponse = ResetEmailTokenResponse.builder()
        .userUuid(UUID)
        .build();

      given(authorizationService.getUuidByEmailToken(token)).willReturn(tokenResponse);

      Optional<ResetEmailTokenResponse> response = authorizationService.getOptionalUuidByEmailToken(token);

      assertTrue(response.isPresent());
      assertEquals(response.get().getUserUuid(), UUID);

    }

    @Test
    public void testGetOptionalUuidByEmailTokenReturnsOptionalEmpty() throws Exception {
      String token = "test.test@email.com";

      given(authorizationService.getUuidByEmailToken(token)).willReturn(null);

      Optional<ResetEmailTokenResponse> response = authorizationService.getOptionalUuidByEmailToken(token);

      assertFalse(response.isPresent());

    }

    @Test
    public void testSendEmail() throws Exception {

        final ValidateEmailResponse validEmailResponse = ValidateEmailResponse.builder().uuid(UUID).userName(CONTACT_EMAIL).build();
        final FinanceUser financeUser = FinanceUser.builder().firstName(FIRST_NAME).lastName(LAST_NAME).username(USER_NAME).build();
        given(restTemplate.postForObject(SEND_EMAIL_URL, validEmailResponse, FinanceUser.class)).willReturn(financeUser);

        FinanceUser response = authorizationService.sendEmail(validEmailResponse);
        assertThat(response.getFirstName(), is(FIRST_NAME));
        assertThat(response.getLastName(), is(LAST_NAME));
        assertThat(response.getUsername(), is(USER_NAME));
    }

    @Test
    public void testSendEmailReturnsNull() throws Exception {

        final ValidateEmailResponse validEmailResponse = ValidateEmailResponse.builder().uuid(UUID).userName(CONTACT_EMAIL).build();
        given(restTemplate.postForObject(SEND_EMAIL_URL, validEmailResponse, FinanceUser.class)).willReturn(null);

        FinanceUser response = authorizationService.sendEmail(validEmailResponse);
        assertThat(response, is(nullValue()));
    }

    @Test
    public void testGetUuidByEmailToken() throws Exception {

        final ResetEmailTokenResponse resetEmailTokenResponse = ResetEmailTokenResponse.builder().userUuid(UUID).build();
        given(restTemplate.postForObject(GET_UUID_BY_TOKEN, "token", ResetEmailTokenResponse.class)).willReturn(resetEmailTokenResponse);

        ResetEmailTokenResponse response = authorizationService.getUuidByEmailToken("token");
        assertThat(response.getUserUuid(), is(UUID));
    }

    @Test
    public void testGetUuidByEmailTokenReturnsNull() throws Exception {

        given(restTemplate.postForObject(GET_UUID_BY_TOKEN, "token", ResetEmailTokenResponse.class)).willReturn(null);

        ResetEmailTokenResponse response = authorizationService.getUuidByEmailToken("token");
        assertThat(response, is(nullValue()));
    }

    @Test
    public void testGetUuidByEmailTokenThrowsException() throws Exception {
      String token = "test.test@email.com";

      given(restTemplate.postForObject(GET_UUID_BY_TOKEN, token, ResetEmailTokenResponse.class))
          .willAnswer(invocation -> { throw new Exception("Failed to verify Token"); });

      try {
        authorizationService.getUuidByEmailToken(token);

      }catch (Exception e) {
        assertEquals("Failed to verify Token", e.getMessage());
      }

    }

    @Test
    public void testEmailTokenIsDeleted() throws Exception {


        given(restTemplate.postForObject(DELETE_TOKEN, "token", boolean.class)).willReturn(true);

        boolean response = authorizationService.deleteEmailToken("token");

        assertThat(response, is(true));
    }

    @Test
    public void testEmailTokenIsNotDeleted() throws Exception {

        given(restTemplate.postForObject(DELETE_TOKEN, "token", boolean.class)).willReturn(false);

        boolean response = authorizationService.deleteEmailToken("token");

        assertThat(response, is(false));
    }

    @Test
    public void testDeleteEmailTokenThrowsException() throws Exception {
      String token = "test.test@email.com";

      given(restTemplate.postForObject(DELETE_TOKEN, token, boolean.class))
          .willAnswer(invocation -> { throw new Exception("Failed to delete Token"); });

      try {
        authorizationService.deleteEmailToken(token);

      }catch (Exception e) {
        assertEquals("Failed to delete Token", e.getMessage());
      }

    }

  @Test
  public void testGetUserByEmail() throws Exception {

    String email = "email@email.com";

    User expectedUser = User.builder().name(email).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/email/{email}/").params(expectedUser.getName()), User.class)).willReturn(expectedUser);

    Optional<User> currentUser = authorizationService.getUserByEmail(email);

    assertEquals(currentUser.get().getName(), expectedUser.getName());

  }

  @Test
  public void testGetUserByEmailThrowsException() throws Exception {
    String email = "email@email.com";
    User expectedUser = User.builder().name(email).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/email/{email}/")
        .params(expectedUser.getName()), User.class))
        .willAnswer(invocation -> { throw new Exception("Failed getting authentication user"); });

    try {
      authorizationService.getUserByEmail(email);

    }catch (Exception e) {
      assertEquals("Failed getting authentication user", e.getMessage());
    }

  }

  @Test
  public void testGetStandardUsersReturnsAccountUserList() throws Exception {

    User expectedUser = User.builder().id(1L).uuid(UUID).name("Standard user").role("ROLE_STANDARD").deletedOn(null).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}").params(expectedUser.getUuid()), User.class)).willReturn(expectedUser);

    List<FinanceUser> financeUserList = Collections.singletonList(FinanceUser.builder().uuid(UUID).firstName("Standard").lastName("user").role("ROLE_STANDARD").build());

    List<AccountUser> accountUsers = authorizationService.getStandardUsers(financeUserList);

    assertEquals(accountUsers.get(0).getUuid(),financeUserList.get(0).getUuid());
    assertEquals(accountUsers.get(0).getUserRole(),expectedUser.getRole());
    assertEquals(accountUsers.get(0).getName(),financeUserList.get(0).getFirstName()+' '+financeUserList.get(0).getLastName());

  }

  @Test
  public void testGetStandardUsersWithAdminUserReturnsNoAccountUser() throws Exception {

    User expectedUser = User.builder().id(1L).uuid(UUID).name("Standard user").role("ROLE_ADMIN").deletedOn(null).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}").params(expectedUser.getUuid()), User.class)).willReturn(expectedUser);

    List<FinanceUser> financeUserList = Collections.singletonList(FinanceUser.builder().uuid(UUID).firstName("Standard").lastName("user").role("ROLE_ADMIN").build());

    List<AccountUser> accountUsers = authorizationService.getStandardUsers(financeUserList);

    assertTrue(accountUsers.isEmpty());

  }

  @Test
  public void testGetStandardUsersWhosDeletedReturnsNoAccountUser() throws Exception {

    User expectedUser = User.builder().id(1L).uuid(UUID).name("Standard user").role("ROLE_STANDARD").deletedOn(DELETED_ON).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}").params(expectedUser.getUuid()), User.class)).willReturn(expectedUser);

    List<FinanceUser> financeUserList = Collections.singletonList(FinanceUser.builder().uuid(UUID).firstName("Standard").lastName("user").role("ROLE_ADMIN").build());

    List<AccountUser> accountUsers = authorizationService.getStandardUsers(financeUserList);

    assertTrue(accountUsers.isEmpty());

  }

  @Test
  public void testGetUserByUuidReturnsUser() throws Exception {

    User expectedUser = User.builder().uuid(UUID).build();

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}").params(expectedUser.getUuid()), User.class)).willReturn(expectedUser);

    Optional<User> currentUser = authorizationService.getUserByUuid(UUID);

    assertEquals(currentUser.get().getUuid(), expectedUser.getUuid());

  }

  @Test
  public void testGetUserByUuidReturnsNull() throws Exception {

    String role = "ROLE_STANDARD";

    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}").params(UUID), User.class)).willReturn(null);

    Optional<User> currentUser = authorizationService.getUserByUuid(UUID);

    assertFalse(currentUser.isPresent());

  }

  @Test
  public void testGetUserByUuidThrowsException() throws Exception {
    given(restTemplate.getForObject(backendAuthenticationApiUriService.path("/user/{uuid}")
        .params(UUID), User.class))
        .willAnswer(invocation -> { throw new Exception("Failed getting authentication user with uuid"); });

    try {
      authorizationService.getUserByUuid(UUID);

    }catch (Exception e) {
      assertEquals("Failed getting authentication user with uuid", e.getMessage());
    }

  }

  @Test
  public void testDeleteUserByUuidReturnsUser() throws Exception {

    User expectedUser = User.builder().uuid(UUID).build();

    given(restTemplate.postForObject(backendAuthenticationApiUriService.path("/user/delete-user/{uuid}")
        .params(expectedUser.getUuid()), expectedUser.getUuid(), User.class))
        .willReturn(expectedUser);

    Optional<User> currentUser = authorizationService.deleteUser(UUID);

    assertEquals(currentUser.get().getUuid(), expectedUser.getUuid());

  }

  @Test
  public void testDeleteUserByUuidReturnsOptionalEmpty() throws Exception {

    User expectedUser = User.builder().uuid(UUID).build();

    given(restTemplate.postForObject(backendAuthenticationApiUriService.path("/user/delete-user/{uuid}")
        .params(expectedUser.getUuid()), expectedUser.getUuid(), User.class))
        .willReturn(null);

    Optional<User> currentUser = authorizationService.deleteUser(UUID);

    assertFalse(currentUser.isPresent());

  }
}