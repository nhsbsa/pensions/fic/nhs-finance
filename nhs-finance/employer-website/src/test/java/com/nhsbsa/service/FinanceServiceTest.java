package com.nhsbsa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailRequest;
import com.nhsbsa.security.EmailResponse;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
@WithMockUser
public class FinanceServiceTest {

  private static final String EXPECTED_UUID = "user-uuid";
  private static final String EXPECTED_ORG_UUID = "org-uuid";

  private static final FinanceUser FINANCE_USER = FinanceUser.builder()
      .uuid(EXPECTED_UUID)
      .build();

  private FinanceService financeService;

  private BackendApiUriService backendApiUriService;

  private RestTemplate restTemplate;

  @Before
  public void before() {
    restTemplate = Mockito.mock(RestTemplate.class);

    backendApiUriService = new BackendApiUriService(
        "http",
        "host",
        "8080",
        "/employer-service-context"
    );

    financeService = Mockito.spy(new FinanceService(backendApiUriService,restTemplate));

  }

  @Test
  public void getFinanceUserByUuidReturnsUser() {

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/user/" + EXPECTED_UUID,
        FinanceUser.class)).willReturn(FINANCE_USER);

    Optional<FinanceUser> actualUser = financeService.getFinanceUserByUuid(EXPECTED_UUID);

    assertEquals(actualUser.get().getUuid(), FINANCE_USER.getUuid());
  }

  @Test
  public void getFinanceUserByUuidThrowsException() {

    HttpClientErrorException httpClientErrorException = new HttpClientErrorException((HttpStatus.BAD_REQUEST));

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/user/" + EXPECTED_UUID,
        FinanceUser.class)).willThrow(httpClientErrorException);

    try {
      financeService.getFinanceUserByUuid(EXPECTED_UUID);

    }catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
    }

  }

  @Test
  public void getFinanceUserByOrgUuidReturnsUser() {

    FinanceUser[] userList = new FinanceUser[1];
    userList[0] = FINANCE_USER;

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/org-users/" + EXPECTED_ORG_UUID,
        FinanceUser[].class)).willReturn(userList);

    List<FinanceUser> actualUser = financeService.getFinanceUserByOrgUuid(EXPECTED_ORG_UUID);

    assertEquals(actualUser.get(0).getUuid(), userList[0].getUuid());
  }

  @Test
  public void getFinanceUserByOrgUuidReturnsNull() {

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/org-users/" + EXPECTED_ORG_UUID,
        FinanceUser[].class)).willReturn(null);

    List<FinanceUser> actualUser = financeService.getFinanceUserByOrgUuid(EXPECTED_ORG_UUID);

    assertTrue(actualUser.isEmpty());
  }

  @Test
  public void getFinanceUserByOrgUuidThrowsException() {

    HttpClientErrorException httpClientErrorException = new HttpClientErrorException((HttpStatus.NOT_FOUND));

    given(restTemplate.getForObject("http://host:8080/employer-service-context/finance/org-users/" + EXPECTED_ORG_UUID,
        FinanceUser[].class)).willThrow(httpClientErrorException);

    try {
      financeService.getFinanceUserByOrgUuid(EXPECTED_ORG_UUID);

    }catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
    }

  }

  @Test
  public void createUserReturnsResponse() {

    CreateUserRequest request = CreateUserRequest.builder().uuid(EXPECTED_UUID).build();
    CreateUserResponse response = CreateUserResponse.builder().uuid(EXPECTED_UUID).build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/user",
        request, CreateUserResponse.class)).willReturn(response);

    CreateUserResponse actualUser = financeService.createUser(request);

    assertEquals(actualUser.getUuid(), EXPECTED_UUID);
  }

  @Test
  public void createUserReturnsNull() {

    CreateUserRequest request = CreateUserRequest.builder().uuid(EXPECTED_UUID).build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/user",
        request, CreateUserResponse.class)).willReturn(null);

    CreateUserResponse actualUser = financeService.createUser(request);

    assertNull(actualUser);
  }

  @Test
  public void createUserThrowsException() {

    CreateUserRequest request = CreateUserRequest.builder().uuid(EXPECTED_UUID).build();

    HttpClientErrorException httpClientErrorException = new HttpClientErrorException((HttpStatus.NO_CONTENT));

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/user",
        request, CreateUserResponse.class)).willThrow(httpClientErrorException);

    try {
      financeService.createUser(request);

    }catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.NO_CONTENT, e.getStatusCode());
    }
  }

  @Test
  public void sendStandardUserCreatedEmailReturnsResponse() {

    EmailRequest request = EmailRequest.builder().firstName("Test").lastName("Last name").email("email").createdBy("Sam Jones").build();
    EmailResponse response = EmailResponse.builder().name("Test's").build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/standard-user-email",
        request, EmailResponse.class)).willReturn(response);

    Optional<EmailResponse> actual = financeService.sendStandardUserCreatedEmail(request);

    assertEquals(actual.get().getName(), "Test's");
  }

  @Test
  public void sendStandardUserCreatedEmailReturnsNull() {

    EmailRequest request = EmailRequest.builder().firstName("Test").lastName("Last name").email("email").createdBy("Sam Jones").build();
    EmailResponse response = EmailResponse.builder().name("Test's").build();

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/standard-user-email",
        request, EmailResponse.class)).willReturn(null);

    Optional<EmailResponse> actual = financeService.sendStandardUserCreatedEmail(request);

    assertFalse(actual.isPresent());
  }

  @Test
  public void sendStandardUserCreatedEmailThrowsException() {

    EmailRequest request = EmailRequest.builder().firstName("Test").lastName("Last name").email("email").createdBy("Sam Jones").build();

    HttpClientErrorException httpClientErrorException = new HttpClientErrorException((HttpStatus.METHOD_NOT_ALLOWED));

    given(restTemplate.postForObject("http://host:8080/employer-service-context/finance/standard-user-email",
        request, EmailResponse.class)).willThrow(httpClientErrorException);

    try {
      financeService.sendStandardUserCreatedEmail(request);

    }catch (HttpClientErrorException e) {
      assertEquals(HttpStatus.METHOD_NOT_ALLOWED, e.getStatusCode());
    }

  }

}