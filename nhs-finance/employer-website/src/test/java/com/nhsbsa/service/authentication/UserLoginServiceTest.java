package com.nhsbsa.service.authentication;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import com.nhsbsa.exceptions.LoginAuthenticationException;
import com.nhsbsa.exceptions.UserNotFoundAuthenticationException;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.service.BackendAuthenticationApiUriService;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


/**
 * Created by jeffreya on 24/10/2016.
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class UserLoginServiceTest {

    @Mock
    BackendAuthenticationApiUriService backendAuthenticationApiUriService;

    private  final UserLoginService USER_LOGIN = new UserLoginService(null);

    private  UserLoginService USER_LOGIN_SERVICE;

    @Before
    public void init(){

        when(backendAuthenticationApiUriService.path(anyString())).thenReturn(null);

        USER_LOGIN_SERVICE = new UserLoginService(new AuthorizationService(backendAuthenticationApiUriService,null) {

            @Override
            public Optional<AuthenticationResponse> authenticate(LoginRequest loginRequest) {
                return Optional.empty();
            }
        });


    }

    @Test(expected = LoginAuthenticationException.class)
    public void financeUserLoginNoCredentials() throws Exception {
        USER_LOGIN.financeLogin(null, null);
    }
    @Test(expected = LoginAuthenticationException.class)
    public void financeUserLoginBlankUserCredentials() throws Exception {
        USER_LOGIN.financeLogin("", null);
    }

    @Test(expected = LoginAuthenticationException.class)
    public void financeUserLoginBlankPswCredentials() throws Exception {
        USER_LOGIN.financeLogin(null, "");
    }

    @Test(expected = LoginAuthenticationException.class)
    public void financeUserLoginBlankCredentials() throws Exception {
        USER_LOGIN.financeLogin("", "");
    }

    @Test(expected = UserNotFoundAuthenticationException.class)
    public void financeUserNotFound() throws Exception {
        USER_LOGIN_SERVICE.financeLogin("abc", "123");
    }

}