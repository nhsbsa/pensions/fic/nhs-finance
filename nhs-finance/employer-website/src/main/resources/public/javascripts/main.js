(function ($, window) {
    "use strict";

    var nhsbsa = {};

    nhsbsa.properties = {
        isLessThanIE9: false
    };

    nhsbsa.environment = {
        init: function (){
            if ($('html').hasClass('lt-ie9')) {
                nhsbsa.properties.isLessThanIE9 = true;
            }
        }
    };

    nhsbsa.utils = {
        show: function(obj){
            $(obj).removeClass('js-hidden');
        },

        hide: function(obj){
            $(obj).addClass('js-hidden');
        }
    };


    //==================================
    
    
    nhsbsa.toggles = {
        switches: $('.js-toggle'),
        init: function(){
            nhsbsa.toggles.switches.each(function(i, obj){
                $(obj).on('click', function(){
                    var idToHide = $(this).attr('data-hide');
                    var idToShow = $(this).attr('data-show');
                    nhsbsa.utils.hide(idToHide);
                    nhsbsa.utils.show(idToShow);
                })
            })
        }
    }
    

    nhsbsa.fixesForIsLessThanIE9 = {
        tags: ['input', 'button'],
        addFocusEvent: function(obj){
            $(obj).on('focus', function () {
                $(this).addClass('focus');
            }).on('blur', function () {
                $(this).removeClass('focus');
            })
        },

        init: function(){
            if(nhsbsa.properties.isLessThanIE9){
                $(nhsbsa.fixesForIsLessThanIE9.tags).each(function (i, obj) {
                    nhsbsa.fixesForIsLessThanIE9.addFocusEvent($(obj));
                });
            }
        }
    };

    nhsbsa.init = function () {
        // all init
        nhsbsa.environment.init();
        nhsbsa.fixesForIsLessThanIE9.init();
        nhsbsa.toggles.init();
    };

    // main init
    $(document).ready(function () {
        nhsbsa.init();
    });

}(jQuery, window));