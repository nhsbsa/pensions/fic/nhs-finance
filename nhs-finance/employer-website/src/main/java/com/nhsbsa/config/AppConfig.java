package com.nhsbsa.config;

import com.nhsbsa.interceptors.ApplicationHeaderInterceptor;
import java.util.Arrays;
import java.util.Locale;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Created by jeffreya on 18/08/2016.
 * AppConfig
 */
@Configuration
public class AppConfig {


    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        return new TomcatEmbeddedServletContainerFactory();
    }

    @Bean
    public ApplicationHeaderInterceptor applicationHeaderInterceptor() {
        return new ApplicationHeaderInterceptor();
    }

    @Bean
    @Profile("!user-propagation")
    public RestTemplate ficRestTemplate() {
        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setInterceptors(Arrays.asList(applicationHeaderInterceptor()));
        return restTemplate;
    }

    @Bean
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver sessionLocaleResolver = new SessionLocaleResolver();
        sessionLocaleResolver.setDefaultLocale(Locale.ENGLISH);
        return sessionLocaleResolver;
    }

}
