package com.nhsbsa.service.authentication;

import com.nhsbsa.model.EmployerUser;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.RegistrationRequest;
import com.nhsbsa.service.FinanceService;
import java.util.Optional;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Log4j
@Service
public class UserRegistrationService {

  private final AuthorizationService authorizationService;
  private final FinanceService financeService;

  @Autowired
  public UserRegistrationService(final AuthorizationService authorizationService,
      final FinanceService financeService) {
    this.authorizationService = authorizationService;
    this.financeService = financeService;
  }

  // TODO Remove this method once registerAuthenticationUser has been tested
  public Optional<CreateUserResponse> createEmployerUser(
      EmployerUser employerUserToCreate) {

    return registerUserInEmployerSchema(employerUserToCreate);
  }

  // TODO Remove this method once registerAuthenticationUser has been tested
  public Optional<AuthenticationResponse> createAuthenticationUser(EmployerUser authenticationUserToCreate){

    return Optional
        .ofNullable(registerUserInAuthenticationSchema(authenticationUserToCreate));
  }

  public Optional<AuthenticationResponse> registerAuthenticationUser(RegistrationRequest authenticationUserToCreate){

    return Optional
        .ofNullable(authorizationService.registerUser(authenticationUserToCreate));

  }

  public Optional<CreateUserResponse> createFinanceUser(
      CreateUserRequest userToCreate) {

    return Optional.of(financeService.createUser(userToCreate));

  }

  private AuthenticationResponse registerUserInAuthenticationSchema(
      EmployerUser employerUserToCreate) {

    final RegistrationRequest registrationRequest = buildEmployerUserRegistrationRequestForAuthenticationSchema(
        employerUserToCreate);

    return authorizationService.registerUser(registrationRequest);
  }

  private RegistrationRequest buildEmployerUserRegistrationRequestForAuthenticationSchema(
      EmployerUser employerUserToCreate) {

    return RegistrationRequest.builder()
        .username(employerUserToCreate.getEmail().toLowerCase())
        .password(employerUserToCreate.getFirstPassword())
        .role(employerUserToCreate.getRole().name())
        .build();
  }

  Optional<CreateUserResponse> registerUserInEmployerSchema(
      EmployerUser employerUserToCreate){

    final Optional<CreateUserRequest> createUserRequest = buildEmployerUserCreationRequestForEmployerSchema(
        employerUserToCreate);

    return createUserRequest.map(financeService::createUser);
  }

  private Optional<CreateUserRequest> buildEmployerUserCreationRequestForEmployerSchema(
      EmployerUser employerUserToCreate) {

    return Optional.of(CreateUserRequest.builder()
        .username(employerUserToCreate.getEmail().toLowerCase())
        .organisations(employerUserToCreate.getOrganisations())
        .firstName(employerUserToCreate.getFirstName())
        .surname(employerUserToCreate.getLastName())
        .contactNumber(employerUserToCreate.getContactTelephone())
        .uuid(employerUserToCreate.getUuid())
        .build());
  }
}
