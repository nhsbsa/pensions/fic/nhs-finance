package com.nhsbsa.security;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.UserRoles;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by nataliehulse on 04/10/2017.
 * This class has been used for now, may change
 */
public class UrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {


    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws IOException {

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication)
            throws IOException {

        String targetUrl = determineTargetUrl(authentication);

        if (response.isCommitted()) {
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(Authentication authentication) {


        FinanceUser currentUser = (FinanceUser) authentication.getDetails();

        boolean isFirstTimeLogin = currentUser.isFirstLogin();

        if (isFirstTimeLogin) {
            return "/set-password";
        } else {

            if(currentUser.getRole().equals(UserRoles.ROLE_MASTER.name())) {

                return "/download-files";
            } else if(currentUser.getOrganisations().get(0).getEas().size()>1) { // Multi eas
                return "/multi-ea";
            } else {
                return "/scheduleyourpayment";
            }

        }
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

}