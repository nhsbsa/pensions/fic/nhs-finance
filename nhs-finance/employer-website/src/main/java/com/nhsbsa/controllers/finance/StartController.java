package com.nhsbsa.controllers.finance;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StartController {

    private static final String START_PAGE = "start";

    @GetMapping(value = {"/", "/start"})
    public String scheduleyourpayment() {
        return START_PAGE;
    }
}
