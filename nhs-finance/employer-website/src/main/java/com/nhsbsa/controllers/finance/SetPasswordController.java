package com.nhsbsa.controllers.finance;

/**
 * Created by nataliehulse on 28/09/2017.
 */

import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.validation.CheckPreviousPasswordValidator;
import com.nhsbsa.model.validation.ComparePasswordValidator;
import com.nhsbsa.security.SetPassword;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@Slf4j
public class SetPasswordController {

    private final AuthorizationService authorizationService;


    private ComparePasswordValidator comparePasswordValidator = new ComparePasswordValidator();

    private CheckPreviousPasswordValidator checkPreviousPasswordValidator = new CheckPreviousPasswordValidator();


    private static final String SET_PASS_VIEW_NAME = "setpassword";

    // This will need to change to the Confirm Password Changed View once it's been created
    private static final String PASS_UPDATED_REDIRECT = "redirect:/passwordUpdated";


    @Autowired
    public SetPasswordController(final AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @RequestMapping(value = "/set-password", method = RequestMethod.GET)
    public ModelAndView showSetPasswordPage() {

        log.info("GET - Accessing to Set Password screen");

        ModelAndView modelAndView = new ModelAndView(SET_PASS_VIEW_NAME);
        modelAndView.addObject("setPassword", new SetPassword());

        return modelAndView;
    }

    @PostMapping("/set-password")
    public String postSetPasswordPage(@ModelAttribute @Valid SetPassword setPassword, final BindingResult bindingResult) {

        FinanceUser user = (FinanceUser) SecurityContextHolder.getContext().getAuthentication().getDetails();

        log.info("POST - Setting password to user {}", user.getUsername());

        setPassword.setUuid(user.getUuid());

        // perform a check against currently saved password
        setPassword.setPasswordSameAsPrevious(authorizationService.validatePassword(setPassword));
        checkPreviousPasswordValidator.validate(setPassword, bindingResult);

        // validation to check passwords match
        comparePasswordValidator.validate(setPassword, bindingResult);


        if (bindingResult.hasErrors()) {
            return SET_PASS_VIEW_NAME;
        }

        // save new password to db and update first_login flag to false
        setPassword.setFirstLogin(false);

        log.info("Set Password - Saving validated password of user = {} ", user.getUsername());
        authorizationService.savePassword(setPassword);


        return PASS_UPDATED_REDIRECT;
    }



}
