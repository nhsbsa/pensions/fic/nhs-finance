package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.model.validation.SchedulePaymentValidationSequence;
import com.nhsbsa.service.RequestForTransferService;
import com.nhsbsa.utility.ExampleDateGenerator;
import com.nhsbsa.view.RequestForTransferView;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for /sheduleyourpayment endpoints
 *
 * Created by nataliehulse on 03/11/2016.
 */
@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN') or hasRole('STANDARD')")
public class ScheduleYourPaymentController {

  private static final String SCHEDULE_YOUR_PAYMENT = "scheduleyourpayment";
  private final RequestForTransferService requestForTransferService;
  private final ExampleDateGenerator exampleDateGenerator;

  static final String PAYMENT_MONTH_EXAMPLE_ATTR_NAME = "paymentMonthExample";
  static final String HINT_DATE_EXAMPLE_ATTR_NAME = "hintDateExample";
  static final String USER_ROLE_ATTR_NAME = "userRole";
  static final String EA_CODE = "eaCode";

  @Autowired
  public ScheduleYourPaymentController(final RequestForTransferService requestForTransferService,
      final ExampleDateGenerator exampleDateGenerator) {
    this.requestForTransferService = requestForTransferService;
    this.exampleDateGenerator = exampleDateGenerator;
  }

  @GetMapping(value = "/scheduleyourpayment") // Single Ea
  public ModelAndView scheduleyourpayment() {
    log.info("GET - Accessing to Schedule your payment screen");

    Optional<FinanceUser> currentUser = getCurrentUser();

    ModelAndView modelAndView = new ModelAndView(SCHEDULE_YOUR_PAYMENT);
    modelAndView.addObject("rft", new RequestForTransferView());
    modelAndView.addObject(PAYMENT_MONTH_EXAMPLE_ATTR_NAME, exampleDateGenerator.generatePaymentMonthExample());
    modelAndView.addObject(HINT_DATE_EXAMPLE_ATTR_NAME, exampleDateGenerator.generateFormattedPaymentHintDate());
    modelAndView.addObject(USER_ROLE_ATTR_NAME, currentUser.isPresent()?currentUser.get().getRole():"");
    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    modelAndView.addObject(EA_CODE, currentUser.isPresent()?currentUser.get().getOrganisations().get(0).getEas().get(0).getEaCode():"");
    return modelAndView;
  }

  /**
   * Display schedule page with existing request for transfer data. This handler is invoked when one
   * of the application 'go back to' links is followed from either the contributions and payments or
   * the summary pages.
   *
   * @param rftUuid Uuid allocated to RFT object
   * @param forwardTo (Optional) page to forward on to after processing the post
   * @param response Injected instance to support {@code sendError} call
   * @return Spring model and view
   * @throws IOException if there is an IO exception during controller -> spring service -> rest
   * service and back calls.
   */
  @GetMapping(value = "/scheduleyourpayment/{rftUuid}")
  public ModelAndView schedulePaymentWithRft(@PathVariable("rftUuid") String rftUuid,
      @RequestParam(name = "forward", required = false) String forwardTo,
      HttpServletResponse response) throws IOException {

    log.info("GET - Accessing to Schedule your payment screen with rft uuid = {}", rftUuid);

    ModelAndView modelAndView = new ModelAndView(SCHEDULE_YOUR_PAYMENT);
    RequestForTransferView rft;

    Optional<RequestForTransferView> optionalRft = requestForTransferService
        .getOptionalRequestForTransferByRftUuid(rftUuid);
    if (optionalRft.isPresent()) {

      Optional<FinanceUser> currentUser = getCurrentUser();

      rft = optionalRft.get();
      rft.setForwardTo(forwardTo);

      modelAndView.addObject("rft", rft);
      modelAndView.addObject(PAYMENT_MONTH_EXAMPLE_ATTR_NAME, exampleDateGenerator.generatePaymentMonthExample());
      modelAndView.addObject(HINT_DATE_EXAMPLE_ATTR_NAME, exampleDateGenerator.generateFormattedPaymentHintDate());
      modelAndView.addObject(USER_ROLE_ATTR_NAME, currentUser.isPresent()?currentUser.get().getRole():"");
      modelAndView.addObject(EA_CODE, rft.getEaCode());

      // Need to set payAsSoonAsPossible field on page (as not saved in database)
      if(rft.getTransferDate()!=null) {
        Date transferDate = rft.getTransferDate().getDate();
        Date generatedDateFromLocalDT = Date.from(exampleDateGenerator.generateTransferDate().atStartOfDay(ZoneId.systemDefault()).toInstant());

        if(generatedDateFromLocalDT.compareTo(transferDate)==0) {
          rft.setPayAsSoonAsPossible("Y");
        } else {
          rft.setPayAsSoonAsPossible("N");
        }
      }

      return modelAndView;
    }

    log.error("request for transfer with rft uuid = {} not found", rftUuid);
    response.sendError(404);
    return null;

  }

  /**
   * Get schedule page for multi eas.
   * Set rft.eaCode = param eaCode
   *
   */
  @GetMapping(value = "/scheduleyourpayment/ea/{eaCode}")
  public ModelAndView schedulePaymentWithEaCode(@PathVariable("eaCode") String eaCode, HttpServletResponse httpResponse) throws IOException{

    log.info("GET - Accessing to Schedule your payment screen with EaCode = {}", eaCode);

    Optional<FinanceUser> currentUser = getCurrentUser();

    if(!currentUser.isPresent()) {
      log.error("currentUser not found");
      httpResponse.sendError(404);
      return null;
    }

    //Check Ea Code param exists in CurrentUser.EaList
      boolean eaExists = doesEaCodeBelongToCurrentUser(currentUser.get(), eaCode);
      if (!eaExists) {
        log.error("eaCode = {} not found", eaCode);
        httpResponse.sendError(404);
        return null;
      }

      RequestForTransferView rft = new RequestForTransferView();
      rft.setEaCode(eaCode);

      ModelAndView modelAndView = new ModelAndView(SCHEDULE_YOUR_PAYMENT);
      modelAndView.addObject("rft", rft);
      modelAndView.addObject(PAYMENT_MONTH_EXAMPLE_ATTR_NAME, exampleDateGenerator.generatePaymentMonthExample());
      modelAndView.addObject(HINT_DATE_EXAMPLE_ATTR_NAME, exampleDateGenerator.generateFormattedPaymentHintDate());
      modelAndView.addObject(USER_ROLE_ATTR_NAME, currentUser.get().getRole());
      modelAndView.addObject(EA_CODE, eaCode);
      return modelAndView;

    }


  /**
   * Post schedule page. If rft.rftUuid on template = "null" then create new instance,
   * else update existing instance.
   *
   */
  @PostMapping(value = "/scheduleyourpayment/{eaCode}")
  public String updatePaymentSchedule(
      @Validated(SchedulePaymentValidationSequence.class) @ModelAttribute("rft") final RequestForTransferView formObject,
      final BindingResult bindingResult,
      ModelMap map,
      @PathVariable("eaCode") String eaCode, HttpServletResponse httpResponse) throws IOException {

    log.info("POST - Schedule your payment with rft uuid = {}", formObject.getRftUuid());

    if (bindingResult.hasErrors()) {
      map.addAttribute(PAYMENT_MONTH_EXAMPLE_ATTR_NAME, exampleDateGenerator.generatePaymentMonthExample());
      map.addAttribute(HINT_DATE_EXAMPLE_ATTR_NAME, exampleDateGenerator.generateFormattedPaymentHintDate());
      map.addAttribute(EA_CODE, eaCode);
      return SCHEDULE_YOUR_PAYMENT;
    }

    //Get Current User to check EA Code param value is valid
    Optional<FinanceUser> currentUser = getCurrentUser();
    if(!currentUser.isPresent()) {
      log.error("currentUser not found");
      httpResponse.sendError(404);
      return null;
    }

    //Check Ea Code param exists in CurrentUser.EaList
    boolean eaExists = doesEaCodeBelongToCurrentUser(currentUser.get(), eaCode);
    if (!eaExists) {
      log.error("eaCode = {} not found", eaCode);
      httpResponse.sendError(404);
      return null;
    }

    if("Y".equals(formObject.getPayAsSoonAsPossible())) {
      LocalDate transferDate = exampleDateGenerator.generateTransferDate();
      TransferFormDate transferFormDate = new TransferFormDate();
      transferFormDate.setDays(Integer.toString(transferDate.getDayOfMonth()));
      transferFormDate.setMonth(transferDate.getMonth().toString());
      transferFormDate.setYear(Integer.toString(transferDate.getYear()));
      formObject.setTransferDate(transferFormDate);
    }

    // Set eaCode in formObject
    formObject.setEaCode(eaCode);

    // Template passes null RftUuid as "" so transform to null
    // else set to itself
    if("".equals(formObject.getRftUuid())) {
      formObject.setRftUuid(null);
    }else{
      formObject.setRftUuid(formObject.getRftUuid());
    }

    if (formObject.getRftUuid() != null) {
      // Load existing entity so the schedule payment details can be updated.
      // It is an error for the RFT entity to not exist already at this point.
      RequestForTransferView savedRequestForTransfer = requestForTransferService
          .getRequestForTransferByRftUuid(formObject.getRftUuid());
      ScheduleYourPaymentController
          .mergePaymentScheduleDetails(savedRequestForTransfer, formObject);
      savedRequestForTransfer = requestForTransferService
          .saveRequestForTransfer(savedRequestForTransfer);
      if ("summary".equals(formObject.getForwardTo())) {
        return "redirect:/" + formObject.getForwardTo() + "/" + savedRequestForTransfer
            .getRftUuid();
      } else {
        return "redirect:/contributionsandpayment/" + savedRequestForTransfer.getRftUuid();
      }
    } else {
      RequestForTransferView savedRequestForTransfer = requestForTransferService
          .saveRequestForTransfer(formObject);
      return "redirect:/contributionsandpayment/" + savedRequestForTransfer.getRftUuid();
    }
  }

  /**
   * Update properties managed by the schedule your payment page held in {@code into} with values
   * from {@code from}. The latter has been populated from values on the schedule your payment form
   * and supercede those that already exist.
   *
   * @param into Object to populate with payment schedule details held in {@code from}
   * @param from Object holding payment schedule details to copy to {@code into}
   */
  private static void mergePaymentScheduleDetails(RequestForTransferView into,
      RequestForTransferView from) {
    into.setTransferDate(from.getTransferDate());
    // hack to force entity to be persisted when only the transfer date
    // field is changed; at the moment this appears to fail to flag the entity
    // as dirty in the hibernate cache.
    into.setContributionDate(from.getContributionDate());
  }

  Optional<FinanceUser> getCurrentUser() {

    final Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
    if (details instanceof FinanceUser) {
      return Optional.of((FinanceUser) details);
    }

    return Optional.empty();

  }

  private boolean doesEaCodeBelongToCurrentUser(FinanceUser currentUser, String eaCode) {
    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    return currentUser.getOrganisations().get(0).getEas().stream()
        .anyMatch(ea -> eaCode.equals(ea.getEaCode()));

  }
}