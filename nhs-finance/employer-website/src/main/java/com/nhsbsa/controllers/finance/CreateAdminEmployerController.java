package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.AdminEmployerUser;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.model.validation.AdminEmployerValidationGroup;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.service.OrganisationService;
import com.nhsbsa.service.authentication.AuthorizationService;
import com.nhsbsa.service.authentication.UserRegistrationService;
import com.nhsbsa.view.OrganisationView;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('MASTER')")
@SessionAttributes("adminEmployerUser")
//TODO Remove this controller once CreateAdminUserController tested
public class CreateAdminEmployerController {

  private static final String CREATE_ADMIN_EMPLOYER_VIEW_NAME = "createAdminEmployer";
  private static final String ADMIN_EMPLOYER_DETAILS_VIEW_NAME = "adminEmployerDetails";

  private static final String CREATE_ADMIN_EMPLOYER_ENDPOINT = "/create-admin-employer";
  private static final String ADMIN_EMPLOYER_DETAILS_ENDPOINT = "/admin-employer-details";
  private static final String CREATE_FINANCE_EMPLOYER_USER_ENDPOINT = "/create-finance-employer-user";

  private static final String ADMIN_EMPLOYER_DETAILS_FORWARD = "forward:/admin-employer-details";
  private static final String EMPLOYER_CREATED_FORWARD = "forward:/employer-created";

  private static final String NEW_USER_MAIL = "userMail";
  private static final String ENDPOINT = "endpointAttribute";

  private final OrganisationService organisationService;
  private final AuthorizationService authorizationService;
  private final UserRegistrationService userRegistrationService;
  private final FinanceService financeService;

  @Autowired
  public CreateAdminEmployerController(final OrganisationService organisationService,
      AuthorizationService authorizationService, UserRegistrationService userRegistrationService, FinanceService financeService) {
    this.organisationService = organisationService;
    this.authorizationService = authorizationService;
    this.userRegistrationService = userRegistrationService;
    this.financeService = financeService;
  }

  @GetMapping(value = CREATE_ADMIN_EMPLOYER_ENDPOINT)
  public ModelAndView showCreateAdminEmployerPage() {
    log.info("GET - Accessing to Create Admin Employer screen - Step 1");

    ModelAndView modelAndView = new ModelAndView(CREATE_ADMIN_EMPLOYER_VIEW_NAME);
    modelAndView.addObject("adminEmployerUser", new AdminEmployerUser());

    return modelAndView;
  }

  @PostMapping(value = CREATE_ADMIN_EMPLOYER_ENDPOINT)
  public String createAdminEmployer(
      @ModelAttribute @Valid AdminEmployerUser adminEmployerUser,
      final BindingResult bindingResult, ModelMap model,
      HttpServletResponse httpResponse) throws IOException {

    log.info("POST - Create admin employer - Step 1");

    if (bindingResult.hasErrors()) {
      return CREATE_ADMIN_EMPLOYER_VIEW_NAME;
    }

    log.info("Create admin employer - Step 1 - Retrieving user by email = {}", adminEmployerUser.getEmail().toLowerCase());
    Optional<User> maybeAuthenticationUser = authorizationService.getUserByEmail(adminEmployerUser.getEmail().toLowerCase());

    if (maybeAuthenticationUser.isPresent()) {

      String uuid = maybeAuthenticationUser.get().getUuid();

      log.info("Create admin employer - Step 1 - Retrieving finance user by uuid = {}", uuid);
      Optional<FinanceUser> maybeFinanceUser = financeService.getFinanceUserByUuid(uuid);

      if (maybeFinanceUser.isPresent()) {

        model.addAttribute("userExists", true);
        return CREATE_ADMIN_EMPLOYER_VIEW_NAME;
      } else {

        adminEmployerUser.setUuid(uuid);
        return ADMIN_EMPLOYER_DETAILS_FORWARD;
      }
    }

    log.info("Create admin employer - Step 1 - registering authentication user");
    Optional<AuthenticationResponse> response = registerAuthenticationUser(adminEmployerUser);

    if (!response.isPresent()) {

      httpResponse.sendError(500);
      return null;
    }

    adminEmployerUser.setUuid(response.get().getUuid());
    return ADMIN_EMPLOYER_DETAILS_FORWARD;
  }

  private Optional<AuthenticationResponse> registerAuthenticationUser(AdminEmployerUser adminEmployerUser) {

    return userRegistrationService.createAuthenticationUser(adminEmployerUser);
  }


  private Optional<CreateUserResponse> registerEmployerUser(AdminEmployerUser adminEmployerUser) {

    String eaCode = adminEmployerUser.getEaCode();
    Optional<OrganisationView> org = organisationService.getOrganisationByEaCode(eaCode);
    boolean organisationDoesNotExist = !org.isPresent();

    if (organisationDoesNotExist) {

      Optional<OrganisationView> newOrg = organisationService
          .registerOrganisation(eaCode, adminEmployerUser.getAccountName(), adminEmployerUser.getEmployerType());

      if(! newOrg.isPresent()){
        return Optional.empty();
      }
      List<OrganisationView> organisations = Collections.singletonList(newOrg.get());
      adminEmployerUser.setOrganisations(organisations);

    }else{
      org.map(Collections::singletonList).ifPresent(adminEmployerUser::setOrganisations);
    }

    return userRegistrationService.createEmployerUser(adminEmployerUser);
  }


  @PostMapping(value = ADMIN_EMPLOYER_DETAILS_ENDPOINT)
  public ModelAndView showEmployerCreatedPage( @ModelAttribute @Valid AdminEmployerUser adminEmployerUser) {

    log.info("GET - Accessing to Create Admin Employer screen - Step 2");

    return new ModelAndView(ADMIN_EMPLOYER_DETAILS_VIEW_NAME);
  }

  @PostMapping(value = CREATE_FINANCE_EMPLOYER_USER_ENDPOINT)
  public String createFinanceEmployer(
      @ModelAttribute @Validated (value = AdminEmployerValidationGroup.class) AdminEmployerUser adminEmployerUser,
      final BindingResult bindingResult, ModelMap model,
      HttpServletResponse httpResponse) throws IOException {

    log.info("POST - Create admin employer - Step 2");

    if (bindingResult.hasErrors()) {
      return ADMIN_EMPLOYER_DETAILS_VIEW_NAME;
    }

    log.info("Create admin employer - Step 2 - registering employer user");
    Optional<CreateUserResponse> response = registerEmployerUser(adminEmployerUser);

    if (!response.isPresent()) {

      httpResponse.sendError(500);
      return null;
    }

    model.addAttribute(NEW_USER_MAIL, adminEmployerUser.getEmail().toLowerCase());
    model.addAttribute(ENDPOINT, CREATE_ADMIN_EMPLOYER_ENDPOINT);

    return EMPLOYER_CREATED_FORWARD;
  }


}
