package com.nhsbsa.controllers.finance;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.MultiEa;
import com.nhsbsa.service.RequestForTransferService;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@PreAuthorize("hasRole('ADMIN') or hasRole('STANDARD')")
public class MultiEaController {

  private final RequestForTransferService requestForTransferService;

  private static final String CHOOSE_EA_ACCOUNT = "choose-ea-account";

  private static final String DAY_MONTH_YEAR_FORMAT = "dd MMMM yyyy";
  private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_FORMAT);

  @Autowired
  public MultiEaController(final RequestForTransferService requestForTransferService) {
    this.requestForTransferService = requestForTransferService;
  }

  @GetMapping(value = "/multi-ea")
  public ModelAndView chooseEaAccount() {
    log.info("GET - Accessing to Choose Ea Account screen");

    Optional<FinanceUser> currentUser = getCurrentUser();

    //Transform currentUser ea to MultiEa Object

    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    List<MultiEa> eas = currentUser.map(user -> user.getOrganisations().get(0).getEas().stream()
        .map(ea ->
          MultiEa.builder()
              .lastSubmitted(
                  requestForTransferService.getSubmitDateByEaCode(ea.getEaCode())
                  .map(date -> date.format(dateFormatter)).orElse("-")
              )
              .name(ea.getEaCode() + " - " + ea.getName())
              .eaCode(ea.getEaCode())
              .build()
        )
        .collect(Collectors.toList()))
        .orElse(Collections.emptyList());

    ModelAndView modelAndView = new ModelAndView(CHOOSE_EA_ACCOUNT);
    modelAndView.addObject("eas", eas);
    return modelAndView;
  }


  Optional<FinanceUser> getCurrentUser() {

    final Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
    if (details instanceof FinanceUser) {
      return Optional.of((FinanceUser) details);
    }

    return Optional.empty();

  }
}