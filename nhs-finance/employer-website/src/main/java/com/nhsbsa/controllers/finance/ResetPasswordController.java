package com.nhsbsa.controllers.finance;

/**
 * Created by nataliehulse on 12/12/2017
 */

import com.nhsbsa.model.validation.CheckPreviousPasswordValidator;
import com.nhsbsa.model.validation.ComparePasswordValidator;
import com.nhsbsa.security.ResetEmailTokenResponse;
import com.nhsbsa.security.SetPassword;
import com.nhsbsa.security.SetPasswordResponse;
import com.nhsbsa.service.authentication.AuthorizationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@Controller
@Slf4j
public class ResetPasswordController {

    private final AuthorizationService authorizationService;


    private ComparePasswordValidator comparePasswordValidator = new ComparePasswordValidator();

    private CheckPreviousPasswordValidator checkPreviousPasswordValidator = new CheckPreviousPasswordValidator();


    private static final String RESET_PASS_VIEW_NAME = "resetpassword";

    private static final String PASS_UPDATED_REDIRECT = "redirect:/passwordUpdated";


    @Autowired
    public ResetPasswordController(final AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    @GetMapping(value = "/reset-password/{emailToken}")
    public ModelAndView showResetPasswordPage(@PathVariable("emailToken") final String emailToken, HttpSession session, HttpServletResponse response) throws IOException {

        log.info("GET - Accessing to Reset Password screen");

        ResetEmailTokenResponse resetEmailTokenResponse;

        Optional<ResetEmailTokenResponse> optionalResetEmailToken = authorizationService.getOptionalUuidByEmailToken(emailToken);

        if (optionalResetEmailToken.isPresent()) {
            resetEmailTokenResponse = optionalResetEmailToken.get();

            SetPassword setPassword = new SetPassword();
            setPassword.setUuid(resetEmailTokenResponse.getUserUuid());

            ModelAndView modelAndView = new ModelAndView(RESET_PASS_VIEW_NAME);
            modelAndView.addObject("setPassword", setPassword);

            session.setAttribute("uuid", resetEmailTokenResponse.getUserUuid());
            session.setAttribute("token", emailToken);

            return modelAndView;
        }
        else {
            log.error("Reset Password - user uuid not found");
            response.sendError(404);
            return null;
        }
    }

    @PostMapping("/reset-password")
    public String postResetPasswordPage(@ModelAttribute @Valid SetPassword setPassword, final BindingResult bindingResult, HttpSession session) {

        String uuid = session.getAttribute("uuid").toString();

        log.info("GET - Resetting password to user with uuid = {}", uuid);

        setPassword.setUuid(uuid);

        // perform a check against currently saved password
        setPassword.setPasswordSameAsPrevious(authorizationService.validatePassword(setPassword));
        checkPreviousPasswordValidator.validate(setPassword, bindingResult);

        // validation to check passwords match
        comparePasswordValidator.validate(setPassword, bindingResult);


        if (bindingResult.hasErrors()) {
            return RESET_PASS_VIEW_NAME;
        }

        // save new password to db and update first_login flag to false
        setPassword.setFirstLogin(false);
       SetPasswordResponse savedPassword =  authorizationService.savePassword(setPassword);

       if (savedPassword != null) {
        authorizationService.deleteEmailToken(session.getAttribute("token").toString());

       }


        return PASS_UPDATED_REDIRECT;
    }

}
