package com.nhsbsa.service;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.service.authentication.impl.OrganisationDto;
import com.nhsbsa.service.authentication.impl.UserDto;
import com.nhsbsa.view.OrganisationView;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service(value = "userService")
public class UserService {

  @SuppressWarnings("squid:S1075") // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS3437
  private static final String POST_REQUEST_FOR_CREATE_USER_WITHIN_AN_ORGANISATION_PATH = "/organisation/{organisationId}/standard-user";

  @SuppressWarnings("squid:S1075")
  private static final String GET_FINANCE_USER_PATH = "/user/{uuid}";

  private static final String GET_FINANCE_USER_BY_ORG_PATH = "/org-users/{orgUuid}";

  private RestTemplate ficRestTemplate;

  private String authorizationBackendUrl;
  
  private OrganisationService organisationService;
  
  private final UriComponentsBuilder baseUriComponentsBuilder;
  
  @Autowired
  public UserService(final RestTemplate ficRestTemplate,
      @Value("${api.employer-details-service.url}") final String employerDetailsUrl,
      @Value("${authorization.user.url}") final String authorizationBackendUrl,
      final OrganisationService organisationService) {

    this.ficRestTemplate = ficRestTemplate;
    this.authorizationBackendUrl = authorizationBackendUrl;
    this.organisationService = organisationService;
    
    baseUriComponentsBuilder =
        UriComponentsBuilder.newInstance().fromHttpUrl(employerDetailsUrl);
  }


  public Optional<UserDto> getFinanceUserByUuid(String uuid) {

    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI financeUserUri =
        uriComponentsBuilder.path(GET_FINANCE_USER_PATH).buildAndExpand(uuid).toUri();

    return Optional.ofNullable(ficRestTemplate.getForObject(financeUserUri, UserDto.class));
  }


  public Optional<List<UserDto>> getFinanceUsersByOrgUuid(String orgUuid) {

    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI financeUserUri =
        uriComponentsBuilder.path(GET_FINANCE_USER_BY_ORG_PATH).buildAndExpand(orgUuid).toUri();

    return Optional.ofNullable(ficRestTemplate.getForObject(financeUserUri, UserDto[].class))
        .map(Arrays::asList);
  }

  public CreateUserResponse createUserWithinAnOrganisation(CreateUserRequest createUserRequest) {

    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    final URI createUserUri =
        uriComponentsBuilder.path(POST_REQUEST_FOR_CREATE_USER_WITHIN_AN_ORGANISATION_PATH).buildAndExpand(createUserRequest.getOrganisations().get(0).getIdentifier()).toUri();

    return ficRestTemplate
        .postForObject(createUserUri, createUserRequest, CreateUserResponse.class);
  }

  public List<User> getAllEmployerAdminsUsers() {

    final URI uri = UriComponentsBuilder
            .fromHttpUrl(authorizationBackendUrl)
            .query("role={keyword}")
            .buildAndExpand(UserRoles.ROLE_ADMIN.name())
            .toUri();

    return Arrays.asList(ficRestTemplate.getForObject(uri, User[].class));
}
  
  public FinanceUser getFinanceUser(User user) {
    final UriComponentsBuilder userUriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI userUri = userUriComponentsBuilder
            .path(GET_FINANCE_USER_PATH)
            .buildAndExpand(user.getUuid()).toUri();

    final UserDto userView = ficRestTemplate.getForObject(userUri, UserDto.class);

    //TODO Change getOrganisation().get(0) once we know how to handle multiple orgs
    final OrganisationDto organisationDto  = organisationService.getOrganisationByID(userView.getOrganisations().get(0).getIdentifier().toString());
    OrganisationView organisation = OrganisationView.builder().identifier(organisationDto.getIdentifier()).name(organisationDto.getName()).eas(organisationDto.getEas()).build();
    List<OrganisationView> organisationList = Collections.singletonList(organisation);

    return FinanceUser.builder()
            .username(user.getName())
            .firstName(userView.getFirstName())
            .lastName(userView.getLastName())
            .contactEmail(userView.getEmailAddress())
            .contactTelephone(userView.getContactNumber())
            .uuid(userView.getIdentifier().toString())
            .role(user.getRole())
            .organisations(organisationList)
            .build();

}
}