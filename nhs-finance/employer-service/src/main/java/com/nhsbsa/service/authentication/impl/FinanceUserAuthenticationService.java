package com.nhsbsa.service.authentication.impl;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.service.authentication.AuthenticationService;

import java.net.URI;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service(value = "financeUserAuthenticationService")
public class FinanceUserAuthenticationService implements AuthenticationService<FinanceUser> {

  @SuppressWarnings("squid:S1075") // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS3437
  private static final String GET_USER_PATH = "/user/{user-id}";

  private RestTemplate ficRestTemplate;

  private final UriComponentsBuilder baseUriComponentsBuilder;
    
  public FinanceUserAuthenticationService(
      final RestTemplate ficRestTemplate,
      @Value("${api.employer-details-service.url}") final String employerDetailsUrl) {

    this.ficRestTemplate = ficRestTemplate;
    baseUriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(employerDetailsUrl);
  }

  @Override
  public FinanceUser authenticateUser(final LoginRequest loginRequest) {

    final UriComponentsBuilder userUriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();
      
    final URI userUri = userUriComponentsBuilder.path(GET_USER_PATH)
        .buildAndExpand(loginRequest.getUuid(), loginRequest.isFirstLogin()).toUri();

    final UserDto userView = ficRestTemplate.getForObject(userUri, UserDto.class);

    return FinanceUser
            .builder()
            .username(loginRequest.getUsername())
            .firstName(userView.getFirstName())
            .lastName(userView.getLastName())
            .contactEmail(userView.getEmailAddress())
            .contactTelephone(userView.getContactNumber())
            .uuid(userView.getIdentifier().toString())
            .role(UserRoles.ROLE_STANDARD.name())
            .organisations(userView.getOrganisations())
            .firstLogin(loginRequest.isFirstLogin())
            .build();
    }

}
