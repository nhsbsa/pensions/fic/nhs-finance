package com.nhsbsa.service;

import com.nhsbsa.view.RequestForTransferView;
import java.net.URI;
import java.time.LocalDate;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Slf4j
@Service(value = "financeService")
public class FinanceService {

  @SuppressWarnings("squid:S1075")
  // https://next.sonarqube.com/sonarqube/coding_rules#rule_key=squid%3AS3437
  private static final String GET_REQUEST_FOR_TRANSFER_PATH = "/requestfortransfer/{rftUuid}";
  @SuppressWarnings("squid:S1075")
  private static final String POST_REQUEST_FOR_TRANSFER_PATH = "/requestfortransfer";

  private static final String GET_FILE_LIST = "/file";
  private static final String GET_FILE = "/file/{filename}";
  @SuppressWarnings("squid:S1075")
  private static final String GET_SUBMIT_DATE = "/submitdate/{eaCode}";

  private RestTemplate ficRestTemplate;

  private final UriComponentsBuilder baseUriComponentsBuilder;
 
  public FinanceService(final RestTemplate ficRestTemplate,
    @Value("${api.employer-contributions.url}") final String employerContributionsUrl) {

    this.ficRestTemplate = ficRestTemplate;
    baseUriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(employerContributionsUrl);
  }

  public RequestForTransferView getRequestForTransferByRftUuid(String rftUuid) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI rftUri =
        uriComponentsBuilder.path(GET_REQUEST_FOR_TRANSFER_PATH).buildAndExpand(rftUuid).toUri();

    return ficRestTemplate.getForObject(rftUri, RequestForTransferView.class);
  }

  public RequestForTransferView saveRequestForTransfer(RequestForTransferView requestForTransfer) {

    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI rftUri = uriComponentsBuilder.path(POST_REQUEST_FOR_TRANSFER_PATH).build().toUri();

    return ficRestTemplate.postForObject(rftUri, requestForTransfer, RequestForTransferView.class);
  }

  public String[] getFileList() {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI fileListUri = uriComponentsBuilder.path(GET_FILE_LIST).build().toUri();

    return ficRestTemplate.getForObject(fileListUri, String[].class);
  }

  public byte[] getFile(String fileName) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI fileListUri = uriComponentsBuilder.path(GET_FILE).buildAndExpand(fileName).toUri();

    return ficRestTemplate.getForObject(fileListUri, byte[].class);

  }

  public Optional<LocalDate> getSubmitDateByEaCode(String eaCode) {
    final UriComponentsBuilder uriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();

    final URI uri =
        uriComponentsBuilder.path(GET_SUBMIT_DATE).buildAndExpand(eaCode).toUri();

    return Optional.ofNullable(ficRestTemplate.getForObject(uri, LocalDate.class));
  }

}
