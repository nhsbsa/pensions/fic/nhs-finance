package com.nhsbsa.service.email;

import lombok.Data;

@Data
public class EmailConfig {

  private String fromEmail;
  private String templateName;

  public EmailConfig(String fromEmail, String templateName) {

    this.fromEmail = fromEmail;
    this.templateName = templateName;
  }
}
