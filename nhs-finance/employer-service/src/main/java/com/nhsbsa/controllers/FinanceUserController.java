package com.nhsbsa.controllers;

import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailRequest;
import com.nhsbsa.security.EmailResponse;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.security.ValidateEmailResponse;
import com.nhsbsa.service.UserService;
import com.nhsbsa.service.authentication.AuthenticationService;
import com.nhsbsa.service.authentication.impl.UserDto;
import com.nhsbsa.service.email.UserAdminEmailService;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequestMapping("/finance")
@RestController
public class FinanceUserController {

  private final AuthenticationService<FinanceUser> financeUserAuthenticationService;
  private final UserAdminEmailService userAdminEmailService;

  private final UserService userService;

  @Autowired
  public FinanceUserController(
      final AuthenticationService<FinanceUser> financeUserAuthenticationService, final UserService userService, final UserAdminEmailService userAdminEmailService) {
    this.financeUserAuthenticationService = financeUserAuthenticationService;
    this.userAdminEmailService = userAdminEmailService;
    this.userService = userService;
  }

  @RequestMapping(value = "/authentication/login", method = RequestMethod.POST)
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<FinanceUser> login(@RequestBody @Valid final LoginRequest loginRequest) {
    final FinanceUser authenticateUser = financeUserAuthenticationService.authenticateUser(loginRequest);
    log.info("POST - Logging in FinanceUser: {}", authenticateUser);
    return ResponseEntity.ok(authenticateUser);
  }

  @RequestMapping(value = "/user", method = RequestMethod.POST)
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<CreateUserResponse> createUserWithinAnOrganisation(
      @RequestBody @Valid final CreateUserRequest createUserRequest) {
    log.info("POST - Creating user {} within organisation {}",
        createUserRequest.getUsername(), createUserRequest.getOrganisations());
    final CreateUserResponse createUserResponse = userService.createUserWithinAnOrganisation(createUserRequest);
    return ResponseEntity.ok(createUserResponse);
  }


  @RequestMapping(value = "/user/{uuid}")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<FinanceUser> getFinanceUserByUuid(
      @PathVariable("uuid") final String uuid) {

    log.info("GET - searching for user with uuid = {}", uuid);
    Optional<UserDto> user  = userService.getFinanceUserByUuid(uuid);
   return user.map(u -> ResponseEntity.ok(convertUserDtoToFinanceUser(u)))
        .orElse( ResponseEntity.notFound().build());
  }

  @RequestMapping(value = "/org-users/{orgUuid}")
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<List<FinanceUser>> getFinanceUsersByOrgUuid(
      @PathVariable("orgUuid") final String orgUuid) {

    log.info("GET - searching for users with orgUuid = {}", orgUuid);
    Optional<List<UserDto>> user  = userService.getFinanceUsersByOrgUuid(orgUuid);

    return user.map(u -> ResponseEntity.ok(u.stream().map(this::convertUserDtoToFinanceUser).collect(Collectors.toList())))
        .orElse( ResponseEntity.notFound().build());
  }

  @RequestMapping(value = "/email", method = RequestMethod.POST)
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<FinanceUser> sendResetPasswordEmail(@RequestBody @Valid final ValidateEmailResponse validateEmailResponse) {
    log.info("POST - Send reset password email to user with uuid = {}", validateEmailResponse.getUuid());

    Optional<UserDto> user = userService.getFinanceUserByUuid(validateEmailResponse.getUuid());

    if (user.isPresent()) {
      userAdminEmailService
          .sendPasswordResetEmail(user.get().getEmailAddress(), validateEmailResponse.getToken());
      return ResponseEntity.ok(convertUserDtoToFinanceUser(user.get()));
    }
    return ResponseEntity.notFound().build();
  }

  @RequestMapping(value = "/standard-user-email", method = RequestMethod.POST)
  @ApiOperation(value = "default", notes = "default")
  public ResponseEntity<EmailResponse> sendStandardUserCreatedEmail(@RequestBody final EmailRequest emailRequest) {
    log.info("POST - Send Standard User created email to user with email = {}", emailRequest.getEmail());

    userAdminEmailService.sendStandardUserCreatedEmail(emailRequest);
      return ResponseEntity.ok(EmailResponse.builder().name(emailRequest.getFirstName()+"'s").build());

  }
  
  private FinanceUser convertUserDtoToFinanceUser(UserDto user) {

    return FinanceUser
        .builder()
        .username(user.getEmailAddress())
        .firstName(user.getFirstName())
        .lastName(user.getLastName())
        .contactEmail(user.getEmailAddress())
        .contactTelephone(user.getContactNumber())
        .uuid(user.getIdentifier().toString())
        .role(user.getRole())
        .organisations(user.getOrganisations())
        .build();
  }
}