package com.nhsbsa.config;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import com.nhsbsa.exception.TooManyExcludedDatesException;

public class ConfigurationGuardTest {
    
  @Test(expected = Test.None.class)
  public void should_continue_when_excluded_date_size_less_than_max_dates() {
    testConfigurationGuard(Arrays.asList("01/01","07/05","28/05","27/07","25/12","26/12"), 30);
  }
  
  @Test(expected = Test.None.class)
  public void should_continue_when_excluded_date_not_set() {
    testConfigurationGuard(null, 30);
  }
   
  @Test(expected = TooManyExcludedDatesException.class)
  public void should_exception_when_excluded_date_size_greater_than_maxDates() {
    testConfigurationGuard(Arrays.asList("01/01","07/05","28/05","27/07","25/12","26/12"), 5);
  }
  
  private void testConfigurationGuard(final List<String> excludedDates, final int maxDates) {
    ConfigurationGuard configurationGuard = new ConfigurationGuard(excludedDates, maxDates);
  
    configurationGuard.afterPropertiesSet();
  }

}
