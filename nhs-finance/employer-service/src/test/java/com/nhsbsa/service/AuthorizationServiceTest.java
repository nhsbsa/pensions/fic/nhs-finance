package com.nhsbsa.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.nhsbsa.rest.RestAuthentication;
import com.nhsbsa.security.AppToken;
import com.nhsbsa.security.AuthenticationResponse;
import com.nhsbsa.security.AuthenticationResponse.AuthenticationStatus;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.service.authentication.AuthorizationService;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationServiceTest {

  private static final String AUTH_URL = "/auth-test";

  @Mock
  private RestAuthentication restAuthentication;

  @Mock
  private RestTemplate restTemplate;

  private AuthorizationService service;

  @Before
  public void before() throws Exception{
    service = new AuthorizationService(restAuthentication, restTemplate);
    FieldUtils.writeDeclaredField(service, "authorizationBackendUri", AUTH_URL, true);
  }

  @Test
  public void testGetUuidReturnsResponse() {
    AppToken appToken = AppToken.builder().token("app-token").build();
    LoginRequest loginRequest = LoginRequest.builder()
        .appToken(appToken)
        .build();

    HttpEntity<LoginRequest> entity = new HttpEntity<LoginRequest>(loginRequest);
    given(restAuthentication.createRequestWithAppToken(appToken, loginRequest)).willReturn(entity);

    AuthenticationResponse authenticationResponse = AuthenticationResponse.builder()
        .uuid("user-uuid")
        .authenticationStatus(AuthenticationStatus.AUTH_SUCCESS)
        .build();
    given(restTemplate.postForObject(AUTH_URL, entity, AuthenticationResponse.class)).willReturn(authenticationResponse);

    String response = service.getUuid(loginRequest);

    assertEquals(response, "user-uuid");

    verify(restTemplate, times(1)).postForObject(AUTH_URL, entity, AuthenticationResponse.class);

  }

}