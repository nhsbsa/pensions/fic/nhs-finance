package com.nhsbsa.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;

import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(MockitoJUnitRunner.class)
public class FinanceServiceTest {

  @Mock
  private RestTemplate restTemplate;

  private FinanceService financeService;

  @Before
  public void before() {
    financeService = new FinanceService(restTemplate, "http://employer-contributions-url:1234/contributions-context");
  }

  @Test
  public void when_get_requestForTransfer_invoked_with_valid_id_requestForTransfer_is_returned() {

    RequestForTransferView expectedRft = RequestForTransferView.builder().rftUuid("TEST_UUID").build();

    final URI rftUri = URI.create(
        "http://employer-contributions-url:1234/contributions-context/requestfortransfer/" + expectedRft.getRftUuid());

    given(restTemplate.getForObject(rftUri, RequestForTransferView.class)).willReturn(expectedRft);

    RequestForTransferView actualRft =
        financeService.getRequestForTransferByRftUuid(expectedRft.getRftUuid());

    assertEquals(actualRft.getRftUuid(), expectedRft.getRftUuid());
  }

  @Test
  public void when_post_requestForTransfer_invoked_with_valid_requestForTransfer_is_returned() {

    final RequestForTransferView requestForTransfer = RequestForTransferView.builder().rftUuid("TEST_UUID")
        .employeeAddedYears(new BigDecimal("50")).additionalPension(new BigDecimal("60"))
        .errbo(new BigDecimal("70")).totalDebitAmount(new BigDecimal("210")).build();

    final URI rftUri = URI.create("http://employer-contributions-url:1234/contributions-context/requestfortransfer");

    given(
        restTemplate.postForObject(rftUri, requestForTransfer, RequestForTransferView.class))
            .willReturn(requestForTransfer);

    RequestForTransferView actualRft = financeService.saveRequestForTransfer(requestForTransfer);

    assertNotNull(actualRft);
    assertEquals(actualRft.getRftUuid(), requestForTransfer.getRftUuid());
  }

  @Test
  public void when_get_submitDate_invoked_with_valid_eaCode_date_is_returned() {

    String eaCode = "EA1234";
    LocalDate expectedDate = LocalDate.of(2018,8,15);

    final URI eaUri = URI.create(
        "http://employer-contributions-url:1234/contributions-context/submitdate/" + eaCode);

    given(restTemplate.getForObject(eaUri, LocalDate.class)).willReturn(expectedDate);

    Optional<LocalDate> actualDate =
       financeService.getSubmitDateByEaCode(eaCode);

    assertEquals(actualDate.get(), expectedDate);
  }

  @Test
  public void when_get_submitDate_invoked_with_invalid_eaCode_null_is_returned() {

    String eaCode = "EA1234";

    final URI eaUri = URI.create(
        "http://employer-contributions-url:1234/contributions-context/submitdate/" + eaCode);

    given(restTemplate.getForObject(eaUri, LocalDate.class)).willReturn(null);

    Optional<LocalDate> actualDate =
        financeService.getSubmitDateByEaCode(eaCode);

    assertFalse(actualDate.isPresent());
  }

  @Test
  public void when_get_fileList_then_string_is_returned() {

    String[] fileList = new String[2];
    fileList[0] = "file1";
    fileList[1] = "file2";

    final URI uri = URI.create(
        "http://employer-contributions-url:1234/contributions-context/file");

    given(restTemplate.getForObject(uri, String[].class)).willReturn(fileList);

    String[] result = financeService.getFileList();

    assertEquals(result[0], "file1");
    assertEquals(result[1], "file2");
  }

  @Test
  public void when_get_file_then_byte_is_returned() {

    String fileName = "test-file-name";
    byte[] fileList = {0,1,2};

    final URI uri = URI.create(
        "http://employer-contributions-url:1234/contributions-context/file/" + fileName);

    given(restTemplate.getForObject(uri, byte[].class)).willReturn(fileList);

    byte[] result = financeService.getFile(fileName);

    assertEquals(result[0], 0);
    assertEquals(result[1], 1);
    assertEquals(result[2], 2);
  }

}
