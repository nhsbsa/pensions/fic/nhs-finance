package com.nhsbsa.service;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.model.User;
import com.nhsbsa.model.UserRoles;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.service.authentication.impl.OrganisationDto;
import com.nhsbsa.service.authentication.impl.UserDto;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

  @Mock
  private RestTemplate restTemplate;

  @Mock
  private OrganisationService organisationService;
  
  private UserService service;

  private UriComponentsBuilder baseUriComponentsBuilder;
  
  @Before
  public void before() {

    service = new UserService(
        restTemplate, 
        "http://employer-details-service:1234/employer-context", 
        "http://authorizationURI",
        organisationService);
    
    baseUriComponentsBuilder = 
        UriComponentsBuilder.newInstance().fromHttpUrl("http://employer-details-service:1234/employer-context");
  }

  @Test
  public void check_that_createUserWithinAnOrganisation_method_does_post_request() {

    final String EXPECTED_ORG_UUID = "0910dda1-0704-4061-ad30-f5f8a7bb6140";

    OrganisationView organisation = OrganisationView.builder().identifier(UUID.fromString("0910dda1-0704-4061-ad30-f5f8a7bb6140")).build();
    List<OrganisationView> organisations = Collections.singletonList(organisation);
    CreateUserRequest request = CreateUserRequest.builder()
        .username("valid@email.com")
        .organisations(organisations)
        .firstName("-")
        .surname("-")
        .contactNumber("-")
        .build();

    CreateUserResponse mockedResponse = CreateUserResponse.builder()
        .uuid("2132-654651-21321")
        .email("valid@email.com").build();

    URI URI_request = URI
        .create("http://employer-details-service:1234/employer-context/organisation/"
            + EXPECTED_ORG_UUID + "/standard-user");

    given(restTemplate.postForObject(URI_request, request, CreateUserResponse.class)).willReturn(mockedResponse);

    service.createUserWithinAnOrganisation(request);

    verify(restTemplate, times(1)).postForObject(URI_request, request, CreateUserResponse.class);

  }

  @Test
  public void when_get_finance_user_invoked_with_valid_uuid_finance_user_is_returned() {

    final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    UserDto expectedUser = UserDto.builder().identifier(UUID.fromString(EXPECTED_UUID)).build();
    final URI financeUserUri = URI.create(
        "http://employer-details-service:1234/employer-context/user/" + EXPECTED_UUID);

    given(restTemplate.getForObject(financeUserUri, UserDto.class)).willReturn(expectedUser);

    Optional<UserDto> actualFinanceUser =
        service.getFinanceUserByUuid(EXPECTED_UUID);

    assertEquals(actualFinanceUser.get().getIdentifier(), expectedUser.getIdentifier());
  }

  @Test
  public void when_get_finance_user_by_orgid_invoked_with_valid_orgUuid_finance_user_is_returned() {

    final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    UserDto[] expectedUser = new UserDto[1];
    expectedUser[0] = UserDto.builder().identifier(UUID.fromString(EXPECTED_UUID)).build();

    final URI financeUserUri = URI.create(
        "http://employer-details-service:1234/employer-context/org-users/" + EXPECTED_UUID);

    given(restTemplate.getForObject(financeUserUri, UserDto[].class)).willReturn(expectedUser);

    Optional<List<UserDto>> actualFinanceUser =
        service.getFinanceUsersByOrgUuid(EXPECTED_UUID);

    assertEquals(actualFinanceUser.get().get(0).getIdentifier(), expectedUser[0].getIdentifier());
  }

  @Test
  public void when_get_finance_user_by_orgid_returns_empty() {

    final String EXPECTED_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    UserDto[] expectedUser = new UserDto[1];
    expectedUser[0] = UserDto.builder().identifier(UUID.fromString(EXPECTED_UUID)).build();

    final URI financeUserUri = URI.create(
        "http://employer-details-service:1234/employer-context/org-users/" + EXPECTED_UUID);

    given(restTemplate.getForObject(financeUserUri, UserDto[].class)).willReturn(null);

    Optional<List<UserDto>> actualFinanceUser =
        service.getFinanceUsersByOrgUuid(EXPECTED_UUID);

    assertFalse(actualFinanceUser.isPresent());
  }
  
  @Test
  public void whenAuthorizationServiceCalledWithRoleThenListOfAdminsReturned() {

      User admin1 = User.builder()
              .id(1L)
              .name("User1")
              .hash("hash1")
              .firstLogin(false)
              .role(UserRoles.ROLE_ADMIN.name())
              .build();
      
      User admin2 = User.builder()
              .id(2L)
              .name("User2")
              .hash("hash2")
              .firstLogin(false)
              .role(UserRoles.ROLE_ADMIN.name())
              .build();
      
      User admin3 = User.builder()
              .id(3L)
              .name("User3")
              .hash("hash3")
              .firstLogin(false)
              .role(UserRoles.ROLE_ADMIN.name())
              .build();

      User[] users = new User[] { admin1, admin2, admin3 };

      final URI uri = UriComponentsBuilder
              .fromHttpUrl("http://authorizationURI")
              .query("role={keyword}")
              .buildAndExpand(UserRoles.ROLE_ADMIN.name())
              .toUri();

      given(restTemplate.getForObject(uri, User[].class)).willReturn(users);

      List<User> entity = service.getAllEmployerAdminsUsers();

      verify(restTemplate, times(1)).getForObject(uri, User[].class);
      assertThat(entity, hasSize(3));
      assertThat(entity.get(0), is(sameInstance(admin1)));
  }
  
  @Test
  public void whenGivenAValidUserThenTheCorrispondingFinanceUserIsReturned() {
      
      User admin1 = User.builder()
              .id(1L)
              .name("User1")
              .hash("hash1")
              .firstLogin(false)
              .role(UserRoles.ROLE_ADMIN.name())
              .build();

      EmployingAuthorityView employingAuthority = EmployingAuthorityView.builder().eaCode("EA1234").name("TEST ORG").employerType("").build();
      List<EmployingAuthorityView> employingAuthorityList = Collections.singletonList(employingAuthority);
      OrganisationView organisation = OrganisationView.builder().identifier(UUID.fromString("0910dda1-0704-4061-ad30-f5f8a7bb6140")).name("").eas(employingAuthorityList).build();
      List<OrganisationView> organisationList = Collections.singletonList(organisation);
      
      UserDto userView1 = UserDto.builder()
                          .identifier(UUID.fromString("a1ed70a7-966f-48b0-bcc8-351a5ff9c4ca"))
                          .firstName("Firstname1")
                          .lastName("LastName1")
                          .contactNumber("0123456789")
                          .emailAddress("user1@email.com")
                          .organisations(organisationList)
                          .role(UserRoles.ROLE_ADMIN.name())
                          .build();
      
      final URI userUri = 
          baseUriComponentsBuilder.cloneBuilder()
              .path("/user/{user-id}")
              .buildAndExpand(admin1.getUuid()).toUri();
      
      given(restTemplate.getForObject(userUri, UserDto.class)).willReturn(userView1);                 
      
      OrganisationDto orgView1 = OrganisationDto.builder()
                                  .eas(employingAuthorityList)
                                  .accountName("TEST ORG")
                                  .identifier(userView1.getOrganisations().get(0).getIdentifier())
                                  .build();
                               
      given(organisationService.getOrganisationByID(userView1.getOrganisations().get(0).getIdentifier().toString())).willReturn(orgView1);  
      
      FinanceUser entity = service.getFinanceUser(admin1);
      
      assertNotNull(entity);
      assertEquals(admin1.getName(),entity.getUsername());
      assertEquals(userView1.getFirstName(),entity.getFirstName());
      assertEquals(userView1.getLastName(),entity.getLastName());
      assertEquals(userView1.getEmailAddress(),entity.getContactEmail());
      assertEquals(userView1.getContactNumber(),entity.getContactTelephone());
      assertEquals(userView1.getIdentifier().toString(),entity.getUuid());
      assertEquals(UserRoles.ROLE_ADMIN.name(),entity.getRole());
      assertEquals(orgView1.getName(),entity.getOrganisations().get(0).getName());
      assertEquals(orgView1.getEas().get(0).getEaCode(),entity.getOrganisations().get(0).getEas().get(0).getEaCode());

  }


}