package com.nhsbsa.service.email.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import com.nhsbsa.service.email.EmailConfig;
import uk.nhs.nhsbsa.notify.api.v2.model.EmailNotificationRequest;
import uk.nhs.nhsbsa.notify.api.v2.model.NotificationResponse;
import uk.nhs.nhsbsa.notify.api.v2.service.INotificationService;

@RunWith(SpringRunner.class)
public class BSANotifyV2ServiceTest {

  @Mock
  private INotificationService notificationClient;

  private BSANotifyV2Service service;

  private String toEmail = "toAddress1@example.com";
  private String fromEmail = "fromEmail@";
  private String emailSubject = "Unit Test Email";
  private String emailTemplate = "test.template";

  private EmailConfig emailConfig = new EmailConfig(fromEmail, emailTemplate);

  private Map<String, String> personalisation = new HashMap<>();

  private NotificationResponse notificationResponse = new NotificationResponse();
  
  @Before
  public void setUp() {
    service = new BSANotifyV2Service(notificationClient, "TEST-SERVICE");
  }
	
  @Test
  public void shouldSendEmailNotificationRequestWhenRequestIsValid() {
    notificationResponse.setSuccess(true);
    doReturn(notificationResponse).when(notificationClient)
        .send(any(EmailNotificationRequest.class));

    service.sendEmail(Arrays.asList(toEmail), emailSubject, emailConfig, personalisation);

    verify(notificationClient, times(1)).send(any(EmailNotificationRequest.class));
  }

  @Test
  public void shouldPopulatedRequestWhenSendCalled() {

    notificationResponse.setSuccess(true);
    doReturn(notificationResponse).when(notificationClient)
        .send(any(EmailNotificationRequest.class));
    
    ArgumentCaptor<EmailNotificationRequest> argument =
        ArgumentCaptor.forClass(EmailNotificationRequest.class);

    service.sendEmail(Arrays.asList(toEmail), emailSubject, emailConfig, personalisation);

    verify(notificationClient).send(argument.capture());

    assertEquals(toEmail, argument.getValue().getEmailAddress());
    assertEquals(emailTemplate, argument.getValue().getTemplate());
    assertEquals(emailSubject, argument.getValue().getPersonalisation().get("emailSubject"));
  }

  @Test
  public void shouldPopulateRequestWithARandomIdWhenSend() {

    notificationResponse.setSuccess(true);
    doReturn(notificationResponse).when(notificationClient)
        .send(any(EmailNotificationRequest.class));
    
    ArgumentCaptor<EmailNotificationRequest> argument =
        ArgumentCaptor.forClass(EmailNotificationRequest.class);
    
    service.sendEmail(Arrays.asList(toEmail), emailSubject, emailConfig, personalisation);

    verify(notificationClient).send(argument.capture());
    assertNotNull(argument.getValue().getId());
  }
  
}
