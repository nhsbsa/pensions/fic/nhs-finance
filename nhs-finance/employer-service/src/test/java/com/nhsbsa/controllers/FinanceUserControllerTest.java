package com.nhsbsa.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.model.FinanceUser;
import com.nhsbsa.security.CreateUserRequest;
import com.nhsbsa.security.CreateUserResponse;
import com.nhsbsa.security.EmailRequest;
import com.nhsbsa.security.EmailResponse;
import com.nhsbsa.security.LoginRequest;
import com.nhsbsa.security.ValidateEmailResponse;
import com.nhsbsa.service.UserService;
import com.nhsbsa.service.authentication.AuthenticationService;
import com.nhsbsa.service.authentication.impl.UserDto;
import com.nhsbsa.service.email.UserAdminEmailService;
import com.nhsbsa.view.EmployingAuthorityView;
import com.nhsbsa.view.OrganisationView;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by natalie hulse on 06/12/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FinanceUserControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Mock
  private AuthenticationService<FinanceUser> authenticationService;

  @Mock
  private UserAdminEmailService userAdminEmailService;

  @Mock
  private UserService userService;

  @Mock
  private FinanceUser authenticateUser;

  private CreateUserResponse createUserResponse;

  private static final Long USER_ID = 1L;
  private static final String USER_UUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";
  private static final boolean FIRST_LOGIN = false;
  private static final String USER_NAME = "test@test";
  private static final String PASS_WORD = "pass-word";
  private static final String FIRST_NAME = "test";
  private static final String LAST_NAME = "tester";
  private static final String CONTACT_EMAIL = "test@test";
  private static final String CONTACT_TEL = "1234567";
  private static final String ROLE = "STANDARD";
  private static final String EA_CODE = "ea-code";
  private final EmployingAuthorityView EMPLOYING_AUTHORITY = EmployingAuthorityView.builder()
      .eaCode(EA_CODE).name("EA1234")
      .employerType("STAFF")
      .build();
  private final List<EmployingAuthorityView> EMPLOYING_AUTHORITY_LIST = Collections.singletonList(EMPLOYING_AUTHORITY);
  private final OrganisationView ORGANISATION = OrganisationView.builder()
      .identifier(UUID.fromString("0910dda1-0704-4061-ad30-f5f8a7bb6140"))
      .name("Org Name")
      .eas(EMPLOYING_AUTHORITY_LIST)
      .build();
  private final List<OrganisationView> ORGANISATION_LIST = Collections.singletonList(ORGANISATION);

  public static final String VALID_AUTH_CONTENT = "{" +
          "\"uuid\" : \"" + USER_UUID + "\"," +
          "\"username\" : \"" + USER_NAME + "\"," +
          "\"password\" : \"" + PASS_WORD + "\"," +
          "\"firstlogin\" : \"" + FIRST_LOGIN + "\"" +
          "}";

  public static final String VALID_GET_DETAILS_CONTENT = "{" +
          "\"uuid\" : \"" + USER_UUID + "\"," +
          "\"userName\" : \"" + USER_NAME + "\"" +
          "}";

  public static final String INVALID_CONTENT = "{" +
      "\"contactNumber\" : \"" + CONTACT_TEL + "\"" +
      "}";

  public static final String VALID_EMAIL_REQUEST_CONTENT = "{" +
      "\"email\" : \"" + "sam.Jones@email.net" + "\"," +
      "\"firstName\" : \"" + "Sam" + "\"," +
      "\"lastName\" : \"" + "Jones" + "\"," +
      "\"createdBy\" : \"" + "Anne Smith" + "\"" +
      "}";

  @Before
  public void setUp() throws Exception {

    mockMvc = MockMvcBuilders
        .standaloneSetup(new FinanceUserController(authenticationService, userService,
            userAdminEmailService))
        .build();

    authenticateUser = FinanceUser
            .builder()
            .id(USER_ID)
            .uuid(USER_UUID)
            .username(USER_NAME)
            .firstName(FIRST_NAME)
            .lastName(LAST_NAME)
            .contactTelephone(CONTACT_TEL)
            .contactEmail(CONTACT_EMAIL)
            .organisations(ORGANISATION_LIST)
            .role(ROLE)
            .build();

    createUserResponse = CreateUserResponse.builder()
        .uuid(USER_UUID)
        .email(CONTACT_EMAIL)
        .build();
  }

  @Test
  public void whenUserLoggedInReturnOk() throws Exception {

    LoginRequest loginRequest = LoginRequest.builder().uuid(USER_UUID).firstLogin(FIRST_LOGIN).username(USER_NAME).password(PASS_WORD).build();

    doReturn(authenticateUser).when(authenticationService).authenticateUser(loginRequest);

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/authentication/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(VALID_AUTH_CONTENT))
            .andExpect(status().isOk())
            .andExpect(content().string(is(equalTo(successResponseForFinanceUser()))));

    verify(authenticationService, times(1)).authenticateUser(Mockito.any(LoginRequest.class));
  }

  @Test
  public void noUserDataReturnedIfDoesntExist() throws Exception {

    LoginRequest loginRequest = LoginRequest.builder().uuid(USER_UUID).firstLogin(FIRST_LOGIN).username(USER_NAME).password(PASS_WORD).build();

    doReturn(null).when(authenticationService).authenticateUser(loginRequest);

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/authentication/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(VALID_AUTH_CONTENT))
            .andExpect(status().isOk())
            .andExpect(content().string(isEmptyString()));
  }

  @Test
  public void financeDetailsReturnedOk() throws Exception {

    ValidateEmailResponse validateEmailResponse = ValidateEmailResponse.builder().uuid(USER_UUID).userName(USER_NAME).build();
    
    UserDto expectedUser = UserDto.builder()
        .identifier(UUID.fromString(USER_UUID))
        .emailAddress("email@email.com")
        .contactNumber("1111111111")
        .firstName("Sam")
        .lastName("Jones")
        .role("ROLE_STANDARD")
        .build();
    
    FinanceUser expectedFinanceUser =  FinanceUser
        .builder()
        .username(expectedUser.getEmailAddress())
        .firstName(expectedUser.getFirstName())
        .lastName(expectedUser.getLastName())
        .contactEmail(expectedUser.getEmailAddress())
        .contactTelephone(expectedUser.getContactNumber())
        .uuid(expectedUser.getIdentifier().toString())
        .role(expectedUser.getRole())
        .build();
    
    doReturn(Optional.of(expectedUser)).when(userService).getFinanceUserByUuid(validateEmailResponse.getUuid());

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/email")
            .contentType(MediaType.APPLICATION_JSON)
            .content(VALID_GET_DETAILS_CONTENT))
            .andExpect(status().isOk())
            .andExpect(content().string(is(equalTo(objectMapper.writeValueAsString(expectedFinanceUser)))));

    verify(userService, times(1)).getFinanceUserByUuid(validateEmailResponse.getUuid());
    verify(userAdminEmailService, times(1)).sendPasswordResetEmail(expectedUser.getEmailAddress(),validateEmailResponse.getToken());
  }

  @Test
  public void emailNotSentIfNoUser() throws Exception {

    ValidateEmailResponse validateEmailResponse = ValidateEmailResponse.builder().uuid(USER_UUID).userName(USER_NAME).build();

    doReturn(Optional.empty()).when(userService).getFinanceUserByUuid(validateEmailResponse.getUuid());

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/email")
            .contentType(MediaType.APPLICATION_JSON)
            .content(VALID_GET_DETAILS_CONTENT))
            .andExpect(status().isNotFound())
            .andExpect(content().string(isEmptyString()));


    verify(userService, times(1)).getFinanceUserByUuid(validateEmailResponse.getUuid());
    verify(userAdminEmailService, times(0)).sendPasswordResetEmail(Mockito.anyString(),Mockito.anyString());
  }

  private String successResponseForFinanceUser() throws JsonProcessingException {
    return objectMapper.writeValueAsString(FinanceUser
            .builder()
            .id(USER_ID)
            .uuid(USER_UUID)
            .username(USER_NAME)
            .firstName(FIRST_NAME)
            .lastName(LAST_NAME)
            .contactTelephone(CONTACT_TEL)
            .contactEmail(CONTACT_EMAIL)
            .organisations(ORGANISATION_LIST)
            .role(ROLE)
            .build());
  }

  @Test
  public void get_user_by_uuid_test() throws Exception {

    String expectedUUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";
    UserDto expectedUser = UserDto.builder()
                                    .identifier(UUID.fromString(expectedUUID))
                                    .emailAddress("email@email.com")
                                    .contactNumber("1111111111")
                                    .firstName("Sam")
                                    .lastName("Jones")
                                    .role("ROLE_STANDARD")
                                    .build();

    when(userService.getFinanceUserByUuid(expectedUUID)).thenReturn(Optional.of(expectedUser));

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/user/{uuid}", expectedUUID))
        .andExpect(status().isOk())
        .andDo(print());


  }

  @Test
  public void error_404_response_expected_when_trying_to_get_non_existing_user_by_uuid_test() throws Exception {

    String expectedUUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    when(userService.getFinanceUserByUuid(expectedUUID)).thenReturn(Optional.empty());

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/user/{uuid}", expectedUUID))
        .andExpect(status().isNotFound())
        .andDo(print());


  }

  @Test
  public void whenUserCreatedWithinOrganisationReturnOk() throws Exception {

    CreateUserRequest userRequest = CreateUserRequest.builder()
        .username(USER_NAME)
        .firstName(FIRST_NAME)
        .surname(LAST_NAME)
        .contactNumber(CONTACT_TEL)
        .organisations(ORGANISATION_LIST)
        .uuid(USER_UUID)
        .build();

    given(userService.createUserWithinAnOrganisation(userRequest)).willReturn(createUserResponse);

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/user")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validOrgContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(successResponseForUser()))));

    verify(userService, times(1)).createUserWithinAnOrganisation(Mockito.any(CreateUserRequest.class));
  }

  @Test
  public void whenUserCreatedWithinOrganisationWithInnvalidDataReturnBadRequest() throws Exception {

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/user")
        .contentType(MediaType.APPLICATION_JSON)
        .content(INVALID_CONTENT))
        .andExpect(status().isBadRequest())
        .andExpect(content().string(isEmptyString()));

    verify(userService, times(0)).createUserWithinAnOrganisation(Mockito.any(CreateUserRequest.class));
  }

  @Test
  public void get_user_by_org_uuid_test() throws Exception {

    String expectedUUID = "6a7957d8-d0bc-4a9b-a5c4-65836fe809b6";

    List<UserDto> expectedUser = Collections.singletonList(UserDto.builder()
        .identifier(UUID.fromString(USER_UUID))
        .emailAddress("email@email.com")
        .contactNumber("1111111111")
        .firstName("Sam")
        .lastName("Jones")
        .role("ROLE_STANDARD")
        .build());

    List<FinanceUser> expectedFinanceUser =  Collections.singletonList(FinanceUser
        .builder()
        .username(expectedUser.get(0).getEmailAddress())
        .firstName(expectedUser.get(0).getFirstName())
        .lastName(expectedUser.get(0).getLastName())
        .contactEmail(expectedUser.get(0).getEmailAddress())
        .contactTelephone(expectedUser.get(0).getContactNumber())
        .uuid(expectedUser.get(0).getIdentifier().toString())
        .role(expectedUser.get(0).getRole())
        .build());

    doReturn(Optional.of(expectedUser)).when(userService).getFinanceUsersByOrgUuid(expectedUUID);

    mockMvc.perform(get("/finance/org-users/{orgUuid}", expectedUUID))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(objectMapper.writeValueAsString(expectedFinanceUser)))));

  }

  @Test
  public void StandardUserCreatedEmailIsSent() throws Exception {

    EmailRequest request = EmailRequest.builder().firstName("Sam").lastName("Jones").createdBy("Anne Smith").email("Sam.Jones@email.com").build();
    EmailResponse response = EmailResponse.builder().name("Sam's").build();

    doNothing().when(userAdminEmailService).sendStandardUserCreatedEmail(request);

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/standard-user-email")
        .contentType(MediaType.APPLICATION_JSON)
        .content(VALID_EMAIL_REQUEST_CONTENT))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(objectMapper.writeValueAsString(response)))));

    verify(userAdminEmailService, times(1))
        .sendStandardUserCreatedEmail(Mockito.any());
  }


  private String successResponseForUser() throws JsonProcessingException {
    return objectMapper.writeValueAsString(CreateUserResponse
        .builder()
        .uuid(USER_UUID)
        .email(CONTACT_EMAIL)
        .build());
  }

  private String validOrgContent() throws JsonProcessingException {

    EmployingAuthorityView ea = EmployingAuthorityView.builder()
        .eaCode(EA_CODE).name("EA1234")
        .employerType("STAFF")
        .build();
    List<EmployingAuthorityView> eaList = Collections.singletonList(ea);
    OrganisationView org = OrganisationView.builder()
        .identifier(UUID.fromString("0910dda1-0704-4061-ad30-f5f8a7bb6140"))
        .name("Org Name")
        .eas(eaList)
        .build();
    List<OrganisationView> orgList = Collections.singletonList(org);

    return objectMapper.writeValueAsString(CreateUserRequest
        .builder()
        .uuid(USER_UUID)
        .username(USER_NAME)
        .firstName(FIRST_NAME)
        .surname(LAST_NAME)
        .contactNumber(CONTACT_TEL)
        .organisations(orgList)
        .build());
  }

}
