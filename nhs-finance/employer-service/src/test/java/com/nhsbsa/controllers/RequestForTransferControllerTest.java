package com.nhsbsa.controllers;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nhsbsa.service.FinanceService;
import com.nhsbsa.view.RequestForTransferView;
import java.time.LocalDate;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class RequestForTransferControllerTest {

  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Mock
  private FinanceService financeService;

  private static final String RFT_UUID = "rft_uuid";
  private static final String EA_CODE = "EA1234";
  private static final RequestForTransferView RFT_VIEW =
      RequestForTransferView.builder()
          .rftUuid(RFT_UUID)
          .eaCode(EA_CODE)
          .build();

  public static final String VALID_EMAIL_REQUEST_CONTENT = "{" +
      "\"email\" : \"" + "sam.Jones@email.net" + "\"," +
      "\"firstName\" : \"" + "Sam" + "\"," +
      "\"lastName\" : \"" + "Jones" + "\"," +
      "\"createdBy\" : \"" + "Anne Smith" + "\"" +
      "}";

  @Before
  public void setUp() throws Exception {

    mockMvc = MockMvcBuilders
        .standaloneSetup(new RequestForTransferController(financeService))
        .build();

  }

  @Test
  public void getRequestForTransferByRftUuidReturnsRft() throws Exception {

    doReturn(RFT_VIEW).when(financeService).getRequestForTransferByRftUuid(RFT_UUID);

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/requestfortransfer/{rftUuid}", RFT_UUID)
        .contentType(MediaType.APPLICATION_JSON)
        .content(RFT_UUID))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(validRftContent()))));

    verify(financeService, times(1)).getRequestForTransferByRftUuid(Mockito.any(String.class));
  }

  @Test
  public void saveRequestForTransferReturnOk() throws Exception {

    given(financeService.saveRequestForTransfer(RFT_VIEW)).willReturn(RFT_VIEW);

    mockMvc.perform(MockMvcRequestBuilders.post("/finance/requestfortransfer")
        .contentType(MediaType.APPLICATION_JSON)
        .content(validRftContent()))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(validRftContent()))));

    verify(financeService, times(1)).saveRequestForTransfer(Mockito.any(RequestForTransferView.class));
  }

  @Test
  public void getFileListReturnsOk() throws Exception {
    String[] file = new String[1];
    file[0] = "Test file";

    doReturn(file).when(financeService).getFileList();

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/file")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(objectMapper.writeValueAsString(file)))));

    verify(financeService, times(1)).getFileList();
  }

  @Test
  public void getFileReturnsOk() throws Exception {
    String fileName = "test";
    byte[] file = (new String("data")).getBytes();

    doReturn(file).when(financeService).getFile(fileName);

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/file/{fileName}", fileName)
        .contentType(MediaType.APPLICATION_JSON)
        .content(fileName))
        .andExpect(status().isOk())
        .andExpect(content().bytes(file));

    verify(financeService, times(1)).getFile(Mockito.any(String.class));
  }

  @Test
  public void getFormattedSubmitDateByEaCodeReturnsDate() throws Exception {

    Optional<LocalDate> submitDate = Optional.of(LocalDate.of(2018,8,15));
    doReturn(submitDate).when(financeService).getSubmitDateByEaCode(EA_CODE);

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/submitdate/{eaCode}", EA_CODE)
        .contentType(MediaType.APPLICATION_JSON)
        .content(EA_CODE))
        .andExpect(status().isOk())
        .andExpect(content().string(is(equalTo(objectMapper.writeValueAsString(submitDate.get())))));

    verify(financeService, times(1)).getSubmitDateByEaCode(Mockito.any(String.class));
  }

  @Test
  public void getFormattedSubmitDateByEaCodeReturnsEmpty() throws Exception {

    doReturn(Optional.empty()).when(financeService).getSubmitDateByEaCode(EA_CODE);

    mockMvc.perform(MockMvcRequestBuilders.get("/finance/submitdate/{eaCode}", EA_CODE)
        .contentType(MediaType.APPLICATION_JSON)
        .content(EA_CODE))
        .andExpect(status().isNoContent())
        .andExpect(content().string(isEmptyString()));

    verify(financeService, times(1)).getSubmitDateByEaCode(Mockito.any(String.class));
  }

  private String validRftContent() throws JsonProcessingException {

    return objectMapper.writeValueAsString(RequestForTransferView
        .builder()
        .rftUuid(RFT_UUID)
        .eaCode(EA_CODE)
        .build());
  }

}
