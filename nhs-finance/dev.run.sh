#!/usr/bin/env bash

declare -a APPS=(
  "authorization"
  "employer-details-service"
  "employer-contribs-service"
  "employer-service"
  "employer-website"
  "finance-facade"
)

function start {
  for APP in "${APPS[@]}"
  do
    if [ -d "$APP" ]; then
      pushd . >> /dev/null
      echo "Starting $APP"
      cd "$APP"
      APP_DIR="$(pwd)"
      nohup java -jar $(ls *.jar) --logging.config="$APP_DIR/logback.xml" & echo $! > PID
      popd >> /dev/null
    fi
  done
}

function stop {
  for APP in "${APPS[@]}"
  do
    if [ -d "$APP" ]; then
      pushd . >> /dev/null
      echo "Stopping $APP"
      cd "$APP"
      APP_DIR="$(pwd)"
      kill -9 `cat PID`
      rm PID
      rm nohup.out
      popd >> /dev/null
    fi
  done
}

function usage {
  echo "Usage: $0 [-u] [-d]" 1>&2;
  echo """
    -u                  Starts the services
    -d                  Stops the services
  """
}


while getopts "ud" opt; do
  case $opt in
    u)
      start >&2
      ;;
    d)
      stop >&2
    ;;
  esac
done

if [ $OPTIND -eq 1 ];
	then usage;
	exit;
fi