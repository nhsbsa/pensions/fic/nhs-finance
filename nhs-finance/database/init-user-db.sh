#!/bin/bash
set -e


psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL

    CREATE USER root with PASSWORD 'mysecretpassword';
    ALTER USER root with SUPERUSER;

    CREATE USER employer with PASSWORD 'password';
    ALTER USER employer with SUPERUSER;

    CREATE DATABASE authentication;
    GRANT ALL PRIVILEGES ON DATABASE authentication TO root;

    CREATE DATABASE finance;
    GRANT ALL PRIVILEGES ON DATABASE finance TO root;

    CREATE DATABASE employer;
    GRANT ALL PRIVILEGES ON DATABASE employer TO root;

    CREATE DATABASE test_employer;
    GRANT ALL PRIVILEGES ON DATABASE test_employer TO root;

EOSQL
