package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nhsbsa.model.TransferFormDate;
import com.nhsbsa.view.RequestForTransferView;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import javax.validation.ConstraintValidatorContext;
import javax.validation.ConstraintValidatorContext.ConstraintViolationBuilder.NodeBuilderCustomizableContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


/**
 * Created by nataliehulse on 11/11/2016.
 */
public class DateLessThan31DaysFromTodayValidatorTest {

  private static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd/MM/yyyy");
  DateLessThan31DaysFromTodayValidator dateLessThan31DaysFromTodayValidator;
  ConstraintValidatorContext context;

  @Before
  public void setUp() {
    dateLessThan31DaysFromTodayValidator = new DateLessThan31DaysFromTodayValidator() {
      @Override
      public LocalDate getDate() {
        return LocalDate.of(2016, 12, 1);
      }
    };
    context = Mockito.mock(ConstraintValidatorContext.class);

    final ConstraintValidatorContext.ConstraintViolationBuilder constraintViolationBuilder =
        Mockito.mock(ConstraintValidatorContext.ConstraintViolationBuilder.class);

    when(context.buildConstraintViolationWithTemplate(Mockito.anyString()))
        .thenReturn(constraintViolationBuilder);

    NodeBuilderCustomizableContext customCtx = mock(NodeBuilderCustomizableContext.class);

    doReturn(customCtx).when(constraintViolationBuilder).addPropertyNode(any());
    doReturn(context).when(customCtx).addConstraintViolation();
  }

  @Test
  public void dateIsLessThan31DaysValidatorReturnTrue() throws Exception {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("2").month("December").year("2016").build()).build();
    final Boolean value = dateLessThan31DaysFromTodayValidator.isValid(rft, context);
    assertTrue(value);
  }


  @Test
  public void dateIsMoreThan31DaysValidatorReturnFalse() throws Exception {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("2").month("January").year("2017").build()).build();
    final Boolean value = dateLessThan31DaysFromTodayValidator.isValid(rft, context);
    assertFalse(value);
  }


  @Test
  public void dateIsLessThan31DaysValidatorReturnNull() {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("").month("").year("").build()).build();
    final Boolean value = dateLessThan31DaysFromTodayValidator.isValid(rft, context);
    assertThat(value, is(equalTo(true)));
  }

  @Test
  public void dateInvalidFormatValidatorReturnNull() {
    RequestForTransferView rft = RequestForTransferView.builder().payAsSoonAsPossible("N").transferDate(
        TransferFormDate.builder().days("15").month("May").year("18").build()).build();
    final Boolean value = dateLessThan31DaysFromTodayValidator.isValid(rft, context);
    assertThat(value, is(equalTo(true)));
  }

  private Date localDateToDate(final LocalDate lessThan31) {
    return Date.from(lessThan31.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }

}
