package com.nhsbsa.model.validation;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.model.ContributionDate;
import java.math.BigDecimal;
import java.time.Month;
import java.time.YearMonth;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class AdjustmentNotNullValidatorTest {

  private static Validator validator;

  private YearMonth now = YearMonth.now();
  private YearMonth nowMinus2Months = now.minusMonths(2);

  private ContributionDate validAdjustmentDate = ContributionDate.builder()
      .contributionMonth(Month.of(nowMinus2Months.getMonthValue()).toString())
      .contributionYear(nowMinus2Months.getYear())
      .build();
  
  Adjustment adjustment;
  
  @BeforeClass
  public static void setUpValidator() {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Before
  public void before() {
    
    adjustment = Adjustment.builder()
        .adjustmentContributionDate(validAdjustmentDate)
        .build();
  }

  @Test
  public void shouldReturnErrorMessageWhenAllValuesNull() {
    
    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{adjustment.notBlank}"))));
  }
  
  @Test
  public void shouldReturnNoErrorMessagesWhenAllValuesPresent() {
    
    adjustment = Adjustment.builder()
        .employeeContributions(new BigDecimal("10"))
        .employerContributions(new BigDecimal("10"))
        .employeeAddedYears(new BigDecimal("10"))
        .additionalPension(new BigDecimal("10"))
        .errbo(new BigDecimal("10"))
        .build();
    
    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
    
  }

  @Test
  public void shouldReturnNoErrorMessagesIfAnyValuePresent() {
    
    adjustment = Adjustment.builder()
        .employeeContributions(new BigDecimal("10"))
        .build();
    
    Set<ConstraintViolation<Adjustment>> constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
    
    adjustment = Adjustment.builder()
        .employerContributions(new BigDecimal("10"))
        .build();
    
    constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
    
    adjustment = Adjustment.builder()
        .employeeAddedYears(new BigDecimal("10"))
        .build();
    
    constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
    
    adjustment = Adjustment.builder()
        .additionalPension(new BigDecimal("10"))
        .build();
    
    constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));
    
    adjustment = Adjustment.builder()
        .errbo(new BigDecimal("10"))
        .build();
    
    constraintViolations = validateAdjustment(adjustment);

    assertThat(constraintViolations.size(), is(0));

  }
  
  private Set<ConstraintViolation<Adjustment>> validateAdjustment(Adjustment adjustment) {
    return validator.validate(adjustment, AdjustmentNotNullValidationGroup.class);
  }
}
