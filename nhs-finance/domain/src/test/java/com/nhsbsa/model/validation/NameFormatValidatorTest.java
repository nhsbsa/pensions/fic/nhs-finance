package com.nhsbsa.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(Theories.class)
public class NameFormatValidatorTest {

  public NameFormatValidator nameFormatValidator = new NameFormatValidator();
  private boolean expectedResult;

  public static final @DataPoints("InvalidNames") String[] StringInvalidNameCandidates =
      {"n1me","na#e","n@me","n!me", "n%me", "na&e", "*ame", "nam+", "name,"};



  public static final @DataPoints("validNames") String[] StringValidNameCandidates =
      {"name", "n-ame", "first-name", "name'name"};

  @Test
  @Theory
  public void containsInvalidFormatReturnsFalse(@FromDataPoints("InvalidNames") String candidate) throws Exception {

    expectedResult = nameFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(false));
  }

  @Test
  @Theory
  public void containsValidFormatReturnsTrue(@FromDataPoints("validNames") String candidate) throws Exception {

    expectedResult = nameFormatValidator.isValid(candidate, null);
    assertThat(expectedResult, is(true));
  }


}