package com.nhsbsa.model.validation;

/**
 * Created by nataliehulse on 24/11/2017.
 */

import com.nhsbsa.model.ForgottenPassword;
import org.hibernate.validator.HibernateValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

    @RunWith(Parameterized.class)
    public class EmailFormatValidatorTest {

        private LocalValidatorFactoryBean localValidatorFactory;
        private ForgottenPassword forgottenPassword;
        private boolean expectedValidity;

        public EmailFormatValidatorTest(final ForgottenPassword forgottenPassword, final boolean expectedValidity) {
            this.forgottenPassword = forgottenPassword;
            this.expectedValidity = expectedValidity;
        }


        @Before
        public void setup() {
            localValidatorFactory = new LocalValidatorFactoryBean();
            localValidatorFactory.setProviderClass(HibernateValidator.class);
            localValidatorFactory.afterPropertiesSet();
        }


        @Parameterized.Parameters(name = "{index}: forgottenPassword({0}) expected: {1}")
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {ForgottenPassword.builder()
                            .email("")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("Test")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("Test@")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test@.com.te")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test123@gmail.")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test123@.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test123@.com.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test@test@gmail.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test..2017@gmail.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test123.@gmail.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test()*@gmail.com")
                            .build(), false},

                    {ForgottenPassword.builder()
                            .email("test@%*.com")
                            .build(), false},


                    {ForgottenPassword.builder()
                            .email("test.test@nhs.net")
                            .build(), true},

                    {ForgottenPassword.builder()
                            .email("test@yahoo.com")
                            .build(), true},

                    {ForgottenPassword.builder()
                            .email("test.100@test.com.au")
                            .build(), true},

                    {ForgottenPassword.builder()
                            .email("test100@gmail.com")
                            .build(), true},


            });
        }

        @Test
        public void testObjectValidity() {

            final Set<ConstraintViolation<ForgottenPassword>> constraintViolations = localValidatorFactory.validate(forgottenPassword);

            if (!expectedValidity) {
                final int size = constraintViolations.size();
                assertTrue("Expected to fail validation", size > 0);
            }
            assertEquals("should match validity", expectedValidity, constraintViolations.size() == 0);

        }

    }