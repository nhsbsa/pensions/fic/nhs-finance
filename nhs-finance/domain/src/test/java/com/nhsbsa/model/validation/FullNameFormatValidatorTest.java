package com.nhsbsa.model.validation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FullNameFormatValidatorTest {

  public FullNameFormatValidator fullNameFormatValidator = new FullNameFormatValidator();
  private boolean expectedResult;


  @Test
  public void containsInvalidFormatReturnsFalse() throws Exception {

    String invalidFullName = "Name";

    expectedResult = fullNameFormatValidator.isValid(invalidFullName, null);
    assertThat(expectedResult, is(false));
  }

  @Test
  public void containValidFormatReturnsTrue() throws Exception {

    String validFullName = "Name LastName";

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(true));
  }

  @Test
  public void apostropheCharAllowed() throws Exception {

    String validFullName = "John Lane's";

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(true));
  }

  @Test
  public void dashCharAllowed() throws Exception {

    String validFullName = "John S-T";

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(true));
  }

  @Test
  public void severalNames() throws Exception {

    String validFullName = "John S-T Grand Super";

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(true));
  }

  @Test
  public void fullNameLengthMoreThan200CharNotAllowed() throws Exception {

    String validFullName = makeStringOfLength(201);

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(false));
  }

  @Test
  public void fullNameLengthUpTo200CharAllowed() throws Exception {

    String validFullName = makeStringOfLength(200);

    expectedResult = fullNameFormatValidator.isValid(validFullName, null);
    assertThat(expectedResult, is(true));
  }

  private String makeStringOfLength(int length) {
    String source = "abcde fghij";
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < length; i += 1) {
      builder.append(source.charAt(i % 10));
    }
    return builder.toString();
  }


}