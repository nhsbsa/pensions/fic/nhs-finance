package com.nhsbsa.model.validation;

import org.junit.Test;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test validation rules for password format:
 * <li>
 * <ulMust contain Upper Character</ul>
 * <ul>Must be at least 12 characters</ul>
 * <ul>Must contain a number</ul>
 * </li>
 * <p>
 * Created by nataliehulse on 09/10/2017.
 */
public class PasswordFormatValidatorTest {

    private static final PasswordFormatValidator validator = new PasswordFormatValidator();
    private static final ConstraintValidatorContext DEFAULT_CONTEXT = null;


    @Test
    public void givenPasswordLessThan12CharactersFails() {
        assertFalse(validator.isValid(new String("Test1"), DEFAULT_CONTEXT));
    }

    @Test
    public void givenPasswordDoesNotContainNummberFails() {
        assertFalse(validator.isValid(new String("Testingstest"), DEFAULT_CONTEXT));
    }

    @Test
    public void givenPasswordDoesNotUppercaseCharacterFails() {
        assertFalse(validator.isValid(new String("testing12345"), DEFAULT_CONTEXT));
    }

    @Test
    public void givenPasswordDoesContainCorrectFormatPasses() {
        assertTrue(validator.isValid(new String("Testing12345"), DEFAULT_CONTEXT));
    }


}
