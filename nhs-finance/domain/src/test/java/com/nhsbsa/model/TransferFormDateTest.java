package com.nhsbsa.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import com.nhsbsa.model.validation.DateLessThan31DaysFromToday;
import com.nhsbsa.model.validation.SchedulePaymentValidationGroup;
import com.nhsbsa.model.validation.SchedulePaymentValidationSequence;
import com.nhsbsa.testUtils.TestableAnnotationsUtil;
import com.nhsbsa.view.RequestForTransferView;
import java.lang.annotation.Annotation;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.Constraint;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Created by nataliehulse on 11/11/2016.
 */
public class TransferFormDateTest {

  private RequestForTransferView rft;
  private TransferFormDate transferFormDate;
  private static Validator validator;

  private static Annotation originalAnnotation;

  @BeforeClass
  public static void setUpValidator() {

    // The sentiment upon seeing this code -> https://media.giphy.com/media/aZ3LDBs1ExsE8/giphy.gif
    originalAnnotation = TestableAnnotationsUtil.alterAnnotationValueJDK8(
        DateLessThan31DaysFromToday.class,
        Constraint.class,
        new TestableConstraint(DateLessThan31DaysFromTodayTestableValidator.class)
    );

    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @AfterClass
  public static void afterTest(){
    TestableAnnotationsUtil.alterAnnotationValueJDK8(
        DateLessThan31DaysFromToday.class,
        Constraint.class,
        originalAnnotation
    );
  }

  @Before
  public void setUp() {
    LocalDate localDate = LocalDate.now();
    rft = new RequestForTransferView();
    // Set other fields in rft object so their validations don't fail
    rft.setPayAsSoonAsPossible("N");
    rft.setContributionDate(new ContributionDate(localDate.getMonth().toString(), localDate.getYear()));
    transferFormDate = new TransferFormDate();
  }

  @Test
  public void FormDateNotBlankValidationError() throws Exception {
    transferFormDate.setDays("");
    transferFormDate.setMonth("");
    transferFormDate.setYear("");
    rft.setTransferDate(transferFormDate);

    Set<ConstraintViolation<RequestForTransferView>> constraintViolations = validator
        .validate(rft, SchedulePaymentValidationSequence.class);

    assertThat(constraintViolations, hasSize(1));
    assertThat(constraintViolations.iterator().next().getMessage(),
        is(equalTo(("{formDate.notBlank}"))));
  }


}
