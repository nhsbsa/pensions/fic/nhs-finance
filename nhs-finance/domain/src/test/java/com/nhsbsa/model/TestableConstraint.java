package com.nhsbsa.model;

import java.lang.annotation.Annotation;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import lombok.Data;

@Data
public class TestableConstraint implements Constraint {

  private final Class<? extends ConstraintValidator<?, ?>> validatedBy;

  public TestableConstraint(Class<? extends ConstraintValidator<?, ?>> validatedBy){
    this.validatedBy = validatedBy;
  }

  @Override
  public Class<? extends ConstraintValidator<?, ?>>[] validatedBy() {
    return new Class[]{validatedBy};
  }

  @Override
  public Class<? extends Annotation> annotationType() {
    return Constraint.class;
  }
}
