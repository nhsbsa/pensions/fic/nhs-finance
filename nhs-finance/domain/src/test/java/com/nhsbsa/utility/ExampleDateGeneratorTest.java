package com.nhsbsa.utility;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(SpringRunner.class)
public class ExampleDateGeneratorTest {

  private final List<String> BANK_HOLIDAYS = Arrays.asList("01/01","07/05","28/05","27/07","25/12","26/12");

  private ExampleDateGenerator dateGenerator;
  
  @Before
  public void setUp() {
      dateGenerator = spy(new ExampleDateGenerator());
      
      ReflectionTestUtils.setField(dateGenerator, "deadlineDay", 18);
      ReflectionTestUtils.setField(dateGenerator, "excludedDates", BANK_HOLIDAYS);
      
  }
  
  @Test
  public void testGeneratePaymentMonthExampleAfter18thReturnSameMonth() {
    
    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2017, Month.NOVEMBER, 19));

    String result = dateGenerator.generatePaymentMonthExample();

    assertEquals("November 2017", result);
  }

  @Test
  public void testGeneratePaymentMonthExampleOn18thReturnPreviousMonth() {
    
    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2017, Month.NOVEMBER, 18));

    String result = dateGenerator.generatePaymentMonthExample();

    assertEquals("October 2017", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOnWeekendBefore3() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.APRIL, 26));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("30 April 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOnWeekendAfter3() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.APRIL, 26));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("01 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOnWeekendAndEndsOnBankHoliday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 03));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("08 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOnWeekendAndIncludesBankHoliday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 03));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("09 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOn2Days() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 01));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("03 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOn3DaysOnFriday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 11));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("16 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOn3DaysOnFridayWithBankHol() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 4));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("10 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOn2DaysOnFridayWithBankHol() {
    
    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 4));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("09 May 2018", result);
  }

  @Test
  public void testGenerateFormattedHintDateAddsOn2DaysOnFriday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 11));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    String result = dateGenerator.generateFormattedPaymentHintDate();

    assertEquals("15 May 2018", result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysMonday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 14));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 16), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysMonday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 14));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 17), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysTuesday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 15));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 17), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysTuesday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 15));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 18), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysWednesday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 16));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 18), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysWednesday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 16));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 21), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysThursday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 17));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 21), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysThursday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 17));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 22), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysOnFriday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 11));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 15), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysOnFriday() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 11));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 16), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn2DaysOnFridayWithBankHol() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 4));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(false);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 9), result);
  }

  @Test
  public void testGenerateTransferDateAddsOn3DaysOnFridayWithBankHol() {    

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 4));
    when(dateGenerator.isAfterDeadlineHour()).thenReturn(true);

    LocalDate result = dateGenerator.generateTransferDate();

    assertEquals(LocalDate.of(2018, Month.MAY, 10), result);
  }

  @Test
  public void given_time_on_or_after_3_return_true() {

    when(dateGenerator.now()).thenReturn(LocalTime.of(15,00));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(true, result);
  }

  @Test
  public void given_time_after_3_return_true() {

    when(dateGenerator.now()).thenReturn(LocalTime.of(16,00));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(true, result);
  }

  @Test
  public void given_time_before_3_return_true() {
    
    ReflectionTestUtils.setField(dateGenerator, "deadlineHour", 15);

    when(dateGenerator.now()).thenReturn(LocalTime.of(14,59));

    boolean result = dateGenerator.isAfterDeadlineHour();

    assertEquals(false, result);
  }

  @Test
  public void testGetNextWorkingDayIncludingWeekend() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 18), 2);

    assertEquals(LocalDate.of(2018, Month.MAY, 22), result);
  }

  @Test
  public void testGetNextWorkingDayIncludingWeekendAndBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 4), 2);

    assertEquals(LocalDate.of(2018, Month.MAY, 9), result);
  }

  @Test
  public void testGetNextWorkingDayIncludingBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 4), 3);

    assertEquals(LocalDate.of(2018, Month.MAY, 10), result);
  }

  @Test
  public void testGetNextWorkingDayWithoutWeekendOrBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 1), 2);

    assertEquals(LocalDate.of(2018, Month.MAY, 3), result);
  }

  @Test
  public void testGetNextWorkingDayWithNullBankHolidays() {

    dateGenerator = new ExampleDateGenerator();
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 3), 2);

    assertEquals(LocalDate.of(2018, Month.MAY, 7), result);
  }

  @Test
  public void testGetPreviousWorkingDayFromTuesdayIncludingWeekend() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JUNE, 12), -2); // Tuesday

    assertEquals(LocalDate.of(2018, Month.JUNE, 8), result); // Previous Friday
  }

  @Test
  public void testGetPreviousWorkingDayFromMondayIncludingWeekend() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JUNE, 11), -2); // Monday

    assertEquals(LocalDate.of(2018, Month.JUNE, 7), result); // Previous Thursday
  }

  @Test
  public void testGetPreviousWorkingDayIncludingWeekendAndBankHoliday() {
    
    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 30), -2); // Wednesday

    assertEquals(LocalDate.of(2018, Month.MAY, 25), result); // Previous Friday
  }
  
  @Test
  public void testGetWorkingDayGivenWorkingDayAndZeroOffset() {

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.MAY, 30), 0); // Wednesday

    assertEquals(LocalDate.of(2018, Month.MAY, 30), result); // Wednesday
  }
  
  @Test
  public void testNextWorkingDayGivenNonWorkingDayAndZeroOffset() {    

    LocalDate result = dateGenerator.getNearestWorkingDay(LocalDate.of(2018, Month.JULY, 21), 0); // Saturday

    assertEquals(LocalDate.of(2018, Month.JULY, 23), result); // Monday
  }
  
  
  @Test
  public void testGetDeadlineHour() {

    ReflectionTestUtils.setField(dateGenerator, "deadlineHour", 15);

    int result = dateGenerator.getDeadlineHour();

    assertEquals(15, result);
  }
  
  @Test
  public void testIsWorkingDayReturnsFalseForSaturday() {
    
    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 12));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsFalseForSunday() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 13));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsFalseForBankHoliday() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 7));

    assertEquals(false, result);
  }

  @Test
  public void testIsWorkingDayReturnsTrueForWorkingDay() {

    boolean result = dateGenerator.isWorkingDay(LocalDate.of(2018, Month.MAY, 14));

    assertEquals(true, result);
  }

  @Test
  public void testGenerateCurrentMonthBeforeDeadlineDateReturnsCurrentMonthDateMinus2WorkingDays() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 10));

    String result = dateGenerator.generateFormattedEmailReminderDeadlineDate();

    assertEquals("16 May 2018", result);
  }

  @Test
  public void testGenerateCurrentMonthAfterDeadlineDateReturnsNextMonthDateMinus2WorkingDays() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.JUNE, 20));

    String result = dateGenerator.generateFormattedEmailReminderDeadlineDate();

    assertEquals("17 July 2018", result);
  }

  @Test
  public void testCurrentDateBefore2DaysBeforeDeadlineDateReturnsCurrentMonthDateMinus2WorkingDays() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.JUNE, 14));

    String result = dateGenerator.generateFormattedEmailReminderDeadlineDate();

    assertEquals("15 June 2018", result);
  }

  @Test
  public void testCurrentDate2DaysBeforeDeadlineDateBefore3ReturnsCurrentMonthDateMinus2WorkingDays() {

    ReflectionTestUtils.setField(dateGenerator, "deadlineHour", 15);

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.JUNE, 15));
    when(dateGenerator.now()).thenReturn(LocalTime.of(14,59));

    String result = dateGenerator.generateFormattedEmailReminderDeadlineDate();

    assertEquals("15 June 2018", result);
  }

  @Test
  public void testCurrentDate2DaysBeforeDeadlineDateAfter3ReturnsNextMonthDateMinus2WorkingDays() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.JUNE, 15));
    when(dateGenerator.now()).thenReturn(LocalTime.of(15,00));

    String result = dateGenerator.generateFormattedEmailReminderDeadlineDate();

    assertEquals("17 July 2018", result);
  }

  @Test
  public void testGenerateCurrentMonthBeforeDeadlineDateReturnsPreviousMonth() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 10));

    String result = dateGenerator.generateFormattedDeadlineMonth();

    assertEquals("April", result);
  }

  @Test
  public void testGenerateCurrentMonthAfterDeadlineDateReturnsCurrentMonth() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 20));

    String result = dateGenerator.generateFormattedDeadlineMonth();

    assertEquals("May", result);
  }

  @Test
  public void testGenerateCurrentMonthEqualToDeadlineDateBefore3ReturnsPreviousMonth() {

    ReflectionTestUtils.setField(dateGenerator, "deadlineHour", 15);
    
    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 16));
    when(dateGenerator.now()).thenReturn(LocalTime.of(14,55));

    String result = dateGenerator.generateFormattedDeadlineMonth();

    assertEquals("April", result);
  }

  @Test
  public void testGenerateCurrentMonthEqualToDeadlineDateAfter3ReturnsPreviousMonth() {

    when(dateGenerator.dateNow()).thenReturn(LocalDate.of(2018, Month.MAY, 16));
    when(dateGenerator.now()).thenReturn(LocalTime.of(15,01));

    String result = dateGenerator.generateFormattedDeadlineMonth();

    assertEquals("May", result);
  }

  @Test
  public void testMinusWorkingDays2() {

    LocalDate date = LocalDate.of(2018, Month.NOVEMBER, 15);

    LocalDate result = dateGenerator.minusWorkingDays(date, 2);

    LocalDate expectedDate = LocalDate.of(2018, Month.NOVEMBER, 13);
    assertEquals(expectedDate, result);
  }

  @Test
  public void testMinusWorkingDays2StartingOnMondaySkipsWeekend() {

    LocalDate date = LocalDate.of(2018, Month.NOVEMBER, 12);

    LocalDate result = dateGenerator.minusWorkingDays(date, 2);

    LocalDate expectedDate = LocalDate.of(2018, Month.NOVEMBER, 8);
    assertEquals(expectedDate, result);
  }

  @Test
  public void testMinusWorkingDays2StartingOnTuesdaySkipsWeekend() {

    LocalDate date = LocalDate.of(2018, Month.NOVEMBER, 13);

    LocalDate result = dateGenerator.minusWorkingDays(date, 2);

    LocalDate expectedDate = LocalDate.of(2018, Month.NOVEMBER, 9);
    assertEquals(expectedDate, result);
  }

  @Test
  public void testMinusWorkingDays0ReturnsOriginalDate() {

    LocalDate date = LocalDate.of(2018, Month.NOVEMBER, 13);

    LocalDate result = dateGenerator.minusWorkingDays(date, 0);

    LocalDate expectedDate = LocalDate.of(2018, Month.NOVEMBER, 13);
    assertEquals(expectedDate, result);
  }
}


