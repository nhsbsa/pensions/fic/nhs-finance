package com.nhsbsa.model.validation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

/**
 * Created by Mark Lishman on 26/08/2016.
 */

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = DateLessThan31DaysFromTodayValidator.class)
@Documented
@ReportAsSingleViolation
public @interface DateLessThan31DaysFromToday {

  String message() default "{transferDate.greaterThan31Days}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}
