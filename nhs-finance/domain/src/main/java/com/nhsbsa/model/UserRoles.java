package com.nhsbsa.model;

public enum UserRoles {
  ROLE_MASTER,
  ROLE_ADMIN,
  ROLE_STANDARD
}
