package com.nhsbsa.model.validation;

/**
 * Created by Mark Lishman on 07/11/2016.
 */

import com.nhsbsa.utility.TransferFormDateFormat;
import com.nhsbsa.view.RequestForTransferView;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.apache.commons.lang3.StringUtils;

public class ValidFormDateValidator implements ConstraintValidator<ValidFormDate, RequestForTransferView> {

  private String dateNotBlank;

  private TransferFormDateFormat transferFormDateFormat = new TransferFormDateFormat();

  @Override
  public void initialize(ValidFormDate constraintAnnotation) {
    dateNotBlank = constraintAnnotation.message();
  }

  @Override
  public boolean isValid(RequestForTransferView rft, ConstraintValidatorContext context) {

    if ("Y".equals(rft.getPayAsSoonAsPossible()) || StringUtils.isEmpty(rft.getTransferDate().getDays()) &&
        StringUtils.isEmpty(rft.getTransferDate().getMonth()) &&
        StringUtils.isEmpty(rft.getTransferDate().getYear())) {
      return true;
    }

      if(!transferFormDateFormat.isValidDate(rft.getTransferDate())) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(dateNotBlank)
          .addPropertyNode("transferDate")
          .addConstraintViolation();
      return false;
    }
    return true;
  }

}
