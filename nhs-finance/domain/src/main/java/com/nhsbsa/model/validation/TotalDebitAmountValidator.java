package com.nhsbsa.model.validation;

import com.nhsbsa.model.Adjustment;
import com.nhsbsa.view.RequestForTransferView;
import java.math.BigDecimal;
import java.util.Optional;

public final class TotalDebitAmountValidator {

  private TotalDebitAmountValidator(){}

  public static boolean isTotalDebitAmountValid(RequestForTransferView r) {
    if (r.getTotalDebitAmount() == null) {
      return true;
    }
    final BigDecimal contributionTotal = Optional
        .ofNullable(nullSafe(r.getEmployeeContributions())
            .add(nullSafe(r.getEmployerContributions()))
            .add(nullSafe(r.getEmployeeAddedYears()))
            .add(nullSafe(r.getAdditionalPension()))
            .add(nullSafe(r.getErrbo())))
        .orElse(BigDecimal.ZERO);

    final Adjustment adjustment = r.getAdjustment();
    BigDecimal adjustmentTotal = BigDecimal.ZERO;
    if (r.getAdjustmentsRequired()) {
      adjustmentTotal = nullSafe(adjustment.getEmployeeContributions())
          .add(nullSafe(adjustment.getEmployerContributions()))
          .add(nullSafe(adjustment.getEmployeeAddedYears()))
          .add(nullSafe(adjustment.getAdditionalPension()))
          .add(nullSafe(adjustment.getErrbo()));
    }

    return r.getTotalDebitAmount().compareTo(contributionTotal.add(adjustmentTotal)) == 0;
  }

  public static boolean isTotalDebitAmountPositive(RequestForTransferView r){
    if (r.getTotalDebitAmount() == null) {
      return true;
    }
    if (r.isTotalDebitAmountValid()) {
      return r.getTotalDebitAmount().compareTo(BigDecimal.ZERO) > 0;
    }
    return true;
  }

  private static BigDecimal nullSafe(final BigDecimal value) {
    return Optional.ofNullable(value).orElse(BigDecimal.ZERO);
  }

}
