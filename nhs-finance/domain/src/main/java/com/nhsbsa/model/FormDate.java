package com.nhsbsa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by Mark Lishman on 07/11/2016.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FormDate {

  private String days;
  private String month;
  private String year;

  @JsonIgnore
  public Date getDate() {

    if (StringUtils.isEmpty(days) ||
        StringUtils.isEmpty(month) ||
        StringUtils.isEmpty(year)) {
      return null;
    }

    final int monthNum = new MonthNum().getMonthNumFromName(getMonth());
    final String formattedDate = String.format("%s/%s/%s", getDays(), monthNum, getYear());

    try {
      if (StringUtils.isNotEmpty(formattedDate)) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        return dateFormat.parse(formattedDate);
      }
    } catch (ParseException e) {
      return null;
    }
    return null;
  }

}