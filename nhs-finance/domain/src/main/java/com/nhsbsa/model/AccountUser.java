package com.nhsbsa.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountUser {

  private String name;

  private String email;

  private String userRole;

  private String uuid;

  private boolean isAdmin;

}
