package com.nhsbsa.model.validation;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nat Hulse on 24/11/2017.
 *
 */
public class EmailFormatValidator implements ConstraintValidator<EmailFormat, String> {


  private final Pattern regPwPattern;
  private static final String REGEXP_PW_VALIDATION = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
          + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  public EmailFormatValidator() {
    regPwPattern = Pattern.compile(REGEXP_PW_VALIDATION, Pattern.CASE_INSENSITIVE);
  }

  @Override
  public final void initialize(final EmailFormat annotation) {
    // No implementation needed. Nothing to initialise.
  }

  @Override
  public final boolean isValid(final String email, final ConstraintValidatorContext context) {

    return StringUtils.isNotBlank(email) && isValidFormat(email);

  }

  /*
   * Check email is in correct format
   * If all ok return true
   * Invalid format return false
   */
  private boolean isValidFormat(String email) {

    Matcher matcher = regPwPattern.matcher(email);
    return matcher.matches();
  }

}