package com.nhsbsa.model;

import com.nhsbsa.model.validation.EmailFormat;
import com.nhsbsa.model.validation.NameFormat;
import com.nhsbsa.model.validation.PhoneNumberFormat;
import com.nhsbsa.model.validation.ValidatePassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AdminUser {

  private UserRoles role = UserRoles.ROLE_ADMIN;

  private String uuid;

  @EmailFormat
  private String email;

  @ValidatePassword
  private String firstPassword;

  @NotEmpty (message="{admin.firstName.notBlank}")
  @NameFormat (message="{firstNameFormat.NotValid}")
  private String firstName;

  @NotEmpty (message="{admin.lastName.notBlank}")
  @NameFormat (message="{lastNameFormat.NotValid}")
  private String lastName;

  @PhoneNumberFormat
  private String contactTelephone;


  @NotEmpty (message="{organisationName.NotBlank}")
  private String organisationName;



}

