package com.nhsbsa.utility;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:dates.properties")
public class ExampleDateGenerator {

  @Value("#{'${paymentdate.excluded.dates}'.split(',')}")
  private List<String> excludedDates;

  @Value("${paymentdate.deadline.day:18}")
  private int deadlineDay;
  
  @Value("${paymentdate.deadline.hour:13}")
  private int deadlineHour;

  private static final String MONTH_YEAR_FORMAT = "MMMM yyyy";
  private static final String DAY_MONTH_YEAR_FORMAT = "dd MMMM yyyy";
  private static final String DAY_MONTH_FORMAT = "dd/MM";

  private DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern(MONTH_YEAR_FORMAT);
  private DateTimeFormatter fullDateformatter = DateTimeFormatter.ofPattern(DAY_MONTH_YEAR_FORMAT);
  private DateTimeFormatter dayMonthFormatter = DateTimeFormatter.ofPattern(DAY_MONTH_FORMAT);

  public String generatePaymentMonthExample() {

    return generatePreviousMonthExampleDate().format(monthYearFormatter);
  }

  public String generateFormattedPaymentHintDate() {

    int noDaysToAdd = isAfterDeadlineHour() ? 3 : 2;

    return getNearestWorkingDay(dateNow(), noDaysToAdd).format(fullDateformatter);
  }

  public LocalDate generateTransferDate() {

    int noDaysToAdd = isAfterDeadlineHour() ? 3 : 2;

    return getNearestWorkingDay(dateNow(), noDaysToAdd);
  }

  public String generateFormattedEmailReminderDeadlineDate() {

    LocalDate date = generateCurrentMonthDeadlineDate();

    // Need to minus 2 working days for email reminder (Ticket ES-21)
    return minusWorkingDays(date, 2).format(fullDateformatter);
  }

  public String generateFormattedDeadlineMonth() {

    return generateCurrentDeadlineMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH);
  }

  private LocalDate generatePreviousMonthExampleDate() {

    LocalDate exampleDate = dateNow();
    if (exampleDate.getDayOfMonth() <= deadlineDay) {
      exampleDate = exampleDate.minusMonths(1);
    }

    return exampleDate;
  }

  private LocalDate generateCurrentMonthDeadlineDate() {
    LocalDate today = dateNow();
    YearMonth previousYearMonth = YearMonth.from(today.plusMonths(-1));
    LocalDate previousMonthDeadlineDate = PaymentDueDates.findDeadlineDateByYearMonth(previousYearMonth);
    LocalDate previousDeadlineDateMinus2Days = getNearestWorkingDay(previousMonthDeadlineDate, -2);

    LocalDate deadlineDate;

    // Need to check 2 working days before deadline date and whether time < 3pm (Finance process)
    if(today.isBefore(previousDeadlineDateMinus2Days) || (today.isEqual(previousDeadlineDateMinus2Days) && !isAfterDeadlineHour())) {
      deadlineDate = previousMonthDeadlineDate;
    }else {
      YearMonth currentYearMonth = YearMonth.from(today);
      deadlineDate = PaymentDueDates.findDeadlineDateByYearMonth(currentYearMonth);
    }

    return deadlineDate;
  }

  private Month generateCurrentDeadlineMonth() {
    LocalDate today = dateNow();
    YearMonth previousYearMonth = YearMonth.from(today.plusMonths(-1));
    LocalDate previousMonthDeadlineDate = PaymentDueDates.findDeadlineDateByYearMonth(previousYearMonth);
    LocalDate previousDeadlineDateMinus2Days = getNearestWorkingDay(previousMonthDeadlineDate, -2);

    Month deadlineMonth = Month.from(today);

    // Need to check 2 working days before deadline date and whether time < 3pm (Finance process)
    if(today.isBefore(previousDeadlineDateMinus2Days) || (today.isEqual(previousDeadlineDateMinus2Days) && !isAfterDeadlineHour())) {
      deadlineMonth = Month.from(previousYearMonth);
    }

    return deadlineMonth;
  }

  public LocalDate getNearestWorkingDay(LocalDate date, int minWorkingDaysOffset) {
    
    int direction = minWorkingDaysOffset >= 0 ? 1 : -1;
    
    while ((!isWorkingDay(date)) || (minWorkingDaysOffset != 0)) {
      date = date.plusDays(direction);

      if (isWorkingDay(date) && minWorkingDaysOffset != 0) minWorkingDaysOffset += -direction;
    }
      
    return date;
  }
  
  public int getDeadlineHour() {
    return deadlineHour;
  }
  
  public boolean isAfterDeadlineHour() {
    return now().getHour() >= deadlineHour;
  }
  
  public boolean isWorkingDay(LocalDate date) {
    DayOfWeek dayOfWeek = date.getDayOfWeek();
    if (DayOfWeek.SUNDAY.equals(dayOfWeek) || DayOfWeek.SATURDAY.equals(dayOfWeek)) {
      return false;
    }
    return !isExcludedDate(date);
  }
  
  public boolean isExcludedDate(LocalDate date) {
	  return (excludedDates != null && excludedDates.contains(date.format(dayMonthFormatter)));
  }

  public LocalTime now() {
    return LocalTime.now();
  }

  public LocalDate dateNow() {
    return LocalDate.now();
  }

  public LocalDate minusWorkingDays(LocalDate date, int workdays) {
    if (workdays < 1) {
      return date;
    }

    LocalDate result = date;
    int addedDays = 0;
    while (addedDays < workdays) {
      result = result.minusDays(1);
      if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY ||
          result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
        ++addedDays;
      }
    }

    return result;
  }
}