package com.nhsbsa.utility;

import com.nhsbsa.model.FormDate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TransferFormDateFormat {

public boolean isValidDate(FormDate transferFormDate) {

  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MMMM/yyyy");

  String date =
      transferFormDate.getDays() + "/" + transferFormDate.getMonth() + "/" + transferFormDate
          .getYear();

  try {
    LocalDate.parse(date, formatter);
  } catch (DateTimeParseException e) {
    return false;
  }
  return true;
}

}

