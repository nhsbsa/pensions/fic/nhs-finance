package com.nhsbsa.utility;

import java.time.LocalDate;
import java.time.YearMonth;

public enum PaymentDueDates {

  APRIL2018(YearMonth.of(2018,4), LocalDate.of(2018, 5, 18)),
  MAY2018(YearMonth.of(2018,5), LocalDate.of(2018, 6, 19)),
  JUNE2018(YearMonth.of(2018,6), LocalDate.of(2018, 7, 19)),
  JULY2018(YearMonth.of(2018,7), LocalDate.of(2018, 8, 17)),
  AUGUST2018(YearMonth.of(2018,8), LocalDate.of(2018, 9, 19)),
  SEPTEMBER2018(YearMonth.of(2018,9), LocalDate.of(2018, 10, 19)),
  OCTOBER2018(YearMonth.of(2018,10), LocalDate.of(2018, 11, 19)),
  NOVEMBER2018(YearMonth.of(2018,11), LocalDate.of(2018, 12, 19)),
  DECEMBER2018(YearMonth.of(2018,12), LocalDate.of(2019, 1, 18)),
  JANUARY2019(YearMonth.of(2019,1), LocalDate.of(2019, 2, 19)),
  FEBRUARY2019(YearMonth.of(2019,2), LocalDate.of(2019, 3, 19)),
  MARCH2019(YearMonth.of(2019,3), LocalDate.of(2019, 4, 18));

  private YearMonth yearMonth;
  private LocalDate deadlineDate;

  PaymentDueDates(YearMonth yearMonth, LocalDate deadlineDate) {
    this.yearMonth = yearMonth;
    this.deadlineDate = deadlineDate;
    }

  public YearMonth getYearMonth() { return yearMonth; }

  public LocalDate getDate() { return deadlineDate; }

  public static LocalDate findDeadlineDateByYearMonth(YearMonth yearMonth){
    for(PaymentDueDates p : values()){
      if( p.getYearMonth().equals(yearMonth)){
        return p.deadlineDate;
      }
    }
    return null;
  }

}
