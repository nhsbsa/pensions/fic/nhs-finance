package com.nhsbsa.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;

import javax.validation.Valid;
import java.util.List;

/**
 * Wrapper for collection type; JSR 303 won't validate these directly
 *
 * Created by duncan on 24/03/2017.
 */
@Value
@Builder
public class ListWrappingView {

    @Valid
    List<TransferView> set;

    @JsonCreator
    public static ListWrappingView jsonFactory(@JsonProperty("set") List<TransferView> set) {
        return ListWrappingView.builder().set(set).build();
    }
}
