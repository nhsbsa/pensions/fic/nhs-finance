package com.nhsbsa.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.nhsbsa.model.validation.EACodeFormat;
import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import com.nhsbsa.view.validator.NotZero;

/**
 * Encapsulate data for one transaction; c.f. nhs-finance RequestForTransfer
 *
 * Don't care about total pensionable pay or totalDebitAmount
 *
 * as far as I can tell the only way to tell the adjustments from the "non adjustments" in the
 * csv file is by the sales analysis field which will have 'adj' in it.
 *
 * Created by duncan on 24/03/2017.
 */
@Value
@Builder
public class TransferView {

    /**
     * EA Code - this one is needed
     * field length: 7
     * 2 alpha + 4 numeric + suffix (not needed for private beta - change regex to
     *     "^(?:[a-zA-Z]{3}\\d{4})|(?:[a-zA-Z]{2}\\d{4}[a-zA-Z])$" if it's wanted)
     * 3 alpha + 4 numeric
     */
    @NotBlank
    @EACodeFormat
    private String accountCode;

    /**
     * Transaction date; this one is needed also
     * field length: 10
     * DD/MM/YYYY
     */
    @NotNull
    private LocalDate transactionDate;

    /**
     * This comes in as "just a date" and needs to be converted to be relative to the financial
     * year end within this service.
     * field length: 4
     * number - financial year ending and month
     */
    @NotBlank
    @Pattern(regexp = "^\\d{4}$")
    private String contributionYearMonth;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotNull
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal contributions;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotNull
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal employerContributions;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal addedYears;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal additionalPension;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal errbo;

    /**
     * Need this one too if we're dealing with adjustments at the same time...
     * field length: 4
     * number - financial year ending and month
     */
    @Pattern(regexp = "^\\d{4}$")
    private String adjustmentYearMonth;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal adjustmentContributions;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal adjustmentEmployerContributions;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal adjustmentAddedYears;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal adjustmentAdditionalPension;

    /**
     * field length: 13
     * currency +- 100,000,000
     */
    @NotZero
    @Max(100_000_000)
    @Min(-100_000_000)
    @Digits(integer = 9, fraction = 2)
    private BigDecimal adjustmentErrbo;

    /**
     * Employer type - this one is needed
     * field length: 1
     */
    @NotBlank
    @Pattern(regexp = "(^([SG])$)")
    private String employerType;

    @JsonCreator
    public static TransferView jsonFactory(
            @JsonProperty("accountCode") String accountCode,
            @JsonProperty("transactionDate") LocalDate transactionDate,
            @JsonProperty("contributionYearMonth") String contributionYearMonth,
            @JsonProperty("contributions") BigDecimal contributions,
            @JsonProperty("employerContributions") BigDecimal employerContributions,
            @JsonProperty("addedYears") BigDecimal addedYears,
            @JsonProperty("additionalPension") BigDecimal additionalPension,
            @JsonProperty("errbo") BigDecimal errbo,
            @JsonProperty("adjustmentYearMonth") String adjustmentYearMonth,
            @JsonProperty("adjustmentContributions") BigDecimal adjustmentContributions,
            @JsonProperty("adjustmentEmployerContributions") BigDecimal adjustmentEmployerContributions,
            @JsonProperty("adjustmentAddedYears") BigDecimal adjustmentAddedYears,
            @JsonProperty("adjustmentAdditionalPension") BigDecimal adjustmentAdditionalPension,
            @JsonProperty("adjustmentErrbo") BigDecimal adjustmentErrbo
    ) {
        return TransferView.builder()
                .accountCode(accountCode)
                .transactionDate(transactionDate)
                .contributionYearMonth(contributionYearMonth)
                .contributions(contributions)
                .employerContributions(employerContributions)
                .addedYears(addedYears)
                .additionalPension(additionalPension)
                .errbo(errbo)
                .adjustmentYearMonth(adjustmentYearMonth)
                .adjustmentContributions(adjustmentContributions)
                .adjustmentEmployerContributions(adjustmentEmployerContributions)
                .adjustmentAddedYears(adjustmentAddedYears)
                .adjustmentAdditionalPension(adjustmentAdditionalPension)
                .adjustmentErrbo(adjustmentErrbo)
                .build();
    }

    public boolean hasContributionsValue() {
        // There is always a contribution value
        return true;
    }

    public boolean hasEmployerContributions() {
        // There is always an employer contributions value
        return true;
    }

    private boolean notZero(BigDecimal value) {
        return BigDecimal.ZERO.compareTo(value) != 0;
    }

    private boolean exists(BigDecimal value) {
        return value != null && notZero(value);
    }

    public boolean hasAdditionalPension() {
        return exists(additionalPension);
    }

    public boolean hasAddedYears() {
        return exists(addedYears);
    }

    public boolean hasErrbo() {
        return exists(errbo);
    }

    public boolean hasAdjustmentContributionsValue() {
        return exists(adjustmentContributions);
    }

    public boolean hasAdjustmentEmployerContributions() {
        return exists(adjustmentEmployerContributions);
    }

    public boolean hasAdjustmentAdditionalPension() {
        return exists(adjustmentAdditionalPension);
    }

    public boolean hasAdjustmentAddedYears() {
        return exists(adjustmentAddedYears);
    }

    public boolean hasAdjustmentErrbo() {
        return exists(adjustmentErrbo);
    }
}
