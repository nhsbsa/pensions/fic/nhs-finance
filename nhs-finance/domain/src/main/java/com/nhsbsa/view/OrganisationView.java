package com.nhsbsa.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import lombok.Builder;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.UUID;

/**
 * Model an employer authority.
 *
 * Created by duncan on 09/03/2017.
 */
@lombok.Value
@Builder(builderClassName = "OrganisationBuilder")
public final class OrganisationView {

    @ApiModelProperty(
            notes = "Used throughout the API to identify an organisation. "
                    + "Not populated unless the organisation is held in durable store.",
            required = true
    )
    private final UUID identifier;

    @ApiModelProperty(required = true, notes = "Organisation's name.")
    @NotBlank
    @Size(max = 1000)
    private final String name;

    @ApiModelProperty(required = true, notes = "EA Details.")
    private final List<EmployingAuthorityView> eas;

    @JsonCreator
    public static OrganisationView jsonFactory(@JsonProperty("identifier") UUID id,
                                        @JsonProperty("name") String name,
                                        @JsonProperty("eas") List<EmployingAuthorityView> eas,
                                        @JsonProperty("accountName") String accountName,
                                        @JsonProperty("accountNumber") String accountNumber) {
        return OrganisationView.builder()
                .identifier(id)
                .name(name)
                .eas(eas)
                .build();
    }

    /**
     * Return a new immutable {@code UserView} instance with the identifier
     * provided.
     *
     * @param identifier
     * @return
     */
    public OrganisationView withIdentifier(UUID identifier) {
        return OrganisationView.builder()
                .identifier(identifier)
                .name(this.getName())
                .eas(this.getEas())
                .build();
    }
}
