package com.nhsbsa.security;

import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateEaRequest {

  private String organisationUuid;

  @NotNull
  private String eaName;

  @NotNull
  private String eaCode;

  @NotNull
  private String employerType;
}

