package com.nhsbsa.security;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by nataliehulse on 28/09/2017.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SetPasswordResponse {

    private String uuid;

    private boolean firstLogin;

    private boolean passwordSameAsPrevious;


}

