package com.nhsbsa.security;

import com.nhsbsa.view.OrganisationView;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest {

  @NotNull
  private String username;

  private String firstName;

  private String surname;

  private String contactNumber;

  private List<OrganisationView> organisations;

  private String uuid;

}

