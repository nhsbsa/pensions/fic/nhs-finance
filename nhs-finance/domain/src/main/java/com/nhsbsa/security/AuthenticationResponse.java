package com.nhsbsa.security;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jeffreya on 22/09/2016.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationResponse {

    public enum AuthenticationStatus {
        AUTH_SUCCESS, AUTH_FAILURE, ACCOUNT_LOCKED
    }

    private String uuid;
    private String token;
    private Boolean firstLogin;
    private String role;
    private AuthenticationStatus authenticationStatus;
}
