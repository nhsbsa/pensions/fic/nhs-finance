package com.nhsbsa.exception;

public class TooManyExcludedDatesException extends RuntimeException {

  public TooManyExcludedDatesException(int maxNumDates, int actualNumDates) {
    super("Too many excluded dates have been set.  The maximum is: " + maxNumDates + " but there were: " + actualNumDates);
  }
}
